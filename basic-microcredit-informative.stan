data {
  int<lower=0> K;  // number of sites
  int<lower=0> N;  // total number of observations
  int<lower=0> P;  // dimensionality of parameter vector which is jointly distributed - here, it is 2 dimensional
  real y[N];       // outcome variable of interest
  int ITT[N];      // intention to treat indicator
  int site[N];     // factor variable to split them out into K sites
  matrix[P,P] mutau_prior_sigma;
  vector[P] mutau_prior_mean;
  real sigma_mutau_prior_df;
  matrix[P,P] sigma_mutau_prior_cov;
}
parameters {
  vector[P] mutau;
  matrix[K,P] mutau_k;
  cov_matrix[P] sigma_mutau;
  // real<lower=0> sigma_y;
  real<lower=0> sigma_y_k[K];
}
model {
  // data variance priors
  // sigma_y ~ inv_gamma(0.01, 0.01);
  sigma_y_k ~ uniform(0,100000);

  // parameter variance priors
  sigma_mutau ~ inv_wishart(sigma_mutau_prior_df, sigma_mutau_prior_cov);

  // hyperparameter priors
  mutau ~ multi_normal(mutau_prior_mean, mutau_prior_sigma);

  for (k in 1:K) {
    mutau_k[k] ~ multi_normal(mutau, sigma_mutau);
  }

  {
    vector[N] meanvec_y;
    vector[N] sigmavec_y;
    for (n in 1:N) {
      meanvec_y[n] <- mutau_k[site[n],1] + mutau_k[site[n],2]*ITT[n];
      sigmavec_y[n] <- sigma_y_k[site[n]];
    }
    y ~ normal(meanvec_y, sigmavec_y); // data level
  }
}

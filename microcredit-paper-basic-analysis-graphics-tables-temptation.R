#### 7. Graphics for the paper ####

# GRAPHICS AND TABLES FOR FULL BASIC ANALYSIS FOR THE MICROCREDIT BAYES PAPER IN 1 PLACE :
# This one is for temptation but serves as the general template
# Rachael Meager
# First Version: August 2015

### Notes ###

# Analysis needs to be done before you can run this, obviously

#### 1. Preliminaries and Data Intake ####
rm(list = ls())

chooseCRANmirror(ind=90)
installer <- FALSE
if (installer==TRUE) {
  install.packages('foreign')
  install.packages('Hmisc')
  install.packages('xtable')
  install.packages('coda')
  install.packages('stargazer')
  install.packages('sandwich')
  install.packages('multiwayvcov')
  install.packages('parallel')
  install.packages("ggplot2")
  install.packages("gridExtra")
  install.packages('coda')
}

library(multiwayvcov)
library(sandwich)
library(stargazer)
library(foreign)
library(Hmisc)
library(xtable)
library(coda)
library(parallel)
library(ggplot2)
library(gridExtra)
library(coda)
library(dummies)
library(zoo)
library(Matrix)

library(dplyr)

# warning: if you change the file names for output you will break the paper's latex code!!! 

# Load data
rm(list = ls())
load("microcredit_joint_model_temptation_output.RData")

## 7.1 Tau posterior versus pooled OLS asymptotic distribution ##

ols_pooled_tau_mean <- ols_pooled$coef[2]
ols_pooled_tau_sd_robust <- summary(ols_pooled)$coef[2,2]
tau_mean <- textable_temptation_joint["mutau[2]",1]
tau_X2.5. <- textable_temptation_joint["mutau[2]",4]
tau_X25. <- textable_temptation_joint["mutau[2]",5]
tau_X75. <- textable_temptation_joint["mutau[2]",7]
tau_X97.5. <- textable_temptation_joint["mutau[2]",8]


estimate_pooled_type <- c(rep("BHM Posterior",1), rep("OLS",1))
estimate_pooled_means <- c(tau_mean, ols_pooled_tau_mean)
estimate_pooled_X2.5. <- c(tau_X2.5., ols_pooled_tau_mean-1.96*ols_pooled_tau_sd_robust)
estimate_pooled_X97.5. <- c(tau_X97.5., ols_pooled_tau_mean+1.96*ols_pooled_tau_sd_robust)
estimate_pooled_X25. <- c(tau_X25., ols_pooled_tau_mean + ols_pooled_tau_sd_robust*qnorm(0.25, mean=0, sd=1))
estimate_pooled_X75. <- c(tau_X75., ols_pooled_tau_mean + ols_pooled_tau_sd_robust*qnorm(0.75, mean=0, sd=1))

estimates_pooled_df <- data.frame(estimate_pooled_type, estimate_pooled_means, estimate_pooled_X2.5., estimate_pooled_X97.5., estimate_pooled_X25., estimate_pooled_X75.)


pooled_plot <- ggplot(data=estimates_pooled_df, aes(x=estimate_pooled_type, y=estimate_pooled_means, color = factor(estimate_pooled_type), fill  = factor(estimate_pooled_type), width=.2,  alpha= 0.25))

pooled_fancy_plot <- pooled_plot +  geom_boxplot(aes(ymax=estimate_pooled_X97.5., ymin=estimate_pooled_X2.5., lower=estimate_pooled_X25. , upper = estimate_pooled_X75., middle = estimate_pooled_means), stat=('identity'))
pdf(file="tau_dist_withols_basic_bhm_joint_temptation.pdf",width = 8, height = 4)
pooled_fancy_plot + coord_flip(ylim=c(-10, 5))  + ylab("Posterior mean, 50% interval (box), and 95% interval (line) \n for General Treatment Effect (USD PPP per 2 weeks)") + xlab("")+ theme(legend.position="none") + theme(text = element_text(size=17,color="black"), axis.text.y = element_text(size=17,color="grey40"),axis.text.x = element_text(size=17,color="grey40"))
dev.off()

## 7.2 Tau_k posteriors together ##

tau_k_names <- c("mutau_k[1,2]","mutau_k[2,2]","mutau_k[3,2]","mutau_k[4,2]","mutau_k[5,2]")
textable_tau_ks <- textable_temptation_joint[tau_k_names,]
study_country <- study_country[1:5]

tau_k_df <- data.frame(study_country, textable_tau_ks)

tau_k_plot <- ggplot(data=tau_k_df, aes(x=study_country, y=mean, colour=study_country, fill = study_country), alpha= 0.2, width=.2) + coord_cartesian(ylim = c(-30,30)) 
tau_k_plot_fancy <- tau_k_plot +  geom_boxplot(aes(ymax=X97.5., ymin=X2.5., lower =X25. , upper = X75., middle = mean,  alpha= 0.2, width=.2), stat=('identity'))
pdf(file="tau_k_dist_basic_bhm_joint_temptation.pdf",width = 8, height = 6)
tau_k_plot_fancy + coord_flip(ylim=c(-20,10))  +ylab("Posterior mean, 50% interval (box), and 95% interval (line)\n for each Treatment Effect (USD PPP per 2 weeks)") + xlab("") + theme(legend.position="none") + theme(legend.title=element_blank()) + scale_alpha_identity() + theme(text = element_text(size=17,color="black"), axis.text.y = element_text(size=17,color="grey40"),axis.text.x = element_text(size=17,color="grey40"))
dev.off()

## 7.3 Tau_k posteriors versus separated OLS regressions ##


ols_tau_sd_robust <- standardised_ols_best_match_output_temptation_coefficients_sds
ols_tau_means <- standardised_ols_best_match_output_temptation_coefficients

ols_output_df <- data.frame(study_country, ols_tau_means, ols_tau_sd_robust)

ols_plot <- ggplot(data=ols_output_df, aes(x=study_country, y=ols_tau_means, colour=study_country)) 

ols_plot + geom_boxplot(aes(ymax=ols_tau_means+1.96*ols_tau_sd_robust, ymin=ols_tau_means-1.96*ols_tau_sd_robust, lower = ols_tau_means + ols_tau_sd_robust*qnorm(0.25, mean=0, sd=1), upper = ols_tau_means + ols_tau_sd_robust*qnorm(0.75, mean=0, sd=1), middle = ols_tau_means), stat=('identity'))

estimate_type <- c(rep("BHM Posterior",length(tau_k_names)), rep("OLS",length(tau_k_names)))
estimate_study_country <- rep(study_country,2)
estimate_means <- c(tau_k_df$mean, ols_tau_means)
estimate_X2.5. <- c(tau_k_df$X2.5., ols_tau_means-1.96*ols_tau_sd_robust)
estimate_X97.5. <- c(tau_k_df$X97.5., ols_tau_means+1.96*ols_tau_sd_robust)
estimate_X25. <- c(tau_k_df$X25., ols_tau_means + ols_tau_sd_robust*qnorm(0.25, mean=0, sd=1))
estimate_X75. <- c(tau_k_df$X75., ols_tau_means + ols_tau_sd_robust*qnorm(0.75, mean=0, sd=1))

estimates_df <- data.frame(estimate_type, estimate_study_country, estimate_means, estimate_X2.5., estimate_X97.5., estimate_X25., estimate_X75.)

bhm_ols_plot <- ggplot(data=estimates_df, aes(x=factor(estimate_study_country), y=estimate_means, color = factor(estimate_type), fill  = factor(estimate_type), width=.5,  alpha= 0.25))

bhm_ols_fancy_plot <- bhm_ols_plot +  geom_boxplot(aes(ymax=estimate_X97.5., ymin=estimate_X2.5., lower=estimate_X25. , upper = estimate_X75., middle = estimate_means), stat=('identity'))


pdf(file="tau_k_dist_withols_basic_bhm_joint_temptation.pdf",width = 8, height = 5)
bhm_ols_fancy_plot + coord_flip(ylim=c(-20,20))  +ylab("Posterior mean, 50% interval (box), and 95% interval (line) \n for each Treatment Effect (USD PPP per 2 weeks)") + xlab("") + theme(legend.title=element_blank()) + scale_alpha_identity() + theme(text = element_text(size=17,color="black"), axis.text.y = element_text(size=17,color="grey40"),axis.text.x = element_text(size=17,color="grey40"))
dev.off()


## 7.4 Tau parent distribution versus pooled OLS asymptotic distribution ## 

# now do the parent tau

ols_pooled_tau_mean <- ols_pooled$coef[2]
ols_pooled_tau_sd_robust <- summary(ols_pooled)$coef[2,2]
tau_mean <- textable_temptation_joint["mutau[2]",1]
tau_sd <- sqrt(textable_temptation_joint["sigma_mutau[2,2]",1])


estimate_parent_type <- c(rep("BHM Parent Distribution",1), rep("OLS",1))
estimate_parent_means <- c(tau_mean, ols_pooled_tau_mean)
estimate_parent_X2.5. <- c(tau_mean-1.96*tau_sd, ols_pooled_tau_mean-1.96*ols_pooled_tau_sd_robust)
estimate_parent_X97.5. <- c(tau_mean+1.96*tau_sd, ols_pooled_tau_mean+1.96*ols_pooled_tau_sd_robust)
estimate_parent_X25. <- c(tau_mean+qnorm(0.25, mean=0, sd=1)*tau_sd, ols_pooled_tau_mean + ols_pooled_tau_sd_robust*qnorm(0.25, mean=0, sd=1))
estimate_parent_X75. <- c(tau_mean+qnorm(0.75, mean=0, sd=1)*tau_sd, ols_pooled_tau_mean + ols_pooled_tau_sd_robust*qnorm(0.75, mean=0, sd=1))

estimates_parent_df <- data.frame(estimate_parent_type, estimate_parent_means, estimate_parent_X2.5., estimate_parent_X97.5., estimate_parent_X25., estimate_parent_X75.)


parent_plot <- ggplot(data=estimates_parent_df, aes(x=estimate_parent_type, y=estimate_parent_means, color = factor(estimate_parent_type), fill  = factor(estimate_parent_type), width=.2,  alpha= 0.25))

parent_fancy_plot <- parent_plot +  geom_boxplot(aes(ymax=estimate_parent_X97.5., ymin=estimate_parent_X2.5., lower=estimate_parent_X25. , upper = estimate_parent_X75., middle = estimate_parent_means), stat=('identity'))

pdf(file="tau_parent_withols_basic_bhm_joint_temptation.pdf",width = 8, height = 3)
parent_fancy_plot + coord_flip(ylim=c(-10, 10))  +ylab("Posterior mean, 50% interval (box), and 95% interval (line)\n for posterior mean parent distribution of treatment effects (USD PPP per 2 weeks)") + xlab("")+ theme(legend.position="none") + theme(text = element_text(size=14,color="black"), axis.text.y = element_text(size=14,color="grey40"),axis.text.x = element_text(size=14,color="grey40"))
dev.off()

save.image("microcredit_graphs_workspace_temptation.RData")

### 7.5 Priorbiz Split Graphics ###

# CALL POSTERIOR DRAWS #
rm(list = ls())
load("microcredit_independent_model_priorbiz_split_temptation_output.RData")


tau_k_names <- c("tau_k_block[1,1]","tau_k_block[2,1]","tau_k_block[3,1]","tau_k_block[4,1]",
                 "tau_k_block[5,1]")
taup_k_names <- c("tau_k_block[1,2]","tau_k_block[2,2]","tau_k_block[3,2]","tau_k_block[4,2]",
                  "tau_k_block[5,2]")

S <- 1
country <- c(rep("Mexico (Angelucci)", S),
             rep("Mongolia (Attanasio)", S),
             rep("Bosnia (Augsberg)", S),
             rep("India (Banerjee)", S),
             rep("Morocco (Crepon)", S)
             )



# TAU Ks
textable_tau_ks <- textable_stan_fit_temptation_priorbiz_split[tau_k_names,]
tau_k_df <- data.frame(country, textable_tau_ks)

tau_k_plot <- ggplot(data=tau_k_df, aes(x=country, y=mean, colour=country, fill = country), alpha= 0.2, width=.2) + coord_cartesian(ylim = c(-30,30)) 
tau_k_plot_fancy <- tau_k_plot +  geom_boxplot(aes(ymax=X97.5., ymin=X2.5., lower =X25. , upper = X75., middle = mean,  alpha= 0.2, width=.2), stat=('identity'))

tau_k_plot_fancy + coord_flip(ylim=c(-30,30))  +ylab("Posterior mean, 50% interval (box), and 95% interval (line) for each Treatment Effect with PB =0 (USD PPP per 2 weeks)") + xlab("") + theme(legend.position="none")

# TAU P K's
textable_taup_ks <- textable_stan_fit_temptation_priorbiz_split[taup_k_names,]
taup_k_df <- data.frame(country, textable_taup_ks)

taup_k_plot <- ggplot(data=taup_k_df, aes(x=country, y=mean, colour=country, fill = country), alpha= 0.2, width=.2) + coord_cartesian(ylim = c(-30,30)) 
taup_k_plot_fancy <- taup_k_plot +  geom_boxplot(aes(ymax=X97.5., ymin=X2.5., lower =X25. , upper = X75., middle = mean,  alpha= 0.2, width=.2), stat=('identity'))

taup_k_plot_fancy + coord_flip(ylim=c(-100,100))  +ylab("Posterior mean, 50% interval (box), and 95% interval (line) for each Treatment Effect with PB = 1 (USD PPP per 2 weeks)") + xlab("") + theme(legend.position="none")


# plot both in same graph *flexes bicep* *not really this isn't hard i'm just a noob*

estimate_type <- c(rep("Effect when PB = 0",length(tau_k_names)), rep("Additional Effect when PB=1",length(tau_k_names)))
estimate_country <- rep(country,2)
estimate_means <- c(tau_k_df$mean, taup_k_df$mean)
estimate_X2.5. <- c(tau_k_df$X2.5., taup_k_df$X2.5.)
estimate_X97.5. <- c(tau_k_df$X97.5., taup_k_df$X97.5.)
estimate_X25. <- c(tau_k_df$X25., taup_k_df$X25.)
estimate_X75. <- c(tau_k_df$X75., taup_k_df$X75.)

# The palette with black:
cbbPalette <- c("#FF9933", "#0000FF",  "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7","#000000")


estimates_df <- data.frame(estimate_type, estimate_country, estimate_means, estimate_X2.5., estimate_X97.5., estimate_X25., estimate_X75.)

bhm_split_plot <- ggplot(data=estimates_df, aes(x=factor(estimate_country), y=estimate_means, color = factor(estimate_type), fill  = factor(estimate_type), width=.3,  alpha= 0.25)) + scale_colour_manual(values=cbbPalette) + scale_fill_manual(values=cbbPalette)

bhm_split_fancy_plot <- bhm_split_plot +  geom_boxplot(aes(ymax=estimate_X97.5., ymin=estimate_X2.5., lower=estimate_X25. , upper = estimate_X75., middle = estimate_means), stat=('identity'))

pdf(file="tau_k_dist_basic_bhm_split_te_temptation.pdf",width = 8, height = 5)
bhm_split_fancy_plot + coord_flip(ylim=c(-10,10)) +theme(legend.position="bottom") +ylab("Posterior mean, 50% interval (box), and 95% interval (line)\n for each Treatment Effect (USD PPP per 2 weeks)") + xlab("") + theme(legend.title=element_blank()) + scale_alpha_identity()+ theme(text = element_text(size=17,color="black"), axis.text.y = element_text(size=17,color="grey40"),axis.text.x = element_text(size=17,color="grey40"))
dev.off()


# now do the two tau posteriors together


tau_mean <- textable_stan_fit_temptation_priorbiz_split["tau_block[1]",1]
tau_X2.5. <- textable_stan_fit_temptation_priorbiz_split["tau_block[1]",4]
tau_X25. <- textable_stan_fit_temptation_priorbiz_split["tau_block[1]",5]
tau_X75. <- textable_stan_fit_temptation_priorbiz_split["tau_block[1]",7]
tau_X97.5. <- textable_stan_fit_temptation_priorbiz_split["tau_block[1]",8]

taup_mean <- textable_stan_fit_temptation_priorbiz_split["tau_block[2]",1]
taup_X2.5. <- textable_stan_fit_temptation_priorbiz_split["tau_block[2]",4]
taup_X25. <- textable_stan_fit_temptation_priorbiz_split["tau_block[2]",5]
taup_X75. <- textable_stan_fit_temptation_priorbiz_split["tau_block[2]",7]
taup_X97.5. <- textable_stan_fit_temptation_priorbiz_split["tau_block[2]",8]


estimate_both_type <- c(rep("Effect when PB=0",1), rep("Additional Effect when PB=1",1))
estimate_both_means <- c(tau_mean, taup_mean)
estimate_both_X2.5. <- c(tau_X2.5., taup_X2.5.)
estimate_both_X97.5. <- c(tau_X97.5., taup_X97.5.)
estimate_both_X25. <- c(tau_X25., taup_X25.)
estimate_both_X75. <- c(tau_X75., taup_X75.)

estimates_both_df <- data.frame(estimate_both_type, estimate_both_means, estimate_both_X2.5., estimate_both_X97.5., estimate_both_X25., estimate_both_X75.)


both_plot <- ggplot(data=estimates_both_df, aes(x=estimate_both_type, y=estimate_both_means, color = factor(estimate_both_type), fill  = factor(estimate_both_type), width=.2,  alpha= 0.25))

both_fancy_plot <- both_plot +  geom_boxplot(aes(ymax=estimate_both_X97.5., ymin=estimate_both_X2.5., lower=estimate_both_X25. , upper = estimate_both_X75., middle = estimate_both_means), stat=('identity'))  + scale_colour_manual(values=cbbPalette) + scale_fill_manual(values=cbbPalette)

pdf(file="tau_dist_basic_bhm_split_te_temptation.pdf",width = 6, height = 3)
both_fancy_plot + coord_flip(ylim=c(-10, 10))  +ylab("Posterior mean, 50% interval (box), and 95% interval (line)\n for General Treatment Effect (USD PPP per 2 weeks)") + xlab("")+ theme(legend.position="none")+ theme(text = element_text(size=17,color="black"), axis.text.y = element_text(size=17,color="grey30"),axis.text.x = element_text(size=17,color="grey40"))
dev.off()

save.image("microcredit_graphs_workspace_temptation_split_te.RData")

#### 8. Pooling Factors and tables of such for the paper ####


### 8.3 Tables of pooling factors for rubin model ###

rm(list = ls())
load("microcredit_rubin_model_temptation_output.RData")
# take in and format the basic bhm estimates from the hetero model
tau_k_names <- c("tau_k[1]","tau_k[2]","tau_k[3]","tau_k[4]","tau_k[5]")

tau_k_bhm <- textable_ols_best_match_coefs_temptation[tau_k_names,1]
sigma2_tau <- (textable_ols_best_match_coefs_temptation["sigma_tau",1])^2
tau <- textable_ols_best_match_coefs_temptation["tau",1]
tau_var <- sigma2_tau

# take in and format OLS (which as we know is MLE for the flat model)

tau_k_ols <- standardised_ols_best_match_output_temptation_coefficients
ols_sd_list <- standardised_ols_best_match_output_temptation_coefficients_sds
ols_var_list <- (ols_sd_list)^2

# basic pooling factor at the k level
omegas <- 1-(tau_var/(tau_var+ols_var_list))

# Now, in fact, I think we may get a different answer if we do a brute solve, becuase we ran on the data not ols_coefs
omegas_brute <- (tau_k_bhm - tau_k_ols)/(tau - tau_k_ols)

tau*omegas_brute[1] + (1-omegas_brute[1])*tau_k_ols[1]


# Gelman and Pardoe Pooling factors at the k level
# remember we have to work with the whole vector of simulated draws here
expected_tau_k_draws <- codafitmatrix_stan_fit_ols_best_match_coefs_temptation[,"tau"]
tau_k_draws <- codafitmatrix_stan_fit_ols_best_match_coefs_temptation[,tau_k_names]   
error_tau <- apply(tau_k_draws, 2, function(x) x- expected_tau_k_draws)

# summaries for the taus
rsquared_tau_k <- 1 - mean (apply (error_tau, 1, var))/ mean (apply (tau_k_draws, 1, var)) # NOT RELEVANT TO TAU's
lambda_tau <- 1 - var (apply (error_tau, 2, mean))/ mean (apply (error_tau, 1, var)) # RELEVANT


#create tables

study_names <- c("Mexico (Angelucci)", "Mongolia (Attanasio)", "Bosnia (Augsberg)", "India (Banerjee)", "Morocco (Crepon)")
object_names <- c("Slope pooling", "(brute)", "Int. pooling", "(brute)")

table_dataframe <- cbind(omegas, omegas_brute)
rownames(table_dataframe) <- study_names
colnames(table_dataframe) <- object_names[1:2]
pooling_table <- xtable(table_dataframe)

sink("microcredit_rubin_best_match_temptation_pooling_factors_table.txt")
print.xtable(pooling_table, digits = 4)
print(mean(omegas, na.rm=TRUE))
print(mean(omegas_brute, na.rm=TRUE))
print(lambda_tau)

sink()


### 8.2 Tables of pooling factors for independent full data model ###
rm(list = ls())
load("microcredit_independent_model_temptation_output.RData")

# take in and format the basic bhm estimates from the hetero model
tau_k_names <- c("tau_k[1]","tau_k[2]","tau_k[3]","tau_k[4]","tau_k[5]")
mu_k_names <- c("mu_k[1]","mu_k[2]","mu_k[3]","mu_k[4]","mu_k[5]")
tau_k_bhm <- textable_temptation_independent[tau_k_names,1]
sigma2_tau <- (textable_temptation_independent["sigma_tau",1])^2
tau <- textable_temptation_independent["tau",1]
tau_var <- sigma2_tau

# take in and format OLS (which as we know is MLE for the flat model)

tau_k_ols <- standardised_ols_best_match_output_temptation_coefficients
ols_sd_list <- standardised_ols_best_match_output_temptation_coefficients_sds
ols_var_list <- (ols_sd_list)^2

# basic pooling factor at the k level
omegas <- 1-(tau_var/(tau_var+ols_var_list))

# Now, in fact, I think we may get a different answer if we do a brute solve, becuase we ran on the data not ols_coefs
omegas_brute <- (tau_k_bhm - tau_k_ols)/(tau - tau_k_ols)

tau*omegas_brute[1] + (1-omegas_brute[1])*tau_k_ols[1]


# Gelman and Pardoe Pooling factors at the k level
# remember we have to work with the whole vector of simulated draws here
expected_tau_k_draws <- codafitmatrix_temptation_independent[,"tau"]
tau_k_draws <- codafitmatrix_temptation_independent[,tau_k_names]   
mu_k_draws <- codafitmatrix_temptation_independent[,mu_k_names]
expected_mu_k_draws <- codafitmatrix_temptation_independent[,"mu"]
expected_y_draws <- tau_k_draws + mu_k_draws

error_tau <- apply(tau_k_draws, 2, function(x) x- expected_tau_k_draws)

# summaries for the taus
rsquared_tau_k <- 1 - mean (apply (error_tau, 1, var))/ mean (apply (tau_k_draws, 1, var)) # NOT RELEVANT TO TAU's
lambda_tau <- 1 - var (apply (error_tau, 2, mean))/ mean (apply (error_tau, 1, var)) # RELEVANT




# Now for the mus! Let's do this as a sanity check 
mu_k_bhm <- textable_temptation_independent[mu_k_names,1]
mu_bhm <- textable_temptation_independent["mu",1]
mu_var <- (textable_temptation_independent["sigma_mu",1])^2
pick.int<- function(x){x$coef[1]}
mu_k_ols <- unlist(lapply(ols_sep_best_match_list,pick.int))
pick.int.se <- function(x){x[1]}
ols_sd_list_mu <- unlist(lapply(ols_sep_robust_se, pick.int.se))
ols_var_list_mu <- (ols_sd_list_mu)^2

omegas_mu <- 1-(mu_var/(mu_var+ols_var_list_mu))
omegas_mu_brute <- (mu_k_bhm - mu_k_ols)/(mu_bhm - mu_k_ols)

# summaries for the mus
error_mu <- mu_k_draws - expected_mu_k_draws
lambda_mu <- 1 - var (apply (error_mu, 2, mean))/ mean (apply (error_mu, 1, var)) # RELEVANT



#create tables

study_names <- c("Mexico (Angelucci)", "Mongolia (Attanasio)", "Bosnia (Augsberg)", "India (Banerjee)", "Morocco (Crepon)")
object_names <- c("Slope pooling", "(brute)", "Int. pooling", "(brute)")

table_dataframe <- cbind(omegas, omegas_brute, omegas_mu, omegas_mu_brute)
rownames(table_dataframe) <- study_names
colnames(table_dataframe) <- object_names
pooling_table <- xtable(table_dataframe)

sink("microcredit_independent_temptation_pooling_factors_table.txt")
print.xtable(pooling_table, digits = 4)
print(mean(omegas, na.rm=TRUE))
print(mean(omegas_brute, na.rm=TRUE))
print(mean(omegas_mu, na.rm=TRUE))
print(mean(omegas_mu_brute, na.rm=TRUE))
print(lambda_tau)
print(lambda_mu)
sink()



### 8.3 Tables of pooling factors for joint full data model ###

# Load data
rm(list = ls())
load("microcredit_joint_model_temptation_output.RData")

# take in and format the basic bhm estimates from the hetero model
tau_k_names <- c("mutau_k[1,2]","mutau_k[2,2]","mutau_k[3,2]","mutau_k[4,2]","mutau_k[5,2]")
mu_k_names <- c("mutau_k[1,1]","mutau_k[2,1]","mutau_k[3,1]","mutau_k[4,1]","mutau_k[5,1]")
tau_k_bhm <- textable_temptation_joint[tau_k_names,1]
sigma2_tau <- textable_temptation_joint["sigma_mutau[2,2]",1]
tau <- textable_temptation_joint["mutau[2]",1]
tau_var <- sigma2_tau

# take in and format OLS (which as we know is MLE for the flat model)

tau_k_ols <- standardised_ols_best_match_output_temptation_coefficients
ols_sd_list <- standardised_ols_best_match_output_temptation_coefficients_sds
ols_var_list <- (ols_sd_list)^2

# basic pooling factor at the k level
omegas <- 1-(tau_var/(tau_var+ols_var_list))

# Now, in fact, I think we may get a different answer if we do a brute solve, becuase we ran on the data not ols_coefs
omegas_brute <- (tau_k_bhm - tau_k_ols)/(tau - tau_k_ols)

tau*omegas_brute[1] + (1-omegas_brute[1])*tau_k_ols[1]


# Gelman and Pardoe Pooling factors at the k level
# remember we have to work with the whole vector of simulated draws here
expected_tau_k_draws <- codafitmatrix_temptation_joint[,"mutau[2]"]
tau_k_draws <- codafitmatrix_temptation_joint[,tau_k_names]   
mu_k_draws <- codafitmatrix_temptation_joint[,mu_k_names]
expected_mu_k_draws <- codafitmatrix_temptation_joint[,"mutau[1]"]
expected_y_draws <- tau_k_draws + mu_k_draws

error_tau <- apply(tau_k_draws, 2, function(x) x- expected_tau_k_draws)

# summaries for the taus
lambda_tau <- 1 - var (apply (error_tau, 2, mean))/ mean (apply (error_tau, 1, var)) 




# Now for the mus! Let's do this as a sanity check 
mu_k_bhm <- textable_temptation_joint[mu_k_names,1]
mu_bhm <- textable_temptation_joint["mutau[1]",1]
mu_var <- textable_temptation_joint["sigma_mutau[1,1]",1]
pick.int<- function(x){x$coef[1]}
mu_k_ols <- unlist(lapply(ols_sep_best_match_list,pick.int))
pick.int.se <- function(x){x[1]}
ols_sd_list_mu <- unlist(lapply(ols_sep_robust_se, pick.int.se))
ols_var_list_mu <- (ols_sd_list_mu)^2

omegas_mu <- 1-(mu_var/(mu_var+ols_var_list_mu))
omegas_mu_brute <- (mu_k_bhm - mu_k_ols)/(mu_bhm - mu_k_ols)

# summaries for the mus
error_mu <- mu_k_draws - expected_mu_k_draws
lambda_mu <- 1 - var (apply (error_mu, 2, mean))/ mean (apply (error_mu, 1, var)) # RELEVANT



#create tables

study_names <- c("Mexico (Angelucci)", "Mongolia (Attanasio)", "Bosnia (Augsberg)", "India (Banerjee)", "Morocco (Crepon)")
object_names <- c("Slope pooling", "(brute)", "Int. pooling", "(brute)")

table_dataframe <- cbind(omegas, omegas_brute, omegas_mu, omegas_mu_brute)
rownames(table_dataframe) <- study_names
colnames(table_dataframe) <- object_names
pooling_table <- xtable(table_dataframe)

sink("microcredit_joint_temptation_pooling_factors_table.txt")
print.xtable(pooling_table, digits = 4)
print(mean(omegas, na.rm=TRUE))
print(mean(omegas_brute, na.rm=TRUE))
print(mean(omegas_mu, na.rm=TRUE))
print(mean(omegas_mu_brute, na.rm=TRUE))
print(lambda_tau)
print(lambda_mu)
sink()

### 8.4 Tables of pooling factors for joint reduced ###

# Load data
rm(list = ls())
load("microcredit_joint_model_reduced_data_temptation.RData")

# take in and format the basic bhm estimates from the hetero model
tau_k_names <- c("mutau_k[1,2]","mutau_k[2,2]","mutau_k[3,2]","mutau_k[4,2]","mutau_k[5,2]")
mu_k_names <- c("mutau_k[1,1]","mutau_k[2,1]","mutau_k[3,1]","mutau_k[4,1]","mutau_k[5,1]")
tau_k_bhm <- textable_temptation_joint_reduced[tau_k_names,1]
sigma2_tau <- textable_temptation_joint_reduced["sigma_mutau[2,2]",1]
tau <- textable_temptation_joint_reduced["mutau[2]",1]
tau_var <- sigma2_tau

# take in and format OLS (which as we know is MLE for the flat model)

tau_k_ols <- standardised_ols_best_match_output_temptation_coefficients
ols_sd_list <- standardised_ols_best_match_output_temptation_coefficients_sds
ols_var_list <- (ols_sd_list)^2

# basic pooling factor at the k level
omegas <- 1-(tau_var/(tau_var+ols_var_list))

# Now, in fact, I think we may get a different answer if we do a brute solve, becuase we ran on the data not ols_coefs
omegas_brute <- (tau_k_bhm - tau_k_ols)/(tau - tau_k_ols)

tau*omegas_brute[1] + (1-omegas_brute[1])*tau_k_ols[1]


# Gelman and Pardoe Pooling factors at the k level
# remember we have to work with the whole vector of simulated draws here
expected_tau_k_draws <- codafitmatrix_stan_fit_temptation_joint_reduced[,"mutau[2]"]
tau_k_draws <- codafitmatrix_stan_fit_temptation_joint_reduced[,tau_k_names]   
mu_k_draws <- codafitmatrix_stan_fit_temptation_joint_reduced[,mu_k_names]
expected_mu_k_draws <- codafitmatrix_stan_fit_temptation_joint_reduced[,"mutau[1]"]
expected_y_draws <- tau_k_draws + mu_k_draws

error_tau <- apply(tau_k_draws, 2, function(x) x- expected_tau_k_draws)

# summaries for the taus
lambda_tau <- 1 - var (apply (error_tau, 2, mean))/ mean (apply (error_tau, 1, var)) 




# Now for the mus! Let's do this as a sanity check 
mu_k_bhm <- textable_temptation_joint_reduced[mu_k_names,1]
mu_bhm <- textable_temptation_joint_reduced["mutau[1]",1]
mu_var <- textable_temptation_joint_reduced["sigma_mutau[1,1]",1]
pick.int<- function(x){x$coef[1]}
mu_k_ols <- unlist(lapply(ols_sep_best_match_list,pick.int))
pick.int.se <- function(x){x[1]}
ols_sd_list_mu <- unlist(lapply(ols_sep_robust_se, pick.int.se))
ols_var_list_mu <- (ols_sd_list_mu)^2

omegas_mu <- 1-(mu_var/(mu_var+ols_var_list_mu))
omegas_mu_brute <- (mu_k_bhm - mu_k_ols)/(mu_bhm - mu_k_ols)

# summaries for the mus
error_mu <- mu_k_draws - expected_mu_k_draws
lambda_mu <- 1 - var (apply (error_mu, 2, mean))/ mean (apply (error_mu, 1, var)) # RELEVANT



#create tables

study_names <- c("Mexico (Angelucci)", "Mongolia (Attanasio)", "Bosnia (Augsberg)", "India (Banerjee)", "Morocco (Crepon)")
object_names <- c("Slope pooling", "(brute)", "Int. pooling", "(brute)")

table_dataframe <- cbind(omegas, omegas_brute, omegas_mu, omegas_mu_brute)
rownames(table_dataframe) <- study_names
colnames(table_dataframe) <- object_names
pooling_table <- xtable(table_dataframe)

sink("microcredit_joint_temptation_pooling_factors_reduced_data_table.txt")   
print.xtable(pooling_table, digits = 4)
print("NOTE THIS IS FROM THE REDUCED DATA JOINT MODEL AND THIS IS THE BETTER COMPARISON")
print(mean(omegas, na.rm=TRUE))
print(mean(omegas_brute, na.rm=TRUE))
print(mean(omegas_mu, na.rm=TRUE))
print(mean(omegas_mu_brute, na.rm=TRUE))
print(lambda_tau)
print(lambda_mu)
sink()


### 8.5 Tables of pooling factors for independent reduced ###

# Load data
rm(list = ls())
load("microcredit_optimised_independent_dist_model_temptation_output.RData")

# take in and format the basic bhm estimates from the hetero model
tau_k_names <- c("tau_k[1]","tau_k[2]","tau_k[3]","tau_k[4]","tau_k[5]")
mu_k_names <- c("mu_k[1]","mu_k[2]","mu_k[3]","mu_k[4]","mu_k[5]")
tau_k_bhm <- textable_temptation_independent_reduced[tau_k_names,1]
sigma2_tau <- (textable_temptation_independent_reduced["sigma_tau",1])^2
tau <- textable_temptation_independent_reduced["tau",1]
tau_var <- sigma2_tau

# take in and format OLS (which as we know is MLE for the flat model)

tau_k_ols <- standardised_ols_best_match_output_temptation_coefficients
ols_sd_list <- standardised_ols_best_match_output_temptation_coefficients_sds
ols_var_list <- (ols_sd_list)^2

# basic pooling factor at the k level
omegas <- 1-(tau_var/(tau_var+ols_var_list))

# Now, in fact, I think we may get a different answer if we do a brute solve, becuase we ran on the data not ols_coefs
omegas_brute <- (tau_k_bhm - tau_k_ols)/(tau - tau_k_ols)

tau*omegas_brute[1] + (1-omegas_brute[1])*tau_k_ols[1]


# Gelman and Pardoe Pooling factors at the k level
# remember we have to work with the whole vector of simulated draws here
expected_tau_k_draws <- codafitmatrix_stan_fit_temptation_independent_reduced[,"tau"]
tau_k_draws <- codafitmatrix_stan_fit_temptation_independent_reduced[,tau_k_names]   
mu_k_draws <- codafitmatrix_stan_fit_temptation_independent_reduced[,mu_k_names]
expected_mu_k_draws <- codafitmatrix_stan_fit_temptation_independent_reduced[,"mu"]
expected_y_draws <- tau_k_draws + mu_k_draws

error_tau <- apply(tau_k_draws, 2, function(x) x- expected_tau_k_draws)

# summaries for the taus
rsquared_tau_k <- 1 - mean (apply (error_tau, 1, var))/ mean (apply (tau_k_draws, 1, var)) # NOT RELEVANT TO TAU's
lambda_tau <- 1 - var (apply (error_tau, 2, mean))/ mean (apply (error_tau, 1, var)) # RELEVANT




# Now for the mus! Let's do this as a sanity check 
mu_k_bhm <- textable_temptation_independent_reduced[mu_k_names,1]
mu_bhm <- textable_temptation_independent_reduced["mu",1]
mu_var <- (textable_temptation_independent_reduced["sigma_mu",1])^2
pick.int<- function(x){x$coef[1]}
mu_k_ols <- unlist(lapply(ols_sep_best_match_list,pick.int))
pick.int.se <- function(x){x[1]}
ols_sd_list_mu <- unlist(lapply(ols_sep_robust_se, pick.int.se))
ols_var_list_mu <- (ols_sd_list_mu)^2

omegas_mu <- 1-(mu_var/(mu_var+ols_var_list_mu))
omegas_mu_brute <- (mu_k_bhm - mu_k_ols)/(mu_bhm - mu_k_ols)

# summaries for the mus
error_mu <- mu_k_draws - expected_mu_k_draws
lambda_mu <- 1 - var (apply (error_mu, 2, mean))/ mean (apply (error_mu, 1, var)) # RELEVANT



#create tables

study_names <- c("Mexico (Angelucci)", "Mongolia (Attanasio)", "Bosnia (Augsberg)", "India (Banerjee)", "Morocco (Crepon)")
object_names <- c("Slope pooling", "(brute)", "Int. pooling", "(brute)")

table_dataframe <- cbind(omegas, omegas_brute, omegas_mu, omegas_mu_brute)
rownames(table_dataframe) <- study_names
colnames(table_dataframe) <- object_names
pooling_table <- xtable(table_dataframe)

sink("microcredit_independent_temptation_pooling_factors_table_reduced.txt")
print.xtable(pooling_table, digits = 4)
print(mean(omegas, na.rm=TRUE))
print(mean(omegas_brute, na.rm=TRUE))
print(mean(omegas_mu, na.rm=TRUE))
print(mean(omegas_mu_brute, na.rm=TRUE))
print(lambda_tau)
print(lambda_mu)
sink()



### 8.6 Tables of pooling factors for split priorbiz model ###

rm(list = ls())
load("microcredit_independent_model_priorbiz_split_temptation_output.RData")

# let's get the OLS 


angelucci_temptation_regression_split_priorbiz <- (lm(angelucci_temptation*the_temptation_standardiser_USD_PPP_per_fortnight[1] ~ angelucci_existingbusiness + angelucci_treatment + angelucci_existingbusiness*angelucci_treatment))


attanasio_temptation_regression_split_priorbiz <- (lm(attanasio_temptation*the_temptation_standardiser_USD_PPP_per_fortnight[2] ~ attanasio_existingbusiness + attanasio_treatment + attanasio_existingbusiness*attanasio_treatment))

augsberg_temptation_regression_split_priorbiz <- (lm(augsberg_temptation*the_temptation_standardiser_USD_PPP_per_fortnight[3] ~ augsberg_existingbusiness + augsberg_treatment + augsberg_existingbusiness*augsberg_treatment))

banerjee_temptation_regression_split_priorbiz <- (lm(banerjee_temptation*the_temptation_standardiser_USD_PPP_per_fortnight[4] ~ banerjee_existingbusiness + banerjee_treatment + banerjee_existingbusiness*banerjee_treatment))

crepon_temptation_regression_split_priorbiz <- (lm(crepon_temptation*the_temptation_standardiser_USD_PPP_per_fortnight[5] ~ crepon_existingbusiness + crepon_treatment + crepon_existingbusiness*crepon_treatment))



ols_sep_priorbiz_split <- list(angelucci_temptation_regression_split_priorbiz, 
                               attanasio_temptation_regression_split_priorbiz, 
                               augsberg_temptation_regression_split_priorbiz,
                               banerjee_temptation_regression_split_priorbiz, 
                               crepon_temptation_regression_split_priorbiz
                             )



ols_sep_se_priorbiz_split_list <- lapply(ols_sep_priorbiz_split, vcovHC,type = "HC")

sqrt.diag <- function(x){sqrt(diag(x))}

ols_sep_robust_se_priorbiz_split <- lapply(ols_sep_se_priorbiz_split_list, sqrt.diag)

mu.picker <- function(x){x$coef[1]}
ols_mu_k <- lapply(ols_sep_priorbiz_split,mu.picker )

mup.picker <- function(x){x$coef[2]}
ols_mup_k <- lapply(ols_sep_priorbiz_split,mup.picker )

tau.picker <- function(x){x$coef[3]}
ols_tau_k <- lapply(ols_sep_priorbiz_split,tau.picker )

taup.picker <- function(x){x$coef[4]}
ols_taup_k <- lapply(ols_sep_priorbiz_split,taup.picker )

mu.se.picker <- function(x){x[1]}
ols_mu_k_se <- lapply(ols_sep_robust_se_priorbiz_split,mu.se.picker )

mup.se.picker <- function(x){x[2]}
ols_mup_k_se <- lapply(ols_sep_robust_se_priorbiz_split,mup.se.picker )

tau.se.picker <- function(x){x[1]}
ols_tau_k_se <- lapply(ols_sep_robust_se_priorbiz_split,tau.se.picker )

taup.se.picker <- function(x){x[2]}
ols_taup_k_se <- lapply(ols_sep_robust_se_priorbiz_split,taup.se.picker )

# take in and format the basic bhm estimates from the hetero model
tau_k_names <- c("tau_k_block[1,1]","tau_k_block[2,1]","tau_k_block[3,1]","tau_k_block[4,1]","tau_k_block[5,1]")
mu_k_names <- c("mu_k_block[1,1]","mu_k_block[2,1]","mu_k_block[3,1]","mu_k_block[4,1]","mu_k_block[5,1]")
taup_k_names <- c("tau_k_block[1,2]","tau_k_block[2,2]","tau_k_block[3,2]","tau_k_block[4,2]","tau_k_block[5,2]")
mup_k_names <- c("mu_k_block[1,2]","mu_k_block[2,2]","mu_k_block[3,2]","mu_k_block[4,2]","mu_k_block[5,2]")

tau_k_bhm <- textable_stan_fit_temptation_priorbiz_split[tau_k_names,1]
sigma2_tau <- (textable_stan_fit_temptation_priorbiz_split["sigma_tau_block[1]",1])^2
tau <- textable_stan_fit_temptation_priorbiz_split["tau_block[1]",1]
tau_var <- sigma2_tau
taup_k_bhm <- textable_stan_fit_temptation_priorbiz_split[taup_k_names,1]
sigma2_taup <- (textable_stan_fit_temptation_priorbiz_split["sigma_tau_block[2]",1])^2
taup <- textable_stan_fit_temptation_priorbiz_split["tau_block[2]",1]
taup_var <- sigma2_taup

mu_k_bhm <- textable_stan_fit_temptation_priorbiz_split[mu_k_names,1]
sigma2_mu <- (textable_stan_fit_temptation_priorbiz_split["sigma_mu_block[1]",1])^2
mu <- textable_stan_fit_temptation_priorbiz_split["mu_block[1]",1]
mu_var <- sigma2_mu
mup_k_bhm <- textable_stan_fit_temptation_priorbiz_split[mup_k_names,1]
sigma2_mup <- (textable_stan_fit_temptation_priorbiz_split["sigma_mu_block[2]",1])^2
mup <- textable_stan_fit_temptation_priorbiz_split["mu_block[2]",1]
mup_var <- sigma2_mup

# take in and format OLS (which as we know is MLE for the flat model)

tau_k_ols <- unlist(ols_tau_k)
ols_sd_list_tau <- unlist(ols_tau_k_se) 
ols_var_list_tau <- (ols_sd_list_tau)^2

taup_k_ols <- unlist(ols_taup_k)
ols_sd_list_taup <- unlist(ols_taup_k_se)
ols_var_list_taup <- (ols_sd_list_tau)^2

mu_k_ols <- unlist(ols_mu_k)
ols_sd_list_mu <- unlist(ols_mu_k_se) 
ols_var_list_mu <- (ols_sd_list_tau)^2

mup_k_ols <- unlist(ols_mup_k)
ols_sd_list_mup <- unlist(ols_mup_k_se )
ols_var_list_mup <- (ols_sd_list_tau)^2


# basic pooling factor at the k level
omegas_tau <- 1-(tau_var/(tau_var+ols_var_list_tau))

# Now, in fact, I think we may get a different answer if we do a brute solve, becuase we ran on the data not ols_coefs
omegas_tau_brute <- (tau_k_bhm - tau_k_ols)/(tau - tau_k_ols)


# Gelman and Pardoe Pooling factors at the k level
# remember we have to work with the whole vector of simulated draws here
expected_tau_k_draws <- codafitmatrix_temptation_priorbiz_split[,"tau_block[1]"]
tau_k_draws <- codafitmatrix_temptation_priorbiz_split[,tau_k_names]   

error_tau <- apply(tau_k_draws, 2, function(x) x- expected_tau_k_draws)
lambda_tau <- 1 - var (apply (error_tau, 2, mean))/ mean (apply (error_tau, 1, var)) # RELEVANT


# Now for the mus! Let's do this as a sanity check 

omegas_mu <- 1-(mu_var/(mu_var+ols_var_list_mu))
omegas_mu_brute <- (mu_k_bhm - mu_k_ols)/(mu - mu_k_ols)

# summaries for the mus
mu_k_draws <- codafitmatrix_temptation_priorbiz_split[,mu_k_names]
expected_mu_k_draws <- codafitmatrix_temptation_priorbiz_split[,"mu_block[1]"]
error_mu <- mu_k_draws - expected_mu_k_draws
lambda_mu <- 1 - var (apply (error_mu, 2, mean))/ mean (apply (error_mu, 1, var)) 


# basic pooling factor at the k level
omegas_taup <- 1-(taup_var/(taup_var+ols_var_list_taup))

# Now, in fact, I think we may get a different answer if we do a brute solve, becuase we ran on the data not ols_coefs
omegas_taup_brute <- (taup_k_bhm - taup_k_ols)/(taup - taup_k_ols)



# Gelman and Pardoe Pooling factors at the k level
# remember we have to work with the whole vector of simuplated draws here
expected_taup_k_draws <- codafitmatrix_temptation_priorbiz_split[,"tau_block[2]"]
taup_k_draws <- codafitmatrix_temptation_priorbiz_split[,taup_k_names]   

error_taup <- apply(taup_k_draws, 2, function(x) x- expected_taup_k_draws)
lambda_taup <- 1 - var (apply (error_taup, 2, mean))/ mean (apply (error_taup, 1, var)) # RELEVANT


# Now for the mups! Let's do this as a sanity check 

omegas_mup <- 1-(mup_var/(mup_var+ols_var_list_mup))
omegas_mup_brute <- (mup_k_bhm - mup_k_ols)/(mup - mup_k_ols)

# summaries for the mups
mup_k_draws <- codafitmatrix_temptation_priorbiz_split[,mup_k_names]
expected_mup_k_draws <- codafitmatrix_temptation_priorbiz_split[,"mu_block[2]"]
error_mup <- mup_k_draws - expected_mup_k_draws
lambda_mup <- 1 - var (apply (error_mup, 2, mean))/ mean (apply (error_mup, 1, var)) 


#create tables

study_names <- c("Mexico (Angelucci)", "Mongolia (Attanasio)", "Bosnia (Augsberg)", "India (Banerjee)", "Morocco (Crepon)")
object_names <- c("Slope pooling", "(brute)", "Int. pooling", "(brute)","Slope pooling (p)", "(brute) (p)", "Int. pooling (p)", "(brute) (p)")

table_dataframe <- cbind(omegas_tau, omegas_tau_brute, omegas_mu, omegas_mu_brute,omegas_taup, omegas_taup_brute, omegas_mup, omegas_mup_brute)
rownames(table_dataframe) <- study_names
colnames(table_dataframe) <- object_names
pooling_table <- xtable(table_dataframe)

sink("microcredit_priorbiz_temptation_pooling_factors_table.txt")
print.xtable(pooling_table, digits = 4)
print(mean(omegas_tau, na.rm=TRUE))
print(mean(omegas_tau_brute, na.rm=TRUE))
print(mean(omegas_mu, na.rm=TRUE))
print(mean(omegas_mu_brute, na.rm=TRUE))
print(lambda_tau)
print(lambda_mu)
print(mean(omegas_taup, na.rm=TRUE))
print(mean(omegas_taup_brute, na.rm=TRUE))
print(mean(omegas_mup, na.rm=TRUE))
print(mean(omegas_mup_brute, na.rm=TRUE))
print(lambda_taup)
print(lambda_mup)
sink()

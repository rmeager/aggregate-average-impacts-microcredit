
# Check Replications in Microcredit Meta-Analysis Project temptation 
# Rachael Meager
# First Version: August 2015
# This Version: August 2015

### Notes ###

# This code loads variables from .dta files from 7 RCTs of microcredit
# All data was publicly available from the authors and journals, without whom this project would not be possible. Hooray for open science!
# I use "i" as the general index for household-level variables and "k" as the index for site-level variables


# A note about NAs: I have tried to keep NAs as NAs, except in TWO CASES. 
# 1. temptation/Business expenses/temptation. An NA in here can indicate not operating a business and we want to capture that in a zero.
# 2. Variables constructed by summing other variables should not be NA unless *ALL* of the constituent variables are NA.


### Preliminaries ###

rm(list = ls())

chooseCRANmirror(ind=90)

setwd("//bbkinghome/rmeager/winprofile/mydocs/Research work/bayesian meta analysis/microcredit analysis")
installer <- FALSE
if(installer == TRUE){
  install.packages('foreign')
  install.packages('Hmisc')
  install.packages('xtable')
  install.packages('coda')
  install.packages('stargazer')
  install.packages('dummies')
  install.packages('zoo')
  install.packages('lmtest')
  install.packages('plm')
  install.packages('sandwich')
  install.packages('Matrix')
  install.packages("ggplot2")
  install.packages("gridExtra")}


library(dummies)
library(multiwayvcov)
library(stargazer)
library(foreign)
library(Hmisc)
library(xtable)
library(coda)
library(zoo)
library(plm)
library(lmtest)
library(sandwich)
library(ggplot2)
library(gridExtra)
library(Matrix)

## load data ##

load("microcredit_project_data.RData")

angelucci_temptation_regression <- summary(lm(angelucci_temptation ~ angelucci_treatment))
print(angelucci_temptation_regression)
attanasio_temptation_regression <- summary(lm(attanasio_temptation ~ attanasio_treatment))
print(attanasio_temptation_regression)
augsberg_temptation_regression <- summary(lm(augsberg_temptation ~ augsberg_treatment))
print(augsberg_temptation_regression)
banerjee_temptation_regression <- summary(lm(banerjee_temptation ~ banerjee_treatment))
print(banerjee_temptation_regression)
crepon_temptation_regression <- summary(lm(crepon_temptation ~ crepon_treatment))
print(crepon_temptation_regression)


ols_output_temptation <- list(angelucci_temptation_regression, attanasio_temptation_regression, augsberg_temptation_regression,
                          banerjee_temptation_regression, crepon_temptation_regression)

pick.coefficient.from.summary <- function(lm_summary_object){  return(lm_summary_object$coef[2,1])}
pick.coefficient.sd.from.summary <- function(lm_summary_object){  return(lm_summary_object$coef[2,2])}
pick.mean.from.summary <- function(lm_summary_object){  return(lm_summary_object$coef[1,1])}
pick.mean.sd.from.summary <- function(lm_summary_object){  return(lm_summary_object$coef[1,2])}

ols_output_temptation_coefficients <- unlist(lapply(ols_output_temptation, pick.coefficient.from.summary))
ols_output_temptation_coefficients_sds <- unlist(lapply(ols_output_temptation, pick.coefficient.sd.from.summary))

standardised_ols_output_temptation_coefficients <- the_temptation_standardiser_USD_PPP_per_fortnight*ols_output_temptation_coefficients
standardised_ols_output_temptation_coefficients_sds <- the_temptation_standardiser_USD_PPP_per_fortnight*ols_output_temptation_coefficients_sds

ols_output_temptation_means <- unlist(lapply(ols_output_temptation, pick.mean.from.summary))
ols_output_temptation_means_sds <- unlist(lapply(ols_output_temptation, pick.mean.sd.from.summary))

standardised_ols_output_temptation_means <- the_temptation_standardiser_USD_PPP_per_fortnight*ols_output_temptation_means
standardised_ols_output_temptation_means_sds <- the_temptation_standardiser_USD_PPP_per_fortnight*ols_output_temptation_means_sds



## BEST MATCH TABLE ##

ols_sep_best_match_list <- list (lm(angelucci_temptation*the_temptation_standardiser_USD_PPP_per_fortnight[1] ~ angelucci_treatment), 
                             lm(attanasio_temptation*the_temptation_standardiser_USD_PPP_per_fortnight[2] ~ attanasio_treatment),
                              lm(augsberg_temptation*the_temptation_standardiser_USD_PPP_per_fortnight[3] ~ augsberg_treatment), 
                             lm(banerjee_temptation*the_temptation_standardiser_USD_PPP_per_fortnight[4] ~ banerjee_treatment),
                              lm(crepon_temptation*the_temptation_standardiser_USD_PPP_per_fortnight[5] ~ crepon_treatment))

ols_sep_se_best_match_list <- lapply(ols_sep_best_match_list, vcovHC,type = "HC")

sqrt.diag <- function(x){sqrt(diag(x))}

ols_sep_robust_se <- lapply(ols_sep_se_best_match_list, sqrt.diag)

pick.se.from.robust.se <- function(robust_se_object){robust_se_object[2]}
pick.mean.se.from.robust.se <- function(robust_se_object){robust_se_object[1]}

sink("microcredit_ols_sep_basic_temptation_regressions.txt")
stargazer(ols_sep_best_match_list, se=ols_sep_robust_se,
          
          #ci=TRUE,
          colnames = NULL,
          column.labels = study_country)
sink()



### NOW RE-ENTER temptation DATA IN ORDER TO HAVE THE BEST MATCH NAMED AS "temptation" IN THE DATA ###

# no change needed


# and re-name the coefficients output

angelucci_temptation_regression <- summary(lm(angelucci_temptation ~ angelucci_treatment))
attanasio_temptation_regression <- summary(lm(attanasio_temptation ~ attanasio_treatment))
augsberg_temptation_regression <- summary(lm(augsberg_temptation ~ augsberg_treatment))
banerjee_temptation_regression <- summary(lm(banerjee_temptation ~ banerjee_treatment))
crepon_temptation_regression <- summary(lm(crepon_temptation ~ crepon_treatment))


ols_best_match_output_temptation <- list(angelucci_temptation_regression, attanasio_temptation_regression, augsberg_temptation_regression,
                          banerjee_temptation_regression, crepon_temptation_regression)

ols_output_temptation_coefficients <- unlist(lapply(ols_best_match_output_temptation, pick.coefficient.from.summary))
ols_output_temptation_coefficients_sds <- unlist(lapply(ols_sep_robust_se, pick.se.from.robust.se))

ols_output_temptation_means <- unlist(lapply(ols_best_match_output_temptation, pick.mean.from.summary))
ols_output_temptation_means_sds <- unlist(lapply(ols_sep_robust_se, pick.mean.se.from.robust.se))

standardised_ols_best_match_output_temptation_coefficients <- the_temptation_standardiser_USD_PPP_per_fortnight*ols_output_temptation_coefficients
standardised_ols_best_match_output_temptation_coefficients_sds <- ols_output_temptation_coefficients_sds # do not re-standardise! already standardised!! 

standardised_ols_best_match_output_temptation_means <- the_temptation_standardiser_USD_PPP_per_fortnight*ols_output_temptation_means
standardised_ols_best_match_output_temptation_means_sds <- ols_output_temptation_means_sds # do not re-standardise! already standardised!! 


# and now write the pooled regression because we will want it later

## POOLED OLS REGRESSION ##

# This preps data so that the model can be written efficiently - it's not necessary but it's the best way I know to code it in STAN
site <- c(angelucci_indicator, attanasio_indicator, augsberg_indicator,banerjee_indicator, crepon_indicator)
temptation <- c(angelucci_temptation, attanasio_temptation, augsberg_temptation,banerjee_temptation, crepon_temptation)
treatment <- c(angelucci_treatment, attanasio_treatment, augsberg_treatment,banerjee_treatment, crepon_treatment)

# Now we have to standardise any variables which are in local currency units to USD PPP per fortnight in 2009 dollars


expanded_standardiser_USD_PPP_per_fortnight <- c( rep(the_temptation_standardiser_USD_PPP_per_fortnight[1],length(angelucci_indicator)),
                                                  rep(the_temptation_standardiser_USD_PPP_per_fortnight[2],length(attanasio_indicator)),
                                                  rep(the_temptation_standardiser_USD_PPP_per_fortnight[3],length(augsberg_indicator)),
                                                  rep(the_temptation_standardiser_USD_PPP_per_fortnight[4],length(banerjee_indicator)),
                                                  rep(the_temptation_standardiser_USD_PPP_per_fortnight[5],length(crepon_indicator))
                                                 )

temptation <- temptation*expanded_standardiser_USD_PPP_per_fortnight

# bind everything into a data frame
data <- data.frame(site, temptation, treatment)

# We gotta remove the NA values
data <- data[complete.cases(data),]

ols_pooled <- lm(data$temptation ~ data$treatment + factor(data$site))
summary(ols_pooled)
# now we cluster the standard errors! 

ols_cluster_wild_boot <- cluster.boot(ols_pooled, data$site, parallel = FALSE, use_white = NULL,
                                      force_posdef = FALSE, R = 1000, boot_type = "wild",
                                      wild_type = "rademacher", debug = FALSE)
ols_clustered_vcov <- cluster.vcov(ols_pooled, data$site)

ols_pooled_cluster_sd <- sqrt(diag(ols_cluster_wild_boot))


sink("ols_pooled_regression_temptation_best_match_cluster_ses.txt")
stargazer(ols_pooled, se = list(ols_pooled_cluster_sd, NULL)) 
sink()

sink("ols_pooled_regression_temptation_best_match.txt")
stargazer(ols_pooled)
sink()

# now extract coefficinets from the big regression, which I need to compare to bayes

library(dummies)
site_dummies <- dummy(data$site)
data_with_dummies <- data.frame(data, site_dummies)

standardised_ols_best_match_results_from_pooled_data <- lm(data$temptation ~ factor(data$site) + data$treatment + factor(data$site)*data$treatment )

summary(lm(data$temptation ~ site_dummies + data$treatment + site_dummies*data$treatment ))
# now we cluster the standard errors! 

ols_cluster_wild_boot <- cluster.boot(ols_pooled, data$site, parallel = FALSE, use_white = NULL,
                                      force_posdef = FALSE, R = 1000, boot_type = "wild",
                                      wild_type = "rademacher", debug = FALSE)
ols_clustered_vcov <- cluster.vcov(ols_pooled, data$site)

ols_pooled_cluster_sd <- sqrt(diag(ols_cluster_wild_boot))


save.image(file = "microcredit_project_data_with_replication_temptation.RData")

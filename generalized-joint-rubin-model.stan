

data {
  int<lower=0> K;  // number of sites
  int<lower=0> P;  // dimensionality of parameter vector which is jointly distributed - here, it is 2 dimensional
  real mu_k_hat[K] ; // means
  real tau_k_hat[K] ; // slopes
  real se_mu_k[K] ; // ses for mus
  real se_tau_k[K] ; // ses for taus
  matrix[P,P] mutau_prior_sigma;
  vector[P] mutau_prior_mean;
}


parameters {
  vector[P] mutau;
  matrix[K,P] mutau_k;
  corr_matrix[P] Omega;        //  correlation
  vector<lower=0>[P] theta;    //  scale
}
transformed parameters {
  matrix[P,P] sigma_mutau;
  sigma_mutau <- quad_form_diag(Omega,theta);
}

model {
 

  // parameter variance priors
  // XXX: change this?
  theta ~ cauchy(0,10);
  // theta ~ normal(0,100);
  Omega ~ lkj_corr(3); // pushes towards independence 

  // hyperparameter priors
  mutau ~ multi_normal(mutau_prior_mean, mutau_prior_sigma);

  for (k in 1:K) {
    mutau_k[k] ~ multi_normal(mutau, sigma_mutau);
     mu_k_hat[k] ~ normal(mutau_k[k,1],se_mu_k[k]);
      tau_k_hat[k] ~ normal(mutau_k[k,2],se_tau_k[k]);
  }
  
 
}

# FULL BASIC ANALYSIS FOR THE MICROCREDIT BAYES PAPER IN 1 PLACE :
# This one is for expenditures but serves as the general template
# Rachael Meager
# First Version: August 2015

### Notes ###

# Data needs to be taken in and prepped before this code is run
# Replication file must be run before this code is run
# RStan must be installed and functional before running this code.


#### Preliminaries ####
rm(list = ls())

chooseCRANmirror(ind=90)
installer <- FALSE
if (installer==TRUE) {
  install.packages('foreign')
  install.packages('Hmisc')
  install.packages('xtable')
  install.packages('coda')
  install.packages('stargazer')
  install.packages('sandwich')
  install.packages('multiwayvcov')
  install.packages('parallel')
  install.packages("ggplot2")
  install.packages("gridExtra")
  install.packages('coda')
}

library(multiwayvcov)
library(sandwich)
library(stargazer)
library(foreign)
library(Hmisc)
library(xtable)
library(coda)
library(parallel)
library(ggplot2)
library(gridExtra)
library(coda)
library(dummies)
library(zoo)
library(Matrix)

library(dplyr)

# Call RStan
library(rstan)




#### Rubin model on reported and replicated coefficients #### 

# Load data
rm(list = ls())
load("microcredit_project_data_with_replication_expenditures.RData")
if( exists("USD_convert_to_2009_dollars")=="FALSE"){stop("OLD DATA FILE LOADED! RELOAD CORRECT FILE!")}


# Set the model file and Stan parameters

model_file <- file.path('rubin-model-code.stan')
model <- stan_model(model_file)

# some knobs we can tweak
chains <- 4
iters <- 20000
control <- list(adapt_t0 = 10,       # default = 10
                stepsize = 1,        # default = 1
                max_treedepth = 6)   # default = 10
seed <- 42



### original studies reported coefficients ###

reported_coefs_exist <- FALSE
if(reported_coefs_exist == TRUE) {
reported_coefs_data_expenditures <- list(K = 7, 
                            tau_hat_k = standardised_reported_coefficients_expenditures,
                            se_k = standardised_reported_coefficients_expenditures_sds)


sflist <-
  mclapply(1:chains, mc.cores = chains,
           function(i) sampling(model, data = reported_coefs_data_expenditures, seed = seed,
                                chains = 1, chain_id = i, # refresh = -1,
                                iter = iters, control = control))
stan_fit_reported_coefs_expenditures <- sflist2stanfit(sflist)

# This will display the posterior inference directly
print(stan_fit_reported_coefs_expenditures)

#now latex it
summary_reported_coefs_expenditures <- summary(stan_fit_reported_coefs_expenditures )
textable_reported_coefs_expenditures <- xtable(summary_reported_coefs_expenditures$summary)
print.xtable(textable_reported_coefs_expenditures)

#now save the table for easy access
sink("textable_stan_fit_expenditures_rubin_model_reported_coefs.txt")
print.xtable(textable_reported_coefs_expenditures)
sink()

#codafit it
codafit_stan_fit_reported_coefs_expenditures <- stan2coda(stan_fit_reported_coefs_expenditures)
summary(codafit_stan_fit_reported_coefs_expenditures)
#plot(codafit)
codafitmatrix_stan_fit_reported_coefs_expenditures <- as.matrix(codafit_stan_fit_reported_coefs_expenditures)

}

### RM ols coefficients "best match" set of regressions ###


ols_best_match_coefs_data_expenditures <- list(K = 7, 
                                   tau_hat_k = standardised_ols_best_match_output_expenditures_coefficients,
                                   se_k = standardised_ols_best_match_output_expenditures_coefficients_sds)


sflist <-
  mclapply(1:chains, mc.cores = chains,
           function(i) sampling(model, data = ols_best_match_coefs_data_expenditures, seed = seed,
                                chains = 1, chain_id = i, # refresh = -1,
                                iter = iters, control = control))
stan_fit_ols_best_match_coefs_expenditures <- sflist2stanfit(sflist)



#now latex it
summary_ols_best_match_coefs_expenditures <- summary(stan_fit_ols_best_match_coefs_expenditures )
textable_ols_best_match_coefs_expenditures <- xtable(summary_ols_best_match_coefs_expenditures$summary)
print.xtable(textable_ols_best_match_coefs_expenditures)

#now save the table for easy access
sink("textable_stan_fit_expenditures_rubin_model_best_match.txt")
print.xtable(textable_ols_best_match_coefs_expenditures)
sink()

#codafit it
codafit_stan_fit_ols_best_match_coefs_expenditures <- stan2coda(stan_fit_ols_best_match_coefs_expenditures)
summary(codafit_stan_fit_ols_best_match_coefs_expenditures)
#plot(codafit)
codafitmatrix_stan_fit_ols_best_match_coefs_expenditures <- as.matrix(codafit_stan_fit_ols_best_match_coefs_expenditures)


save.image(file = "microcredit_rubin_model_expenditures_output.RData")

#### Reduced Data Model: Independent Mu and Tau ####
# Load data
rm(list = ls())
load("microcredit_project_data_with_replication_expenditures.RData")
if( exists("USD_convert_to_2009_dollars")=="FALSE"){stop("OLD DATA FILE LOADED! RELOAD CORRECT FILE!")}


model_file <- file.path('microcredit-independent-model-reduced-data.stan') 

model <- stan_model(model_file)
expenditures_data <- list(K = length(standardised_ols_best_match_output_expenditures_coefficients),
                    
                    mu_k_hat = standardised_ols_best_match_output_expenditures_means, 
                    tau_k_hat = standardised_ols_best_match_output_expenditures_coefficients,
                    se_mu_k = standardised_ols_best_match_output_expenditures_means_sds, 
                    se_tau_k = standardised_ols_best_match_output_expenditures_coefficients_sds)




# some knobs we can tweak
chains <- 4
iters <- 20000
control <- list(adapt_t0 = 10,       # default = 10
                stepsize = 1,        # default = 1
                max_treedepth = 6)   # default = 10
seed <- 42

# Run HMC

sflist <-
  mclapply(1:chains, mc.cores = chains,
           function(i) sampling(model, data = expenditures_data, seed = seed,
                                chains = 1, chain_id = i, # refresh = -1,
                                iter = iters, control = control))
stan_fit_expenditures_independent_reduced <- sflist2stanfit(sflist)

# This will display the posterior inference directly
print(stan_fit_expenditures_independent_reduced)

#now latex it
output_expenditures_independent_reduced <- summary(stan_fit_expenditures_independent_reduced)
textable_expenditures_independent_reduced <- xtable(output_expenditures_independent_reduced$summary)
print.xtable(textable_expenditures_independent_reduced)

#now save the table for easy access
sink("textable_expenditures_flat_het_mutau_point_estimates_independent.txt")
print.xtable(textable_expenditures_independent_reduced)
sink()

#now save every object we created (saved here just in case)

save.image(file = "microcredit_optimised_independent_dist_model_output.RData")


# codafit object! 
codafit_stan_fit_expenditures_independent_reduced <- stan2coda(stan_fit_expenditures_independent_reduced)
summary(codafit_stan_fit_expenditures_independent_reduced)
#plot(codafit)
codafitmatrix_stan_fit_expenditures_independent_reduced <- as.matrix(codafit_stan_fit_expenditures_independent_reduced)




#now save every object we created (saved here just in case)

save.image(file = "microcredit_optimised_independent_dist_model_expenditures_output.RData")



#### Reduced Data Model: Jointly distributed Mu and Tau #### 

# Load data
rm(list = ls())
load("microcredit_project_data_with_replication_expenditures.RData")
if( exists("USD_convert_to_2009_dollars")=="FALSE"){stop("OLD DATA FILE LOADED! RELOAD CORRECT FILE!")}


### Impact of Microcredit on expenditures with Heteroskedasticity ###
P <- 2


model_file <- file.path('basic-microcredit-uninformative-ss-point-estimates.stan') 

model <- stan_model(model_file)
expenditures_data <- list(K = length(standardised_ols_best_match_output_expenditures_coefficients),
                    P = P,    
                    mu_k_hat = standardised_ols_best_match_output_expenditures_means, 
                    tau_k_hat = standardised_ols_best_match_output_expenditures_coefficients,
                    se_mu_k = standardised_ols_best_match_output_expenditures_means_sds, 
                    se_tau_k = standardised_ols_best_match_output_expenditures_coefficients_sds, 
                    mutau_prior_sigma = (1000^2)*diag(P),
                    mutau_prior_mean = rep(0,P))



# some knobs we can tweak
chains <- 4
iters <- 20000
control <- list(adapt_t0 = 10,       # default = 10
                stepsize = 1,        # default = 1
                max_treedepth = 6)   # default = 10
seed <- 42

# Run HMC

sflist <-
  mclapply(1:chains, mc.cores = chains,
           function(i) sampling(model, data = expenditures_data, seed = seed,
                                chains = 1, chain_id = i, # refresh = -1,
                                iter = iters, control = control))
stan_fit_expenditures_joint_reduced <- sflist2stanfit(sflist)

# This will display the posterior inference directly
print(stan_fit_expenditures_joint_reduced)

#now latex it
output_expenditures_joint_reduced <- summary(stan_fit_expenditures_joint_reduced)
textable_expenditures_joint_reduced <- xtable(output_expenditures_joint_reduced$summary)
print.xtable(textable_expenditures_joint_reduced)

#now save the table for easy access
sink("textable_expenditures_flat_het_mutau_point_estimates.txt")
print.xtable(textable_expenditures_joint_reduced)
sink()

#now save every object we created (saved here just in case)


# codafit object! 
codafit_stan_fit_expenditures_joint_reduced <- stan2coda(stan_fit_expenditures_joint_reduced)
summary(codafit_stan_fit_expenditures_joint_reduced)
#plot(codafit)
codafitmatrix_stan_fit_expenditures_joint_reduced <- as.matrix(codafit_stan_fit_expenditures_joint_reduced)



save.image(file = "microcredit_joint_model_reduced_data_expenditures.RData")






#### Full Data Model:  Independent Mu and Tau ####

# Load data
rm(list = ls())
load("microcredit_project_data_with_replication_expenditures.RData")
if( exists("USD_convert_to_2009_dollars")=="FALSE"){stop("OLD DATA FILE LOADED! RELOAD CORRECT FILE!")}

### VARIABLE STACKING PROCEDURE FOR RSTAN INTAKE ###

# This preps data so that the model can be written efficiently - it's not necessary but it's the best way I know to code it in STAN
site <- c(angelucci_indicator, attanasio_indicator, augsberg_indicator,banerjee_indicator, crepon_indicator, karlan_indicator, tarozzi_indicator)
expenditures <- c(angelucci_expenditures, attanasio_expenditures, augsberg_expenditures,banerjee_expenditures, crepon_expenditures, karlan_expenditures, tarozzi_expenditures)
treatment <- c(angelucci_treatment, attanasio_treatment, augsberg_treatment,banerjee_treatment, crepon_treatment, karlan_treatment, tarozzi_treatment)

# Now we have to standardise any variables which are in local currency units to USD PPP per fortnight in 2009 dollars


expanded_standardiser_USD_PPP_per_fortnight <- c( rep(the_expenditures_standardiser_USD_PPP_per_fortnight[1],length(angelucci_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[2],length(attanasio_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[3],length(augsberg_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[4],length(banerjee_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[5],length(crepon_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[6],length(karlan_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[7],length(tarozzi_indicator)))

expenditures <- expenditures*expanded_standardiser_USD_PPP_per_fortnight

# bind everything into a data frame
data <- data.frame(site, expenditures, treatment)

# We gotta remove the NA values
data <- data[complete.cases(data),]



# Set the model file and Stan parameters

model_file <- file.path('microcredit-independent-model-ss.stan')
model <- stan_model(model_file)

# some knobs we can tweak
chains <- 4
iters <- 20000
control <- list(adapt_t0 = 10,       # default = 10
                stepsize = 1,        # default = 1
                max_treedepth = 6)   # default = 10
seed <- 40 # it was 42 before, i'm testing for something



### organise data ###

P <- 2
expenditures_data <- list(K = length(unique(data$site)),
                    N = length(data$treatment),
                    P = P,
                    site = data$site,
                    y = data$expenditures,
                    ITT = data$treatment)


sflist <-
  mclapply(1:chains, mc.cores = chains,
           function(i) sampling(model, data = expenditures_data, seed = seed,
                                chains = 1, chain_id = i, # refresh = -1,
                                iter = iters, control = control))
stan_fit_expenditures_independent <- sflist2stanfit(sflist)

# This will display the posterior inference directly
print(stan_fit_expenditures_independent)

#now latex it
summary_stan_fit_expenditures_independent <- summary(stan_fit_expenditures_independent)
textable_expenditures_independent <- xtable(summary_stan_fit_expenditures_independent$summary)
print.xtable(textable_expenditures_independent)

#now save the table for easy access
sink("textable_stan_fit_expenditures_independent.txt")
print.xtable(textable_expenditures_independent)
sink()

# now codafit it


codafit_expenditures_independent <- stan2coda(stan_fit_expenditures_independent)
summary(codafit_expenditures_independent)
codafitmatrix_expenditures_independent <- as.matrix(codafit_expenditures_independent)

save.image(file = "microcredit_independent_model_expenditures_output.RData")





#### Full Data Model: Allows covariation between Mu and Tau ####
# Load data
rm(list = ls())
load("microcredit_project_data_with_replication_expenditures.RData")
if( exists("USD_convert_to_2009_dollars")=="FALSE"){stop("OLD DATA FILE LOADED! RELOAD CORRECT FILE!")}

# This preps data so that the model can be written efficiently - it's not necessary but it's the best way I know to code it in STAN
site <- c(angelucci_indicator, attanasio_indicator, augsberg_indicator,banerjee_indicator, crepon_indicator, karlan_indicator, tarozzi_indicator)
expenditures <- c(angelucci_expenditures, attanasio_expenditures, augsberg_expenditures,banerjee_expenditures, crepon_expenditures, karlan_expenditures, tarozzi_expenditures)
treatment <- c(angelucci_treatment, attanasio_treatment, augsberg_treatment,banerjee_treatment, crepon_treatment, karlan_treatment, tarozzi_treatment)

# Now we have to standardise any variables which are in local currency units to USD PPP per fortnight in 2009 dollars


expanded_standardiser_USD_PPP_per_fortnight <- c( rep(the_expenditures_standardiser_USD_PPP_per_fortnight[1],length(angelucci_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[2],length(attanasio_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[3],length(augsberg_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[4],length(banerjee_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[5],length(crepon_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[6],length(karlan_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[7],length(tarozzi_indicator)))

expenditures <- expenditures*expanded_standardiser_USD_PPP_per_fortnight

# bind everything into a data frame
data <- data.frame(site, expenditures, treatment)

# We gotta remove the NA values
data <- data[complete.cases(data),]


P <- 2
model_file <- file.path('basic-microcredit-uninformative-ss.stan') 
model <- stan_model(model_file)

expenditures_data <- list(K = length(unique(data$site)),
                   N = length(data$treatment),
                   P = P,
                   site = data$site,
                   y = data$expenditures,
                   ITT = data$treatment,
                   mutau_prior_sigma = (1000^2)*diag(P),
                   mutau_prior_mean = rep(0,P))

# some knobs we can tweak
chains <- 4
iters <- 20000
control <- list(adapt_t0 = 10,       # default = 10
                stepsize = 1,        # default = 1
                max_treedepth = 6)   # default = 10
seed <- 42


sflist <-
  mclapply(1:chains, mc.cores = chains,
           function(i) sampling(model, data = expenditures_data, seed = seed,
                                chains = 1, chain_id = i, # refresh = -1,
                                iter = iters, control = control))
stan_fit_expenditures_joint <- sflist2stanfit(sflist)

# This will display the posterior inference directly
print(stan_fit_expenditures_joint)

#now latex it
output_expenditures_joint <- summary(stan_fit_expenditures_joint)
textable_expenditures_joint <- xtable(output_expenditures_joint$summary)
print.xtable(textable_expenditures_joint)

#now save the table for easy access
sink("textable_stan_fit_expenditures_joint.txt")
print.xtable(textable_expenditures_joint)
sink()

#codafit it

codafit_expenditures_joint <- stan2coda(stan_fit_expenditures_joint)
summary(codafit_expenditures_joint)
codafitmatrix_expenditures_joint <- as.matrix(codafit_expenditures_joint)

save.image(file = "microcredit_joint_model_expenditures_output.RData")


#### Full Data Model: Independent but split by prior business ####

# Load data
rm(list = ls())
load("microcredit_project_data_with_replication_expenditures.RData")
if( exists("USD_convert_to_2009_dollars")=="FALSE"){stop("OLD DATA FILE LOADED! RELOAD CORRECT FILE!")}


# This preps data so that the model can be written efficiently - it's not necessary but it's the best way I know to code it in STAN 
site <- c(angelucci_indicator, attanasio_indicator, augsberg_indicator,banerjee_indicator, crepon_indicator, karlan_indicator, tarozzi_indicator)
expenditures <- c(angelucci_expenditures, attanasio_expenditures, augsberg_expenditures,banerjee_expenditures, crepon_expenditures, karlan_expenditures, tarozzi_expenditures)
treatment <- c(angelucci_treatment, attanasio_treatment, augsberg_treatment,banerjee_treatment, crepon_treatment, karlan_treatment, tarozzi_treatment)
priorbiz <- c(angelucci_existingbusiness, attanasio_existingbusiness, augsberg_existingbusiness,banerjee_existingbusiness, crepon_existingbusiness, karlan_existingbusiness, tarozzi_existingbusiness)


# Now we have to standardise any variables which are in local currency units to USD PPP
expanded_standardiser_USD_PPP_per_fortnight <- c( rep(the_expenditures_standardiser_USD_PPP_per_fortnight[1],length(angelucci_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[2],length(attanasio_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[3],length(augsberg_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[4],length(banerjee_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[5],length(crepon_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[6],length(karlan_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[7],length(tarozzi_indicator)))

expenditures <- expenditures*expanded_standardiser_USD_PPP_per_fortnight


# now reformat so the stan code can take the data in as a buncha vectors 

X_mu <- data.frame(rep(1, length(priorbiz)), priorbiz)
X_tau <- data.frame(treatment, treatment*priorbiz)

# bind everything into a data frame

data <- data.frame(site, expenditures, X_mu, X_tau)
# We gotta remove the NA values
data <- data[complete.cases(data),]


Q = 2 # number of subgroups
expenditures_data <- list(K = length(unique(data$site)) ,
                   N = length(data$expenditures),
                   Q = Q,
                   P = 2*Q,
                   site = data$site,
                   y = data$expenditures,
                   X_mu = data[,3:4],
                   X_tau = data[,5:6])


## this bit of code is from Jonathan Huggins ##

# some knobs we can tweak
chains <- 4
iters <- 300000
control <- list(adapt_t0 = 10,       # default = 10
                stepsize = 1,        # default = 1
                max_treedepth = 6)   # default = 10
seed <- 42

# Run HMC

model_file <- file.path('microcredit-interactions-model-ss.stan') #this is ss
model <- stan_model(model_file)


  sflist <-
    mclapply(1:chains, mc.cores = chains,
             function(i) sampling(model, data = expenditures_data, seed = seed,
                                  chains = 1, chain_id = i, # refresh = -1,
                                  iter = iters, control = control))
  stan_fit_expenditures_priorbiz_split <- sflist2stanfit(sflist)
  



#now latex it 
summary_stan_fit_expenditures_priorbiz_split <- summary(stan_fit_expenditures_priorbiz_split)
textable_stan_fit_expenditures_priorbiz_split <- xtable(summary_stan_fit_expenditures_priorbiz_split$summary)
print.xtable(textable_stan_fit_expenditures_priorbiz_split)

#now save all that good stuff

sink("textable_stan_fit_expenditures_priorbiz_split.txt")
print.xtable(textable_stan_fit_expenditures_priorbiz_split)
sink()

codafit_expenditures_priorbiz_split <- stan2coda(stan_fit_expenditures_priorbiz_split)
summary(codafit_expenditures_priorbiz_split)
codafitmatrix_expenditures_priorbiz_split <- as.matrix(codafit_expenditures_priorbiz_split)


save.image(file = "microcredit_independent_model_priorbiz_split_expenditures_output.RData")

#### Full Data Model: Independent with ridge layer on contextual variables ####

# Load data
rm(list = ls())
load("microcredit_project_data_with_replication_expenditures.RData")
if( exists("USD_convert_to_2009_dollars")=="FALSE"){stop("OLD DATA FILE LOADED! RELOAD CORRECT FILE!")}


# This preps data so that the model can be written efficiently - it's not necessary but it's the best way I know to code it in STAN 
site <- c(angelucci_indicator, attanasio_indicator, augsberg_indicator,banerjee_indicator, crepon_indicator, karlan_indicator, tarozzi_indicator)
expenditures <- c(angelucci_expenditures, attanasio_expenditures, augsberg_expenditures,banerjee_expenditures, crepon_expenditures, karlan_expenditures, tarozzi_expenditures)
treatment <- c(angelucci_treatment, attanasio_treatment, augsberg_treatment,banerjee_treatment, crepon_treatment, karlan_treatment, tarozzi_treatment)

# here's the set of contextual variables
targetwomen <- c(angelucci_targetwomen, attanasio_targetwomen, augsberg_targetwomen,banerjee_targetwomen, crepon_targetwomen, karlan_targetwomen, tarozzi_targetwomen)
village_rand <- c(angelucci_village_rand, attanasio_village_rand, augsberg_village_rand,banerjee_village_rand, crepon_village_rand, karlan_village_rand, tarozzi_village_rand)
individual_rand <- 1-village_rand
APR <- c(angelucci_APR, attanasio_APR, augsberg_APR,banerjee_APR, crepon_APR, karlan_APR, tarozzi_APR)
market_saturation <- c(angelucci_currentmarket, attanasio_currentmarket, augsberg_currentmarket,banerjee_currentmarket, crepon_currentmarket, karlan_currentmarket, tarozzi_currentmarket)
promotion <- c(angelucci_promotion, attanasio_promotion, augsberg_promotion,banerjee_promotion, crepon_promotion, karlan_promotion, tarozzi_promotion)
collateralised <- c(angelucci_collateralised, attanasio_collateralised, augsberg_collateralised,banerjee_collateralised, crepon_collateralised, karlan_collateralised, tarozzi_collateralised)
loansize_percentincome <- c(angelucci_loansize_percentincome, attanasio_loansize_percentincome, augsberg_loansize_percentincome,banerjee_loansize_percentincome, crepon_loansize_percentincome, karlan_loansize_percentincome, tarozzi_loansize_percentincome)

X_data <- cbind(individual_rand, targetwomen, APR, market_saturation, promotion, collateralised, loansize_percentincome)
L = dim(X_data)[2]

# Now we have to standardise any variables which are in local currency units to USD PPP

expanded_standardiser_USD_PPP_per_fortnight <- c( rep(the_expenditures_standardiser_USD_PPP_per_fortnight[1],length(angelucci_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[2],length(attanasio_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[3],length(augsberg_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[4],length(banerjee_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[5],length(crepon_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[6],length(karlan_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[7],length(tarozzi_indicator)))

expenditures <- expenditures*expanded_standardiser_USD_PPP_per_fortnight

# bind everything into a data frame

data <- data.frame(site, expenditures, treatment)
# We gotta remove the NA values
data <- data[complete.cases(data),]



### Impact of Microcredit on expenditures: with ridge ###

model_file <- file.path('microcredit-independent-model-with-ridge-ss.stan')
model <- stan_model(model_file)


expenditures_data <- list(K = length(unique(data$site)) ,
                    N = length(data$treatment),
                    L = dim(X_data)[2],
                    P = 2,
                    site = data$site,
                    y = data$expenditures,
                    ITT = data$treatment, 
                    X = X_data, 
                    prior_mean_beta = rep(0, L),
                    prior_var_beta = diag(rep(1,L),L,L)
)



# some knobs we can tweak
chains <- 4
iters <- 500000
control <- list(adapt_t0 = 10,       # default = 10
                stepsize = 1,        # default = 1
                max_treedepth = 6)   # default = 10
seed <- 42

sflist <-
  mclapply(1:chains, mc.cores = chains,
           function(i) sampling(model, data = expenditures_data, seed = seed,
                                chains = 1, chain_id = i, # refresh = -1,
                                iter = iters, control = control))
stan_fit_ridge_independent_expenditures <- sflist2stanfit(sflist)

#now latex it 
summary_stan_fit_ridge_independent_expenditures <- summary(stan_fit_ridge_independent_expenditures)
textable_stan_fit_ridge_independent_expenditures <- xtable(summary_stan_fit_ridge_independent_expenditures$summary)
print.xtable(textable_stan_fit_ridge_independent_expenditures)

#now save all that good stuff

sink("texable_stan_fit_ridge_independent_expenditures.txt")
print.xtable(textable_stan_fit_ridge_independent_expenditures)
sink()



codafit_stan_fit_ridge_independent_expenditures <- stan2coda(stan_fit_ridge_independent_expenditures)
summary(codafit_stan_fit_ridge_independent_expenditures)
#plot(codafit)
codafitmatrix_ridge_independent_expenditures <- as.matrix(codafit_stan_fit_ridge_independent_expenditures)

#now save every object we created 
save.image(file = "microcredit_independent_model_ridge_expenditures_output.RData")


## Now robustness check: do it with binaries ##

# Load data
rm(list = ls())
load("microcredit_project_data_with_replication_expenditures.RData")
if( exists("USD_convert_to_2009_dollars")=="FALSE"){stop("OLD DATA FILE LOADED! RELOAD CORRECT FILE!")}


# This preps data so that the model can be written efficiently - it's not necessary but it's the best way I know to code it in STAN 
site <- c(angelucci_indicator, attanasio_indicator, augsberg_indicator,banerjee_indicator, crepon_indicator, karlan_indicator, tarozzi_indicator)
expenditures <- c(angelucci_expenditures, attanasio_expenditures, augsberg_expenditures,banerjee_expenditures, crepon_expenditures, karlan_expenditures, tarozzi_expenditures)
treatment <- c(angelucci_treatment, attanasio_treatment, augsberg_treatment,banerjee_treatment, crepon_treatment, karlan_treatment, tarozzi_treatment)

# here's the set of contextual variables
targetwomen <- c(angelucci_targetwomen, attanasio_targetwomen, augsberg_targetwomen,banerjee_targetwomen, crepon_targetwomen, karlan_targetwomen, tarozzi_targetwomen)
village_rand <- c(angelucci_village_rand, attanasio_village_rand, augsberg_village_rand,banerjee_village_rand, crepon_village_rand, karlan_village_rand, tarozzi_village_rand)
individual_rand <- 1-village_rand
APR <- c(angelucci_APR, attanasio_APR, augsberg_APR,banerjee_APR, crepon_APR, karlan_APR, tarozzi_APR)
market_saturation <- c(angelucci_currentmarket, attanasio_currentmarket, augsberg_currentmarket,banerjee_currentmarket, crepon_currentmarket, karlan_currentmarket, tarozzi_currentmarket)
promotion <- c(angelucci_promotion, attanasio_promotion, augsberg_promotion,banerjee_promotion, crepon_promotion, karlan_promotion, tarozzi_promotion)
collateralised <- c(angelucci_collateralised, attanasio_collateralised, augsberg_collateralised,banerjee_collateralised, crepon_collateralised, karlan_collateralised, tarozzi_collateralised)
loansize_percentincome <- c(angelucci_loansize_percentincome, attanasio_loansize_percentincome, augsberg_loansize_percentincome,banerjee_loansize_percentincome, crepon_loansize_percentincome, karlan_loansize_percentincome, tarozzi_loansize_percentincome)

market_saturation[market_saturation < 2] <- 0
market_saturation[market_saturation >= 2] <- 1

APR[APR <= median(APR)] <- 0
APR[APR > median(APR)] <- 1

loansize_percentincome[loansize_percentincome <= median(loansize_percentincome)] <- 0
loansize_percentincome[loansize_percentincome > median(loansize_percentincome)] <- 1


X_data <- cbind(individual_rand, targetwomen, APR, market_saturation, promotion, collateralised, loansize_percentincome)
L = dim(X_data)[2]

# Now we have to standardise any variables which are in local currency units to USD PPP

expanded_standardiser_USD_PPP_per_fortnight <- c( rep(the_expenditures_standardiser_USD_PPP_per_fortnight[1],length(angelucci_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[2],length(attanasio_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[3],length(augsberg_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[4],length(banerjee_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[5],length(crepon_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[6],length(karlan_indicator)),
                                                  rep(the_expenditures_standardiser_USD_PPP_per_fortnight[7],length(tarozzi_indicator)))

expenditures <- expenditures*expanded_standardiser_USD_PPP_per_fortnight

# bind everything into a data frame

data <- data.frame(site, expenditures, treatment)
# We gotta remove the NA values
data <- data[complete.cases(data),]


### Impact of Microcredit on expenditures: with ridge ###

model_file <- file.path('microcredit-independent-model-with-ridge-ss.stan')
model <- stan_model(model_file)


expenditures_data <- list(K = length(unique(data$site)) ,
                   N = length(data$treatment),
                   L = dim(X_data)[2],
                   P = 2,
                   site = data$site,
                   y = data$expenditures,
                   ITT = data$treatment, 
                   X = X_data, 
                   prior_mean_beta = rep(0, L),
                   prior_var_beta = diag(rep(1,L),L,L)
)



# some knobs we can tweak
chains <- 4
iters <- 500000
control <- list(adapt_t0 = 10,       # default = 10
                stepsize = 1,        # default = 1
                max_treedepth = 6)   # default = 10
seed <- 42

sflist <-
  mclapply(1:chains, mc.cores = chains,
           function(i) sampling(model, data = expenditures_data, seed = seed,
                                chains = 1, chain_id = i, # refresh = -1,
                                iter = iters, control = control))
stan_fit_ridge_independent_binary_expenditures <- sflist2stanfit(sflist)

#now latex it 
summary_stan_fit_ridge_independent_binary_expenditures <- summary(stan_fit_ridge_independent_binary_expenditures)
textable_stan_fit_ridge_independent_binary_expenditures <- xtable(summary_stan_fit_ridge_independent_binary_expenditures$summary)
print.xtable(textable_stan_fit_ridge_independent_binary_expenditures)

#now save all that good stuff

sink("texable_stan_fit_ridge_independent_binary_expenditures.txt")
print.xtable(textable_stan_fit_ridge_independent_binary_expenditures)
sink()



codafit_stan_fit_ridge_independent_binary_expenditures <- stan2coda(stan_fit_ridge_independent_binary_expenditures)
summary(codafit_stan_fit_ridge_independent_binary_expenditures)
#plot(codafit)
codafitmatrix_ridge_independent_binary_expenditures <- as.matrix(codafit_stan_fit_ridge_independent_binary_expenditures)

#now save every object we created 
save.image(file = "microcredit_independent_model_ridge_binary_expenditures_output.RData")




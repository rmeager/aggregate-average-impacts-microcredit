* Basic regressions on the Augsberg Data 
* Rachael Meager
* Feb 2015

gen totprofit = bm_profit + bs_profit

reg totprofit treatment, robust


reg bm_profit treatment, robust

reg bs_profit treatment, robust

tab followup attrit

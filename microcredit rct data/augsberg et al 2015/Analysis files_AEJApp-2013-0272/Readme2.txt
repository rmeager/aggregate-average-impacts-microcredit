******************************************************************
------------------- AEJ APPLIED SPECIAL ISSUE: -------------------
******************************************************************


******************************************************************
TITLE: The impacts of microcredit: Evidence from Bosnia and Herzegovina
++++++++ 

AUTHORS:
++++++++ Britta Augsburg, Ralph De Haas, Heike Harmgart, Costas Meghir

******************************************************************


To re-run our analysis, please install a folder "Analysis files"
The structure of this folder is as follows:


----------
STRUCTURE:
----------

XXX\Analysis files\data		<------------------------subfolders: "Baseline", "Followup", and a couple of other stata data files
		  \do-files	<------------------------subfolders: no sub-folders, just the do-files
		  \Multiple hypothesis testing	<--------subfolders: "do-files", "Estimated full model" and an output data set
		  \output	<------------------------subfolders: "Attrition", "Impact Estimates", "Impact Estimates - reweighted", 
                                                                     "Impact Estimates - trimmed", "Repayment and client"

------------------------
EXPLANATION OF DO-FILES:
------------------------


The do-files provided in the folders:
- "XXX\Analysis files\do-files", and 
- "Analysis files\Multiple hypothesis testing\do-files"
are described in what follows:



"Augsburg et al (2014)_Bosnia_Create dataset(Covariates Bosnia Baseline).do"
"Augsburg et al (2014)_Bosnia_Create dataset(Household level info working hrs and schooling - teenagers).do"
"Augsburg et al (2014)_Bosnia_Create dataset(labour supply and schooling outcomes - hh level).do"
------------------------------------------------------------
- These three do-files are just for information to show how sub-data sets were created
  The do-files do not need to be run for the impact estimate do-files to work
- To run the files, the working directory paths need to be adjusted


"Augsburg et al (2014)_Bosnia_Table 1 (Randomization).do"
------------------------------------------------------------
- This do-file is best executed quietly -- we just copy-pasetd 
  results into excel. Not beautiful but On the positive side, 
  no folder needs to therefore be created to run this file.
- To run the file all you need to do is change the path of the 
  working directory in line 21.  


"Augsburg et al (2014)_Bosnia_Tables 2-7 & TA7-upper (Impact Estimates).do"
----------------------------------------------------------------
- Estimates all impact results presented in Tables 2-7 as well as results in the upper panel of Table TA7
- To run this do-file you need to change the working directory path in line 18. 
  In the folder this path leads to, you need to have two sub-folder:
  (i) "data" (with all files we submitted), and
  (ii) "output" -- here is where the results will be stored. 
  This folder needs to have sub-folders as described at the beginning of this file



"Augsburg et al (2014)_Bosnia_Multiple testing_RW (bsample)_T2.do"
---------------------------------------------------------------------
- This do-files conducts the Romano Wolf procedure to adjust for 
  multiple hypothesis testing.
- It calls the do-file "(0) prepare data.do"
- working directories must be adjusted in lines 19 and 73
- We only provide the do-file for Table 2. Other Tables work arordingly
  (all that needs to be changed is the global Table in line 90 and
  then ideally also names of output files 
- The file runs the first iteration (do-file is exited in line 236)
  In the case some variables are rejected ("pass") the second iteration needs 
  to be done by hand. This is done by dropping the centered tstats of variables
  that passed and re-defining the global Table with the remaining Variables.
  And then one runs the second iteration (lines 213-235).
- To change the level of significance, one changes the "p(99)" in line 222.


"Augsburg et al (2014)_Bosnia_Table T9 (late payment and default).do"
------------------------------------------------------------
- This do-file estimates results presented in Table 9 of the paper -- estimating 
probability of late payment and default among marginal borrowers
- To run the file the working directory path has to be adjusted in line 20




"Augsburg et al (2014)_Bosnia_Table TA4 & TA6 (Attrition).do"
------------------------------------------------------------
- This do-file estimates results presented in Tables TA4 and TA6 related to attrition.
- To run the file the working directory path has to be adjusted in line 18



"Augsburg et al (2014)_Bosnia_Table TA5 (Reweighting).do"
------------------------------------------------------------
- This do-file estimates results presented in Table TA5, where we deal with attrition by 
  reweighting the data with the inverse probability of being observed, following DiNardo, Fortin 
  and Lemieux (1996).
- To run the file the working directory path has to be adjusted in line 19


"Augsburg et al (2014)_Bosnia_Table TA7 (Intended loan use).do"
------------------------------------------------------------
- This do-file estimates results presented in Tables TA7 where we look at heterogeneous 
  effects by stated inteded loan use at baseline (specifically for consumption).
- To run the file the working directory path has to be adjusted in line 18


"Augsburg et al (2014)_Bosnia_Table TA10 (trimming).do"
------------------------------------------------------------
- This do-file estimates results presented in Tables TA10 where we double checks findings
  for all dollar amount variables - trimmed and untrimmed
- To run the file the working directory path has to be adjusted in line 18












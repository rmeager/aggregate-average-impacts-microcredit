# delimit ;
set more off;
drop _all;
matrix drop _all;
capture log close;
set logtype text;
pause on;

*** GENERATES RESULTS IN APPENDIX TABLE A.5 (Changes in Sample Composition);

capture log using "TableB2Migration", replace;

*** Check changes in presumably pre-determined characteristics to see if there is any evidence of changes in the underlying population;
use "data", clear;

* PANEL A: BY RANDOMLY ASSIGNED TREATMENT;
* % households who moved into current village of residence less than 4 years earlier;
table time patypen if time==1 [w=fwt], c(mean moved4) format(%6.3f);
* Characteristics of household head;
table time patypen if time==1 [w=fwt], c(mean hh_adults mean head_age) format(%6.1f);
table time patypen if time==1 [w=fwt], c(mean male_head mean head_noeduc mean headprimary) format(%6.3f);

* Now test the null of equal changes for all variables (the p-values in column 5);
areg moved4 time t_T_MF t_T_Both t_T_FP [pw=fwt], cluster(pa) absorb(pa);
test t_T_MF t_T_Both t_T_FP;
matrix results = r(p);
foreach var of varlist hh_adults head_age male_head head_noeduc headprimary {;
	areg `var' time t_T_MF t_T_Both t_T_FP [pw=fwt], cluster(pa) absorb(pa);
	test t_T_MF t_T_Both t_T_FP;
	matrix results = results\r(p);
}; 
mat list results, format(%6.4f);

* PANEL B: BY ACTUAL TREATMENT;
* % households who moved into current village of residence less than 4 years earlier;
table time patypeactual2 if time==1 [w=fwt], c(mean moved4) format(%6.3f);
* Characteristics of household head;
table time patypeactual2 if time==1 [w=fwt], c(mean hh_adults mean head_age) format(%6.1f);
table time patypeactual2 if time==1 [w=fwt], c(mean male_head mean head_noeduc mean headprimary) format(%6.3f);

* Now test the null of equal changes for all variables (the p-values in column 5);
areg moved4 time t_D_MF t_D_Both t_D_FP [pw=fwt], cluster(pa) absorb(pa);
test t_D_MF t_D_Both t_D_FP;
matrix results = r(p);
foreach var of varlist hh_adults head_age male_head head_noeduc headprimary {;
	areg `var' time t_D_MF t_D_Both t_D_FP [pw=fwt], cluster(pa) absorb(pa);
	test t_D_MF t_D_Both t_D_FP;
	matrix results = results\r(p);
}; 
mat list results, format(%6.4f);

log close;

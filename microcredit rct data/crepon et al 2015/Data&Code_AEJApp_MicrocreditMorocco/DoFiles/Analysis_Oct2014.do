
**** DO FILE Compares endline treatment versus control groups
**** INPUT fILES: endline_minienquete_outcomes.dta/baseline_minienquete_outcomes.dta
**** OUTPUT FILES: endline_baseline_outcomes.dta
**** date: February, 2014
******************************************************************************************************************************
** This do file is organized in the following way:
	** We first keep in baseline_minienquete_outcomes.dta selected variables ($baseline, $baseline_d and other ID variables)
	** We select variables in endline database ($outcomes and other ID variables)
	** We merge both and complete with zeros missing baseline controls and create `var'_m==1
	** We save endline_baseline_outcomes.dta 
	** We run regressions:
		* RANDOMIZATION & ATTRITION
		* T-C COMPARISON
	
******************************************************************************************************************************
*****************************************************************************************************************************  
# delimit; 
clear; 
capture clear matrix;
set mem 500m; 
set maxvar 10000; 
cap log close; 

set more off;

******************************************;
* WE IDENTIFY BASELINE & ENDLINE VARIABLES
	we will want to keep;
******************************************;
global baseline
	members_resid nadults_resid nchildren_resid head_male head_age head_educ_1

	borrowed_alamana borrowed_oformal2 borrowed_informal borrowed_branching borrowed_total
	aloansamt_alamana aloansamt_oformal2 aloansamt_informal aloansamt_branching

	act_number 
	act_agri inv_agri sale_agri expense_agri savings_agri empl_agri work_agri
	act_livestock inv_livestock sale_livestock expense_livestock savings_livestock empl_livestock work_livestock
	act_business
	
	women_act_number women_act distance_soukpt
	self_empl
	shock1 shock1agri shock1live hadlost_livestockanim shock2
	consumption cons_nondurables cons_durables poor
	consumption_pc

	income_remittance income_pension income_gvt income_assetsales 
	income_agri income_livestock income_business
	income_dep daily_dum

	assets_total output_total expense_total profit_total hours_self_age16_65 hours_outside_age16_65
	client
	prod_agri
	selfempl_agri selfempl_livestock selfempl_business;

global baseline_d
	members_resid_d nadults_resid_d head_age_d act_livestock_d act_business_d  borrowed_total_d;

global outcomes 

client

borrowed_alamana 
borrowed_oamc 
borrowed_oformal 
borrowed_branching 
borrowed_informal 
borrowed_total

loansamt_alamana
loansamt_oamc
loansamt_oformal
loansamt_branching
loansamt_informal
loansamt_total
cons_repay

assets_total
asset_agri
stock_agri
astock_livestock
asset_business
astock_business

output_total
prod_agri
sale_livestock  
sale_business

sale_agri
savings_agri 
sale_livestockanim
sale_livestockprod

expense_total
expense_agri 
expense_livestock 
expense_business

inv_total
inv_agri
inv_livestock  
inv_business_assets

profit_total
profit_agri
profit_livestock 
profit_business

selfempl_agri
selfempl_livestock
selfempl_business
self_empl

act_total_agri
act_total_live
act_number_business

land
land_ha
land_expl_ha

income
income_agri 
income_livestock 
income_business
income_dep
income_assetsales

income_remittance 
income_pension 
income_gvt 
income_othoth
income_work
income_other_new

nmember_age6_65
nmember_age6_16
nmember_age16_20 
nmember_age20_50 
nmember_age50_65

hours_all_age16_65 
hours_self_out_age16_65 
hours_self_age16_65 
hours_outside_age16_65 
hours_chores_age16_65 

hours_all_age6_65
hours_self_out_age6_65 
hours_self_age6_65 
hours_outside_age6_65 
hours_chores_age6_65

hours_all_age6_16
hours_self_out_age6_16 
hours_self_age6_16 
hours_outside_age6_16 
hours_chores_age6_16

hours_all_age16_20 
hours_self_out_age16_20
hours_self_age16_20 
hours_outside_age16_20 
hours_chores_age16_20 

hours_all_age20_50 
hours_self_out_age20_50 
hours_self_age20_50 
hours_outside_age20_50 
hours_chores_age20_50

hours_all_age50_65 
hours_self_out_age50_65 
hours_self_age50_65 
hours_outside_age50_65 
hours_chores_age50_65

consumption_pc
consumption 
cons_durables
cons_nondurables
cons_food
cons_social
cons_schooling
cons_health
cons_temp

sharekids_inschool
shareteens_inschool
women_index
women_act
women_act_number;


********************************;
* WE GET BASELINE READY;
*******************************;
#delimit;
use Output\baseline_minienquete_outcomes.dta, clear;

* We create dummy variables for baseline controls;
	* We will later replace these dummy variables by 1 if missing at baseline; 

	foreach var in members_resid_d nadults_resid_d act_livestock_d act_business_d borrowed_total_d {;
	gen `var'=0;
		label var `var' "1 if missing obs at baseline";
	};


** we keep baseline variables;
keep $baseline $baseline_d ident id1 id2 id3 id4 id10_combined id11_combined id13
	score1 score2 score3 treatment paire demi_paire wave random5 
	m1 m2 m3 m4 m5 m6 m7c m8n m8s m9a m9b m9c m9d m9e m9fb m9fc m9g m10a m10b m11 m12 m13 m14
	retr-e4part share_inc_self share_inc_dep share_cons_income;
  
** we rename baseline variables;
foreach v of varlist _all {; 
	rename `v' `v'_bl; 
	}; 
 
foreach j in ident treatment paire demi_paire random5 { ; 
	rename `j'_bl `j'; 
	}; 

sort ident; 
save Output\temp_baseline.dta, replace; 


********************************;
* WE MERGE ENDLINE AND BASELINE;
*******************************;
use Output\endline_minienquete_outcomes.dta, clear; 

keep $outcomes $baseline m1-m14 retr-e4part 
ccm_resp_activ other_resp_activ
ident id1 id2 id3 id4 id10_d id11_d id13
score1 score2 score3 treatment paire demi_paire wave newhh random5_end
client_admin admin_*;


**********************************************************;
** WE FIRST TRIM KEY VARIABLES & KEEP A UNIQUE POPULATION; 
	*we use our summary variables; 

global variables_trim 
/*loansamt_total*/ loansamt_total loansamt_alamana loansamt_oamc loansamt_oformal loansamt_branching loansamt_informal
/*assets_total asset_agri astock_livestock asset_business*/ assets_total stock_agri astock_livestock astock_business
/*output_total*/ output_total sale_agri savings_agri sale_livestockanim sale_livestockprod sale_business  
/*expense_total*/ expense_total expense_agri expense_livestock expense_business 
/*income_dep*/ income_dep 
/*hours_self_out_age16_65*/
/* consumption*/ consumption;  


foreach var of global variables_trim {;
	sum `var', detail;
	};

foreach var of global variables_trim {;
	_pctile `var' if `var'!=0, p(90);
	gen r_`var' = `var' /  r(r1);
	};

egen maxratio=rmax(r_loansamt_total-r_consumption);
	_pctile maxratio, p(99.5);
	gen ratiocut = r(r1);
	gen trimobs= (maxratio>ratiocut & maxratio!=.) ;
		tab trimobs;  

	label var trimobs "1 if trimmed observation";

* we turn to missing all variables we defined in the outcome do files;
foreach var in $outcomes $baseline {;
	replace `var'=. if trimobs==1;
	};

foreach var in loansamt_total assets_total output_total expense_total profit_total income_dep hours_self_out_age16_65 consumption {;
	sum `var', detail;
	sum `var' if `var'>0, detail;
	};

	drop r_loansamt_total-ratiocut; 

***********************************************;
*** WE MERGE WITH BASELINE;	

merge ident using Output\temp_baseline.dta; 
	tab _merge; 
	erase Output\temp_baseline.dta;  

	label var _merge "1 only endline, 2 only baseline, 3 merged";

	* We complete mini survey data for _merge==2 and _merge==1;
	foreach var of varlist m1-m14 {;
		replace `var'=`var'_bl if _merge==2;
		};

	foreach var of varlist m1-m14 {;
		replace `var'=0 if `var'==. & _merge==1;
		};

	foreach var of varlist retr-e4part {;
		replace `var'=`var'_bl if _merge==2;
		};

	codebook m1-m14 retr-e4part;

*********************************************************************************************;
*********************************************************************************************;
** We merge with the FULL MINIENQUETE DATABASE and we compute the PROBA of being in the sample;

sort ident;
merge ident using Input/Microcredit_MS_anonym.dta, _merge(sampled);
	tab sampled;

	* we check no discrepancies within mini-survey;
	foreach var of varlist m1-m14 demi_paire {;
		count if `var'!=me_`var' & sampled==3;
		};

	* in the field file used to compute the score, missing became zero;
	foreach var of varlist me_m1- me_m14 {;
		replace `var'=0 if `var'==. & sampled==2;
		};

	* we replace variables of interest for sampled ==2;
	foreach var of varlist m1-m14 demi_paire {;
		replace `var'=me_`var' if sampled==2;
		};
		assert demi_paire!=""; 


** We recode SAMPLED variable;
	recode sampled (2=0);
	recode sampled (3=1);
		tab sampled;
		label var sampled "1 if sampled for HH survey";

** PROBABILITY of being in the HH survey sample;
	egen m6_c=cut(m6), group(5);
	egen m9d_c=cut(m9d), group(5);
	egen m9g_c=cut(m9g), group(5);
	egen m10a_c=cut(m10a), group(5);
	egen m10b_c=cut(m10b), group(5);
	egen m13_c=cut(m13), group(5);

foreach var in  m6_c m9d_c m9g_c m10a_c m10b_c m13_c {;
	label var `var' "categorical variable of `var' in 5 frequency grouping intervals";
	};

xi: probit sampled m1 m2 m3 m4 m5 m6_c 
	m7c m8n m8s m9a m9b m9c m9d_c 
	m9e m9fb m9fc m9g_c m10a_c m10b_c 
	m11 m12 m13_c m14 
	i.demi_paire;

	predict proba;
	label var proba "probability of being sampled"; 
	
	gen weightproba=1/proba;
	* we censor at 10;
		replace weightproba=10 if weightproba>10 & weightproba!=.;
	* for the time being we put weight = 1 for missing;
		replace weightproba=1 if weightproba==.;

	label var weightproba "inverse of the probability of being sampled"; 
   		
	* histogram proba, bin(100);
	* histogram weightproba if  weightproba<25, bin(100);
	* histogram weightproba if  weightproba<25 & sampled==1, bin(100);

drop me_m1-me_demi_paire;

*******************************************;
*******************************************;
** We keep baseline & endline observations;

keep if sampled==1;
	drop sampled;

*******************************************************;
*******************************************************;
*** We check and complete score variables;
 
** We check we obtain same score with baseline and endline minienquete databases;
	assert score1==score1_bl if _merge==3;
	assert score2==score2_bl if _merge==3;
	assert score3==score3_bl if _merge==3;

** we complete scores for _merge==2 obs;
	replace score1=score1_bl if _merge==2;
	replace score2=score2_bl if _merge==2;
	replace score3=score3_bl if _merge==2;

** we complete random5 variable for only endline obs;
	replace random5=0 if _merge==1;

** we complete random5 variable for two villages;
	replace random5=random5_end if random5_end==1 & newhh==0;
		drop random5_end;

********************************************************************;
* We reclassify baseline random obs that have a higher score than obs added at endline;

* We compare different score rankings;
	gsort demi_paire -score2 random5;
		by demi_paire: egen min_score2_random=min(score2) if random5==1;
		by demi_paire: egen min_score2_newhh=min(score2) if newhh==1;

	gsort demi_paire -score3 random5;
		by demi_paire: egen min_score3_random=min(score3) if random5==1;
		by demi_paire: egen min_score3_newhh=min(score3) if newhh==1;


	foreach j in 2 3 {;
		sort demi_paire min_score`j'_random;
		replace min_score`j'_random=min_score`j'_random[_n-1] if min_score`j'_random==. & demi_paire==demi_paire[_n-1];

		sort demi_paire min_score`j'_newhh;
		replace min_score`j'_newhh=min_score`j'_newhh[_n-1] if min_score`j'_newhh==. & demi_paire==demi_paire[_n-1];
		};
		codebook min_score2_random min_score2_newhh min_score3_random min_score3_newhh;
			tab demi_paire if min_score2_random==. | min_score3_random==.;
			* it is paire 15 for which we have no random obs;
			tab wave if min_score2_newhh == . |  min_score3_newhh == .;
			* it is wave 1 for which we do not have newhh;
	
gen random5_final=random5;
	replace random5_final=0 if score2>min_score2_newhh & random5==1 & wave==2 & score2!=.;
	replace random5_final=0 if score3>min_score3_newhh & random5==1 & wave==34 & score3!=.;


	tab random5 random5_final if _merge!=2;
	label var random5_final "1 if final random obs";
	drop random5;
		
** Graph weightproba, by random; 
*histogram weightproba if weightproba<25, bin(100) by(random5_final);

drop min_score2_random - min_score3_newhh;


*******************************************************;
* we put zeroes where the baseline controls are missing; 
*******************************************************;
#delimit;
foreach var in $baseline share_inc_self share_inc_dep share_cons_income {; 
	gen `var'_m = (`var'_bl != .); 
	replace `var'_bl = 0 if `var'_bl == . ; 
		label var `var'_m "1 if `var' baseline obs non-missing"; 
	}; 

****************************;
* We define sample variables; 

gen sampleall=1;
	label var sampleall "1 if sampled";
gen samplemodel=(random5_final==0);
	label var samplemodel "1 if high probability-to-borrow";
gen noweight=1;
	label var noweight "1 if weight equal to one";

*******************************************;
*******************************************;
** TABLES PART A: Randomization & Attrition;
*******************************************;
*******************************************;

*****************************************;
* Table 1 - Summary Statistics HH survey;
*****************************************;
	********************************;
	* Table 1 - Panel A: Verifying randomization - pre-attrition sample;
		*we consider all baseline observations from households survey;

	#delimit;
	set more off;

	foreach var in shock1_bl shock1agri_bl shock1live_bl distance_soukpt_bl {;
		replace `var'=. if wave==1 & `var'==0;
		};  

	*** Baseline Balance - Household Survey - Joint Test**;

global variables_baseline_joint 
	members_resid_bl nadults_resid_bl  nchildren_resid_bl  
	head_male_bl  head_age_bl  head_educ_1_bl 
	borrowed_alamana_bl  borrowed_oformal2_bl  borrowed_informal_bl  borrowed_branching_bl 
	aloansamt_alamana_bl  aloansamt_oformal2_bl  aloansamt_informal_bl  aloansamt_branching_bl 
	act_number_bl  
	act_agri_bl inv_agri_bl sale_agri_bl  expense_agri_bl  savings_agri_bl  empl_agri_bl  work_agri_bl 
	act_livestock_bl  inv_livestock_bl  sale_livestock_bl  expense_livestock_bl  savings_livestock_bl  empl_livestock_bl  work_livestock_bl 
	act_business_bl  
	women_act_number_bl  women_act_bl  distance_soukpt_bl 
	self_empl_bl  daily_dum_bl 
	/*shock1_bl  shock1agri_bl  shock1live_bl*/  hadlost_livestockanim_bl  shock2_bl 
	consumption_bl  cons_nondurables_bl  cons_durables_bl poor_bl;

	/*;
	order $variables_baseline_joint, last;	
	
	xi: reg treatment $variables_baseline_joint i.paire if _merge!=1, cluster(demi_paire);
	testparm   members_resid_bl- poor_bl;
	*/;	
	*******************************;	
	
global variables_baseline 
	members_resid nadults_resid nchildren_resid 
	head_male head_age head_educ_1
	borrowed_alamana borrowed_oformal2 borrowed_informal borrowed_branching
	aloansamt_alamana aloansamt_oformal2 aloansamt_informal aloansamt_branching
	act_number 
	act_agri inv_agri sale_agri expense_agri savings_agri empl_agri work_agri
	act_livestock inv_livestock sale_livestock expense_livestock savings_livestock empl_livestock work_livestock
	act_business 
	women_act_number women_act distance_soukpt
	self_empl daily_dum
	shock1 shock1agri shock1live hadlost_livestockanim shock2
	consumption cons_nondurables cons_durables poor;


gen vars="";
gen N=.;
gen n_control=.;
gen mean_control=.;
gen stdev_control=.;
gen coef_treat=.;
gen p_treat=.;

local i 0;
foreach var of global variables_baseline {;
local i=`i'+1;

replace vars="`var'_bl" if _n==`i';

sum `var'_bl if treatment==0 & _merge!=1;
	replace n_control=r(N) if _n==`i';
	replace mean_control=r(mean) if _n==`i';
	replace stdev_control=r(sd) if _n==`i';


xi: reg `var'_bl treatment i.paire if _merge!=1, cluster(demi_paire);
	replace N=e(N) if _n==`i';
	mat beta1=e(b);
	test treatment=0;
	replace p_treat=r(p) if _n==`i';
 	replace coef_treat=beta1[1,1] if _n==`i'; 
	};

foreach var in mean_control stdev_control coef_treat p_treat {;
	replace `var'=round(`var', 0.001);
	};

outsheet vars N n_control mean_control stdev_control coef_treat p_treat 
	if _n<=100 using Tables_paper\Table1_pre_attrition_HHsurvey_sampleall_noweight.out, replace;

drop vars-p_treat;

	************************************;
	* Table 1 - Panel B: ATTRITION;

#delimit;
set more off;

gen attrition = 0 if _merge==3 | _merge==2;
	replace attrition =1 if _merge==2;
	label var attrition "1 if attrited";

local weightversion="noweight";
local sample="sampleall";

gen vars="";
gen N=.;
gen n_control=.;
gen mean_control=.;
gen stdev_control=.;
gen coef_treat=.;
gen p_treat=.;

	* Note: attrition var defined only for _merge== 2 | ==3;
 
local i 0;
foreach var in attrition {;
local i=`i'+1;

replace vars="`var'" if _n==`i';

sum `var' if treatment==0 & `sample'==1;
	replace n_control=r(N) if _n==`i';
	replace mean_control=r(mean) if _n==`i';
	replace stdev_control=r(sd) if _n==`i';


xi: reg `var' treatment i.paire if `sample'==1, cluster(demi_paire);
	replace N=e(N) if _n==`i';
	mat beta1=e(b);
	test treatment=0;
	replace p_treat=r(p) if _n==`i';
 	replace coef_treat=beta1[1,1] if _n==`i'; 
};


foreach var in mean_control stdev_control coef_treat p_treat {;
	replace `var'=round(`var', 0.001);
	};

outsheet vars N n_control mean_control stdev_control coef_treat p_treat 
	if _n<=100 using Tables_paper\Table1_attrition_`sample'_`weightversion'.out, replace;

drop vars-p_treat;


*******************************************;
* Table B1 - Summary Statistics Mini-Survey;
*******************************************;
	*******************************************;
	* Table A1- Panel A: Verifying randomization - pre-attrition sample; 

		*we consider all households surveyed at endline using baseline mini-enquete data;

			local weightversion="noweight";
			local sample="sampleall";

		
gen vars="";
gen N=.;
gen n_control=.;
gen mean_control=.;
gen stdev_control=.;
gen coef_treat=.;
gen p_treat=.;

	global variables_minisurvey
	m1 m2 m3 m4 m5 m6 m7c m8n m8s m9a m9b m9c m9d m9e m9fb m9fc m9g m10a m10b m11 m12 m13 m14;


local i 1;
foreach var of global variables_minisurvey {;
local i=`i'+1;

replace vars="`var'" if _n==`i';

sum `var' if treatment==0 & `sample'==1;
	replace n_control=r(N) if _n==`i';
	replace mean_control=r(mean) if _n==`i';
	replace stdev_control=r(sd) if _n==`i';


xi: reg `var' treatment i.paire if `sample'==1, cluster(demi_paire);
	replace N=e(N) if _n==`i';
	mat beta1=e(b);
	test treatment=0;
	replace p_treat=r(p) if _n==`i';
 	replace coef_treat=beta1[1,1] if _n==`i'; 
};

foreach var in mean_control stdev_control coef_treat p_treat {;
	replace `var'=round(`var', 0.001);
	};

outsheet vars N n_control mean_control stdev_control coef_treat p_treat 
	if _n>1 & _n<=100 using Tables_paper\TableB1_preattrition_MiniSurvey_`sample'_noweight.out, replace;

drop vars-p_treat;


	/*;
	*** Baseline Balance - Mini Survey - Joint Test**;
	#delimit;
	global variables_mini_joint 
		m1 m2 m3 m4 m5 m6 m7c m8n m8s m9a m9b m9c m9d m9e m9fb m9fc m9g m10a m10b m11 m12 m13 m14;

	order $variables_mini_joint, last;	
	
	xi: reg treatment $variables_mini_joint i.paire if `sample'==1, cluster(demi_paire);
	testparm m1- m14;			
	*/;			


	*******************************************;
	* Table B1- Panel B: ATTRITION + CLEANNING; 

gen lostobs = (attrition == 1 | trimobs ==1);
	label var lostobs "1 if attrited or trimmed obs"; 
gen meattrition = (attrition ==1);
	label var meattrition "1 if attrited minisurvey obs"; 

	local weightversion="noweight";
	local sample="sampleall";


gen vars="";
gen N=.;
gen n_control=.;
gen mean_control=.;
gen stdev_control=.;
gen coef_treat=.;
gen p_treat=.;

 
local i 0;
foreach var in meattrition lostobs {;
local i=`i'+1;

replace vars="`var'" if _n==`i';

sum `var' if treatment==0 & `sample'==1;
	replace n_control=r(N) if _n==`i';
	replace mean_control=r(mean) if _n==`i';
	replace stdev_control=r(sd) if _n==`i';


xi: reg `var' treatment i.paire if `sample'==1, cluster(demi_paire);
	replace N=e(N) if _n==`i';
	mat beta1=e(b);
	test treatment=0;
	replace p_treat=r(p) if _n==`i';
 	replace coef_treat=beta1[1,1] if _n==`i'; 
};


foreach var in mean_control stdev_control coef_treat p_treat {;
	replace `var'=round(`var', 0.001);
	};

outsheet vars N n_control mean_control stdev_control coef_treat p_treat 
	if _n<=100 using Tables_paper\TableB1_attrition_trimmed_`sample'_`weightversion'.out, replace;

drop vars-p_treat;


**********************************************;
* Table B3 - Attrition HH Survey;
**********************************************;
	*******************************************;
	* Table B3 - Panel A: Attrition rate;
		*Same as Table 1 - Panel B;
  
	*******************************************;
	* Table B3 - Panel B: Attrited;

global variables_attrition 
	members_resid nadults_resid nchildren_resid 
	head_male head_age head_educ_1
	borrowed_alamana borrowed_oformal2 borrowed_informal borrowed_branching
	act_number act_agri act_livestock act_business 
	women_act_number distance_soukpt
	self_empl daily_dum
	consumption consumption_pc poor;

gen vars="";
gen N=.;
gen n_surveyed=.;
gen mean_surveyed=.;
gen stdev_surveyed=.;
gen coef_attrited=.;
gen p_attrited=.;

local i 0;
foreach var of global variables_attrition {;
local i=`i'+1;

replace vars="`var'_bl" if _n==`i';

sum `var'_bl if attrition==0 & sampleall==1 & _merge!=1;
	replace n_surveyed=r(N) if _n==`i';
	replace mean_surveyed=r(mean) if _n==`i';
	replace stdev_surveyed=r(sd) if _n==`i';


xi: reg `var'_bl attrition i.paire if sampleall==1 & _merge!=1, cluster(demi_paire);
	replace N=e(N) if _n==`i';
	mat beta1=e(b);
	test attrition=0;
	replace p_attrited=r(p) if _n==`i';
 	replace coef_attrited=beta1[1,1] if _n==`i'; 
};

foreach var in mean_surveyed stdev_surveyed coef_attrited p_attrited {;
	replace `var'=round(`var', 0.001);
	};

outsheet vars-p_attrited 
	if _n<=100 using Tables_paper\TableB3_Attrited_HHsurvey_sampleall_noweight.out, replace;

drop vars-p_attrited;

	**********************************************;
	* Table B3- Panel C: Attrited by group;

gen attrition_treat = attrition*treatment;
	label var attrition_treat "1 if attrited x treated village"; 

gen vars="";
gen N=.;
gen n_controlna=.;
gen mean_controlna=.;
gen stdev_controlna=.;
gen coef_treatattr=.;
gen p_treatattr=.;


local i 0;
foreach var of global variables_attrition {;
local i=`i'+1;

replace vars="`var'_bl" if _n==`i';

sum `var'_bl if attrition==0 & treatment==0 & sampleall==1 & _merge!=1;
	replace n_controlna=r(N) if _n==`i';
	replace mean_controlna=r(mean) if _n==`i';
	replace stdev_controlna=r(sd) if _n==`i';


xi: reg `var'_bl attrition treatment attrition_treat i.paire if sampleall==1 & _merge!=1, cluster(demi_paire);
	replace N=e(N) if _n==`i';
	mat beta1=e(b);
	test attrition_treat=0;
	replace p_treatattr=r(p) if _n==`i';
 	replace coef_treatattr=beta1[1,3] if _n==`i'; 
};

foreach var in mean_controlna stdev_controlna coef_treatattr p_treatattr {;
	replace `var'=round(`var', 0.001);
	};

outsheet vars-p_treatattr 
	if _n<=100 using Tables_paper\TableB3_Attritedbygroup_HHsurvey_sampleall_noweight.out, replace;

drop vars-p_treatattr;


***********************************;
* Table B4 - Attrition Mini-Survey; 
***********************************;
	***********************************;
	* Table B4 - Panel A: Attrition rate;
		*Same as Table A1 - Panel B;

	***********************************;
	* Table B4 - Panel B: Attrited;

global variables_minisurvey
	m1 m2 m3 m4 m5 m6 m7c m8n m8s m9a m9b m9c m9d m9e m9fb m9fc m9g m10a m10b m11 m12 m13 m14;

gen vars="";
gen N=.;
gen n_surveyed=.;
gen mean_surveyed=.;
gen stdev_surveyed=.;
gen coef_attrited=.;
gen p_attrited=.;

local i 0;
foreach var of global variables_minisurvey {;
local i=`i'+1;

replace vars="`var'" if _n==`i';

sum `var' if meattrition==0 & sampleall==1;
	replace n_surveyed=r(N) if _n==`i';
	replace mean_surveyed=r(mean) if _n==`i';
	replace stdev_surveyed=r(sd) if _n==`i';


xi: reg `var' meattrition i.paire if sampleall==1, cluster(demi_paire);
	replace N=e(N) if _n==`i';
	mat beta1=e(b);
	test meattrition=0;
	replace p_attrited=r(p) if _n==`i';
 	replace coef_attrited=beta1[1,1] if _n==`i'; 
};

foreach var in mean_surveyed stdev_surveyed coef_attrited p_attrited {;
	replace `var'=round(`var', 0.001);
	};

outsheet vars-p_attrited 
	if _n<=100 using Tables_paper\TableB4_Attrited_MEnquete_sampleall_noweight.out, replace;

drop vars-p_attrited;


	***********************************;
	* Table B4 - Panel C: Attrited by group;

gen vars="";
gen N=.;
gen n_controlna=.;
gen mean_controlna=.;
gen stdev_controlna=.;
gen coef_treatattr=.;
gen p_treatattr=.;

gen meattrition_treat = meattrition*treatment;
	label var meattrition_treat "1 if attrited x treated village - minisurvey sample"; 


local i 0;
foreach var of global variables_minisurvey {;
local i=`i'+1;

replace vars="`var'" if _n==`i';

sum `var' if meattrition==0 & treatment==0 & sampleall==1;
	replace n_controlna=r(N) if _n==`i';
	replace mean_controlna=r(mean) if _n==`i';
	replace stdev_controlna=r(sd) if _n==`i';


xi: reg `var' meattrition treatment meattrition_treat i.paire if sampleall==1, cluster(demi_paire);
	replace N=e(N) if _n==`i';
	mat beta1=e(b);
	test meattrition_treat=0;
	replace p_treatattr=r(p) if _n==`i';
 	replace coef_treatattr=beta1[1,3] if _n==`i'; 
};

foreach var in mean_controlna stdev_controlna coef_treatattr p_treatattr {;
	replace `var'=round(`var', 0.001);
	};

outsheet vars-p_treatattr 
	if _n<=100 using Tables_paper\TableB4_Attritedbygroup_MEnquete_sampleall_noweight.out, replace;

drop vars-p_treatattr;


*************************************;
*************************************;
* WE KEEP ENDLINE SAMPLE & SAVE DATA;
*************************************;
drop if _merge==2;
	* with this command we drop all baseline observations that are not matched with endline (_merge ==2);
	* we now have ALL ENDLINE obs;

drop if trimobs==1;

* Distribution of main outcomes;
foreach var of global variables_trim {;
	sum `var' if samplemodel==1, detail;
	};

tab samplemodel;

save Output/endline_baseline_outcomes.dta,replace;



***************************************************************;
***************************************************************;
***************************************************************;
***************************************************************;
***************** ANALYSIS ************************************; 
****************TABLES PART B *********************************;
***************************************************************;

#delimit;
set more off;
use Output/endline_baseline_outcomes.dta,clear;

***********************************************;
******** GLOBALS;
***********************************************;
*** HOUSEHOLD AND RESPONDENT variables; 

* Household Controls;

global householdcontrols 
	members_resid_bl nadults_resid_bl head_age_bl act_livestock_bl act_business_bl borrowed_total_bl
	members_resid_d_bl nadults_resid_d_bl head_age_d_bl act_livestock_d_bl act_business_d_bl borrowed_total_d_bl;

	foreach var in members_resid nadults_resid head_age act_livestock act_business borrowed_total {;
		replace `var'_d_bl=1 if `var'_m==0 & `var'_d_bl==.;
		};  	

global respondentcontrols ccm_resp_activ other_resp_activ ccm_resp_activ_d other_resp_activ_d;

	gen ccm_resp_activ_d=(ccm_resp_activ==.);
		replace ccm_resp_activ=0 if ccm_resp_activ_d==1; 
		label var ccm_resp_activ_d "1 if ccm_resp_activ missing"; 

	gen other_resp_activ_d=(other_resp_activ==.);
		replace other_resp_activ=0 if other_resp_activ_d==1; 
		label var other_resp_activ_d "1 if other_resp_activ missing"; 


*******************************************;
* Table B2 - Summary Statistics Mini-Survey;
*******************************************;
	*******************************************;
	* Table B2- Panel A: Verifying randomization - post-attrition sample - households with high likelihood to borrow; 

		*we consider high PTB households surveyed at endline using baseline mini-enquete data;

			local weightversion="noweight";
			local sample="samplemodel";

gen vars="";
gen N=.;
gen n_control=.;
gen mean_control=.;
gen stdev_control=.;
gen coef_treat=.;
gen p_treat=.;

global variables_minisurvey
	m1 m2 m3 m4 m5 m6 m7c m8n m8s m9a m9b m9c m9d m9e m9fb m9fc m9g m10a m10b m11 m12 m13 m14;

foreach var of global variables_minisurvey {;
	replace `var'=. if trimobs==1;
	};

local i 1;
foreach var of global variables_minisurvey {;
local i=`i'+1;

replace vars="`var'" if _n==`i';

sum `var' if treatment==0 & `sample'==1;
	replace n_control=r(N) if _n==`i';
	replace mean_control=r(mean) if _n==`i';
	replace stdev_control=r(sd) if _n==`i';


xi: reg `var' treatment i.paire if `sample'==1, cluster(demi_paire);
	replace N=e(N) if _n==`i';
	mat beta1=e(b);
	test treatment=0;
	replace p_treat=r(p) if _n==`i';
 	replace coef_treat=beta1[1,1] if _n==`i'; 
};

foreach var in mean_control stdev_control coef_treat p_treat {;
	replace `var'=round(`var', 0.001);
	};

outsheet vars N n_control mean_control stdev_control coef_treat p_treat 
	if _n>1 & _n<=100 using Tables_paper\TableB2_postattrition_MiniSurvey_`sample'_noweight.out, replace;

drop vars-p_treat;


***************************************************;		
*** Variables for the construction of PAPER TABLES;
**************************************************;
global variables_total_loans1 /*table 2 - Panel A*/
client 
borrowed_alamana 
borrowed_oamc 
borrowed_oformal 
borrowed_branching 
borrowed_informal 
borrowed_total;

global variables_total_loans2 /*table 2 - Panel B*/
loansamt_alamana
loansamt_oamc
loansamt_oformal
loansamt_branching
loansamt_informal
loansamt_total
cons_repay;

global variables_acttotal /*table 3*/
assets_total
output_total
expense_total
inv_total
profit_total;
global variables_acttotal_2
self_empl;

global variables_income /*table 4*/
income
income_work
profit_total
income_dep
income_assetsales
income_other_new;

global variables_timeworked_bis /*table 5*/
hours_all_age6_65
hours_self_age6_65 
hours_outside_age6_65 
hours_chores_age6_65
nmember_age6_65
hours_all_age6_16
hours_self_age6_16 
hours_outside_age6_16 
hours_chores_age6_16
nmember_age6_16
hours_all_age16_20 
hours_self_age16_20 
hours_outside_age16_20
hours_chores_age16_20
nmember_age16_20 
hours_all_age20_50 
hours_self_age20_50 
hours_outside_age20_50
hours_chores_age20_50
nmember_age20_50 
hours_all_age50_65
hours_self_age50_65
hours_outside_age50_65
hours_chores_age50_65
nmember_age50_65;

global variables_cons1 /*table 6*/
consumption
cons_durables
cons_nondurables
cons_food
cons_health
cons_schooling
cons_temp
cons_social;

global variables_social /*table 7*/
sharekids_inschool
shareteens_inschool
women_index
women_act;

global variables_social_2
women_act_number; 

global variables_summary /* Table 8 - Borrowers-all weigthed-externalities*/
client
assets_total
output_total
expense_total
profit_total
income_dep
hours_self_age16_65 
hours_outside_age16_65
consumption;  

global variables_late /*LATE table 9*/
assets_total 
output_total 
expense_total 
profit_total 
self_empl
income_dep
hours_self_age16_65 
hours_outside_age16_65
consumption;  



*************************;
* Online Appendix Tables;

global variables_admindata_loans /*table B5 */
client_admin 
admin_time_1stloan_baseline 
admin_time_1stloan6 
admin_time_1stloan612 
admin_time_1stloan1224
admin_nloans 
admin_pgroup 
admin_pindiv 
admin_phousing 
admin_amount 
admin_amount_group_c 
admin_amount_indiv_c 
admin_amount_house_c 
admin_frequency 
admin_loanlength 
admin_plivestock 
admin_pcommerce 
admin_pservices
admin_phandicraft
admin_1stloan_amt 
admin_1stloan_group 
admin_1stloan_indiv 
admin_1stloan_house
admin_clienty1 
admin_clienty2 
admin_loany1_amt_c 
admin_loany2_amt_c;

global variables_agriculture /*table B6 - Panel A*/ 
asset_agri
prod_agri
expense_agri 
inv_agri
profit_agri;

global variables_agriculture_2
selfempl_agri
act_total_agri;

global variables_agriculture_3
land
land_ha
land_expl_ha;

global variables_livestock /*table B6 - Panel B*/
astock_livestock
sale_livestock 
expense_livestock
inv_livestock  
profit_livestock;
global variables_livestock_2 
selfempl_livestock
act_total_live; 

global variables_business /*table B6 - Panel C*/
asset_business
sale_business
expense_business
inv_business_assets
profit_business;

global variables_business_2 
selfempl_business
act_number_business;


drop if client==.;


*************************************;
*** REGRESSIONS: TABLES 2 to 7 and A3; 

#delimit;
set more off;
			local weightversion="noweight";
			local sample="samplemodel";


global main_reg
variables_total_loans1
variables_total_loans2
variables_acttotal
variables_acttotal_2
variables_income
variables_timeworked_bis
variables_cons1
variables_social
variables_social_2
/* Online Appendix */
variables_agriculture
variables_agriculture_2
variables_agriculture_3
variables_livestock
variables_livestock_2
variables_business
variables_business_2;


local j 1;
foreach group in $main_reg {;

		if `j'==1 | `j'==4 | `j'==8 | `j'==11 | `j'==14 | `j'==16 {;
			local digit=3;
			};

		if `j'==2 | `j'==3 | `j'==5 | `j'==7 | `j'==10 | `j'==13 | `j'==15 {;
			local digit=0;
			}; 

		if `j'==9 | `j'==12  {;
			local digit=2;
			}; 

		if `j'==6 {;
			local digit=1;
			}; 

	local i 1;
	foreach var in $`group' {;

		if `i'==1 {;
			local append_replace="replace";
			};
		if `i'!=1 {;
			local append_replace="append";
			};

sum `var' if treatment==0 & `sample'==1;
	local mean_control=r(mean);


 xi: reg `var' treatment ${householdcontrols} ${respondentcontrols} i.paire if `sample'==1, cluster(demi_paire) ;
outreg treatment using Tables_paper\table_TC_`group'_`sample'_`weightversion'.out, nocons nonote se `append_replace' nor2 
addstat("control mean", `mean_control')
adec(`digit') bdec(`digit')sigsymb(***,**,*);

local i=`i'+1;
}; 
local j=`j'+1;
}; 


***************************************;
** TABLE 2-7: INDEX & CORRECTED P-VALUE
***************************************;
* We build the indexes for the top 25% observations as we run the regressions on this population;

* Construction of z-scores for each table ***;

** z score table 2;
foreach var in $variables_total_loans1 $variables_total_loans2 {;
	sum `var' if treatment==0 & samplemodel==1, d;
	gen z_credit`var'=(`var'-r(mean))/r(sd);
	};		
	egen z_table2=rmean(z_credit*);
	drop z_credit*;

	label var z_table2 "table 2: index of dependent variables";		
 	
** z score table 3;		
foreach var in $variables_acttotal $variables_acttotal_2 {;
	sum `var' if treatment==0 & samplemodel==1, d;
	gen z_act`var'=(`var'-r(mean))/r(sd);
	};		
	egen z_table3=rmean(z_act*);			
	drop z_act*;

	label var z_table3 "table 3: index of dependent variables";		
		
** z score table 4;			
foreach var in $variables_income {;
	sum `var' if treatment==0 & samplemodel==1, d;
	gen z_inc`var'=(`var'-r(mean))/r(sd);
	};		
	egen z_table4=rmean(z_inc*);
	drop z_inc*;			

	label var z_table4 "table 4: index of dependent variables";		
		
** z score table 5;	
*rescale variables for having "good" outcomes. This is only done on kids(6-15 y/o);
	gen hours_all_age6_16g=-hours_all_age6_16;
	gen hours_self_age6_16g=-hours_self_age6_16	;
	gen hours_outside_age6_16g=-hours_outside_age6_16;		
	gen hours_chores_age6_16g=-hours_chores_age6_16;		

foreach var of varlist  
hours_all_age6_65
hours_self_age6_65 
hours_outside_age6_65 
hours_chores_age6_65
hours_all_age6_16g
hours_self_age6_16g 
hours_outside_age6_16g 
hours_chores_age6_16g
hours_all_age16_20 
hours_self_age16_20 
hours_outside_age16_20
hours_chores_age16_20
hours_all_age20_50 
hours_self_age20_50 
hours_outside_age20_50
hours_chores_age20_50
hours_all_age50_65
hours_self_age50_65
hours_outside_age50_65
hours_chores_age50_65{;
sum `var' if treatment==0 & samplemodel==1, d;
	gen z_hours`var'=(`var'-r(mean))/r(sd);
	};		
	egen z_table5=rmean(z_hours*);
	drop z_hours*;
			
	label var z_table5 "table 5: index of dependent variables";		
	drop hours_all_age6_16g- hours_chores_age6_16g;

** z score table 6;	
foreach var in $variables_cons1 {;
	sum `var' if treatment==0 & samplemodel==1, d;
	gen z_cons`var'=(`var'-r(mean))/r(sd);
	};		
	egen z_table6=rmean(z_cons*);
	drop z_cons*;		

	label var z_table6 "table 6: index of dependent variables";		


** z score table 7;	
foreach var in $variables_social $variables_social_2 {; 
	sum `var' if treatment==0 & samplemodel==1, d;
	gen z_soc`var'=(`var'-r(mean))/r(sd);
	};		
	egen z_table7=rmean(z_soc*);
	drop z_soc*;

	label var z_table7 "table 7: index of dependent variables";		


****************************************;
** we replace all random obs by missing;
foreach var of varlist z_table2-z_table7 {;
	replace `var'=. if samplemodel==0;
	};
	

********************************;
*** HOCHBERG CORRECTED P-VALUES;

local weightversion="noweight";
local sample="samplemodel";

foreach var in z_table2 z_table3 z_table4 z_table5 z_table6 z_table7 {; 

sum `var' if treatment==0 & `sample'==1;
	local mean_control=r(mean);

	xi: reg `var' treatment ${householdcontrols} ${respondentcontrols} i.paire if `sample'==1, cluster(demi_paire) ;
	test treatment;
	gen pval_`var'=r(p);

	
	outreg treatment using Tables_paper\table_TC_`var'_`sample'_`weightversion'.out, replace nocons nonote se nor2 
	addstat("control mean", `mean_control')
	adec(3) bdec(3)sigsymb(***,**,*);
	};


gen var="";
gen k=.;
local i 0;
foreach var in pval_z_table2 pval_z_table3 pval_z_table4 pval_z_table5 pval_z_table6 pval_z_table7{;
	local i=`i'+1;
	replace k=`var' if _n==`i';
	replace var="`var'" if _n==`i';
	};

sort k;
gen i=.;
replace i=_n ;
replace i=. if k==.; 

gen new_pval=k*(6+1-i);
	replace new_pval=round(new_pval, 0.001);

gen new_pval_string=string(new_pval);

gen pval_corr=">0.999" if _n<=6;
replace pval_corr=new_pval_string if new_pval<=0.999;

sort var;
outsheet var pval_corr 
	if var!="" using Tables_paper\Tables27_correctedpval.out, replace;

#delimit;
set more off;
drop pval_z_table2- pval_corr;


*****************************;
** TABLE 8 - EXTERNALITIES
*****************************;
	**********************************************;
	* Table 8 - Panel A: sample model, no weights;

	local i 1;
	foreach var of global variables_summary {;

		if `i'==1 {;
			local digit=3;
			};
		if `i'==2 | `i'==3 | `i'==4 | `i'==5  | `i'==6 | `i'==9 {;
			local digit=0;
			}; 
		if  `i'==7 | `i'==8{;
			local digit=1;
			}; 


		if `i'==1 {;
			local append_replace="replace";
			};
		if `i'!=1 {;
			local append_replace="append";
			};

sum `var' if treatment==0 & samplemodel==1;
	local mean_control=r(mean);


 xi: reg `var' treatment ${householdcontrols} ${respondentcontrols} i.paire if samplemodel==1, cluster(demi_paire) ;
outreg treatment using Tables_paper\table_TC_variables_summary_samplemodel_noweight.out, nocons nonote se `append_replace' nor2 
addstat("control mean", `mean_control')
adec(`digit') bdec(`digit')sigsymb(***,**,*);

local i=`i'+1;
}; 


	**********************************************;
 	* Table 8 - PANEL B: all sample, proba weights;

	local i 1;
	foreach var of global variables_summary {;

		if `i'==1 {;
			local digit=3;
			};
		if `i'==2 | `i'==3 | `i'==4 | `i'==5  | `i'==6 | `i'==9 {;
			local digit=0;
			}; 
		if  `i'==7 | `i'==8{;
			local digit=1;
			}; 

		if `i'==1 {;
			local append_replace="replace";
			};
		if `i'!=1 {;
			local append_replace="append";
			};

sum `var' [iweight=weightproba] if treatment==0 & sampleall==1;
	local mean_control=r(mean);


 xi: reg `var' treatment ${householdcontrols} ${respondentcontrols} i.paire [pweight=weightproba] if sampleall==1, cluster(demi_paire) ;
outreg treatment using Tables_paper\table_TC_variables_summary_sampleall_weightproba.out, nocons nonote se `append_replace' nor2 
addstat("control mean", `mean_control')
adec(`digit') bdec(`digit')sigsymb(***,**,*);

local i=`i'+1;
}; 


	*******************************************;
 	* Table 8 - PANEL C: all sample, no weights
	*******************************************;
	** SCORE: we estimate score by the end of the evaluation time frame;

	* we predict the likelihood to borrow;

	sum client if treatment==1;
		local mean=r(mean);

	xi: logit client m1 m2 m3 m4 m5 m6_c m7c m8n m8s m9a m9b m9c m9d_c m9e m9fb m9fc m9g_c m10a_c m10b_c m11 m12 m13_c m14 i.paire if treatment==1;

	predict phat;
		local obs=e(N);
		local rsquared =e(r2_p);
		label var phat "predicted probability-to-borrow (by the end of the eval time frame)";

	sort demi_paire phat;
	count if demi_paire!=demi_paire[_n+1] & phat!=. & treatment==1; 
		local nvillages=r(N);

	/* Table B8 */;
	outreg m1 m2 m3 m4 m5 m6_c m7c m8n m8s m9a m9b m9c m9d_c m9e m9fb m9fc m10a_c m10b_c m11 m12 m13_c m14
		using "Tables_paper\table_borrowingmodel_endline.out", nonote se sigsymb(***,**,*) replace
		nolabel addstat("Mean dependent variable", `mean', "Pseudo R2", `rsquared', "N villages", `nvillages')
		adec(3) bdec(3);

	* we replace by zero perfectly predicted score by the village dummy;
	replace phat=0 if phat==.;

	*kdensity phat;

** top & bottom 30% for the whole sample;
foreach j in 30 {; 
	_pctile phat, p(`j');
	gen phat_low`j'=(phat<= r(r1));
		label var phat_low30 "1 if propensity to borrow in the bottom 30%"; 

	local k = 100 - `j';
	_pctile phat, p(`k');
	gen phat_high`j'=(phat> r(r1));
		label var phat_high30 "1 if propensity to borrow in the top 30%"; 

	gen T_phat_low`j'=treatment*phat_low`j';
		label var T_phat_low30 "1 if treated village and in the bottom 30% propensity to borrow"; 		

	gen T_phat_high`j'=treatment*phat_high`j';
		label var T_phat_high30 "1 if treated village and in the top 30% propensity to borrow"; 		
	};


	******************************************;
	** Regressions with bottom and top 30% of the total sample;

	#delimit;
	set more off;
foreach j in 30 {; 
	local i 1;
	foreach var of global variables_summary {;

		if `i'==1 {;
			local digit=3;
			};
		if `i'==2 | `i'==3 | `i'==4 | `i'==5  | `i'==6 | `i'==9 {;
			local digit=0;
			}; 
		if  `i'==7 | `i'==8{;
			local digit=1;
			}; 

		if `i'==1 {;
			local append_replace="replace";
			};
		if `i'!=1 {;
			local append_replace="append";
			};

sum `var' if treatment==0 & (phat_low`j'==1 | phat_high`j'==1) ;
	local mean_control=r(mean);

sum `var' if treatment==0 & phat_high`j'==1;
	local mean_control_high=r(mean);

sum `var' if treatment==0 & phat_low`j'==1;
	local mean_control_low=r(mean);
	
xi: reg `var' T_phat_low`j' T_phat_high`j' phat_low`j' ${householdcontrols} ${respondentcontrols} i.paire if (phat_low`j'==1 | phat_high`j'==1), cluster(demi_paire);
	test T_phat_low`j'=T_phat_high`j';
 			local F_Tlow_Thigh=r(F);
 			local p_Tlow_Thigh=r(p);
 
outreg T_phat_high`j' T_phat_low`j' using "tables_paper\table_externalities_binary_sampleall_noweight_p`j'.out", nocons nonote se `append_replace' nor2 
addstat("control mean", `mean_control', "control mean high", `mean_control_high', "control mean low", `mean_control_low', "F_Tlow_Thigh", `F_Tlow_Thigh',"Prob>F",`p_Tlow_Thigh') adec(3) bdec(`digit') sigsymb(***,**,*);

local i=`i'+1;
};
};


*****************************;
** TABLE 9 - OLS & LATE;
*****************************;
	***************************************************;
	* Table 9 - Panel A: OLS, model sample, no weights;

	local i 1;
	foreach var of global variables_late {;
		if `i'==5 {;
			local digit=3;
			};
		if `i'==1 | `i'==2 | `i'==3 | `i'==4 | `i'==6 | `i'==9 {;
			local digit=0;
			}; 
		if `i'==7 | `i'==8 {;
			local digit=1;
			}; 

		if `i'==1 {;
			local append_replace="replace";
			};
		if `i'!=1 {;
			local append_replace="append";
			};
scalar drop _all;

sum `var' if client==0 & samplemodel==1 & treatment==1 ;
	local mean_client=r(mean);


 xi: reg `var' client ${householdcontrols} ${respondentcontrols} i.paire if samplemodel==1 & treatment==1, cluster(demi_paire) ;
outreg client using Tables_paper\table_client_samplemodel_noweight.out, nocons nonote se `append_replace' nor2 
addstat("client mean", `mean_client')
adec(`digit') bdec(`digit')sigsymb(***,**,*);

local i=`i'+1;
}; 


	***************************************************;
	* Table 9 - Panel B: LATE, model sample, no weights;

#delimit;
set more off;
	local i 1;
	foreach var of global variables_late {;
		if `i'==5 {;
			local digit=3;
			};
		if `i'==1 | `i'==2 | `i'==3 | `i'==4 | `i'==6 | `i'==9 {;
			local digit=0;
			}; 
		if `i'==7 | `i'==8 {;
			local digit=1;
			}; 

		if `i'==1 {;
			local append_replace="replace";
			};
		if `i'!=1 {;
			local append_replace="append";
			};
scalar drop _all;


sum `var' if treatment==0 & samplemodel==1 ;
	local mean_control=r(mean);
	scalar mean_control=r(mean);
	summ `var' if samplemodel==1, detail; 
	local mean = r(mean); 
	local std_s = r(sd); 
	local p50_s = r(p50);

sum `var' if treatment==1 & client==0 & samplemodel==1;
scalar mean_nevertaker=r(mean);
sum `var' if treatment==0;
scalar mean_control1=r(mean);
local mean_control1=r(mean);
scalar complier=(mean_control1-mean_nevertaker*0.83)/0.17;
local complier=complier;


 xi: ivreg `var' (client = treatment) ${householdcontrols} ${respondentcontrols} i.paire if samplemodel==1, cluster(demi_paire) ;
outreg client using Tables_paper\table_late_samplemodel_noweight.out, nocons nonote se `append_replace' nor2 
addstat("control mean", `mean_control', "complier mean", `complier', "sample mean", `mean', "median", `p50_s')
adec(`digit') bdec(`digit')sigsymb(***,**,*);

local i=`i'+1;
}; 

		
**************************************************;
** Table B5: Summary statistics of Alamana loans, admin data;

set more off;
gen vars2="";
gen n_client=.;
gen mean_client=.;
gen stdev_client=.;


local i 0;
foreach var of global variables_admindata_loans {;
local i=`i'+1;

replace vars2="`var'" if _n==`i';

sum `var' if client_admin==1 & samplemodel==1;
	replace n_client=r(N) if _n==`i';
	replace mean_client=r(mean) if _n==`i';
	replace stdev_client=r(sd) if _n==`i';

};


foreach var in mean_client stdev_client  {;
	replace `var'=round(`var', 0.001);
	};
#delimit;
outsheet vars2 n_client mean_client stdev_client  
	if _n<=100 using Tables_paper\TableB5_loans_admin_samplemodel_noweight.out, replace;

drop vars2- stdev_client;

*************************;
** Table B7 - Robustness; 
*************************;
	**************************************************;
	* Table B7 - Panel A: without controls;
# delimit;
set more off;
	local i 1;
	foreach var of global variables_summary {;

		if `i'==1 {;
			local digit=3;
			};
		if `i'==2 | `i'==3 | `i'==4 | `i'==5  | `i'==6 | `i'==9 {;
			local digit=0;
			}; 
		if  `i'==7 | `i'==8{;
			local digit=1;
			}; 


		if `i'==1 {;
			local append_replace="replace";
			};
		if `i'!=1 {;
			local append_replace="append";
			};

sum `var' if treatment==0 & samplemodel==1;
	local mean_control=r(mean);


 xi: reg `var' treatment i.paire if samplemodel==1, cluster(demi_paire) ;
outreg treatment using Tables_paper\tableB7_wocontrols_variables_summary_samplemodel_noweight.out, nocons nonote se `append_replace' nor2 
addstat("control mean", `mean_control')
adec(`digit') bdec(`digit')sigsymb(***,**,*);

local i=`i'+1;
};

	****************************************;
	* Table B7 - Panel B1: main specification;
		* same as Table8 - Panel A;

 
	****************************************;
	* Table B7 - Panel B2: without clustered st errors;


	local i 1;
	foreach var of global variables_summary {;

		if `i'==1 {;
			local digit=3;
			};
		if `i'==2 | `i'==3 | `i'==4 | `i'==5  | `i'==6 | `i'==9 {;
			local digit=0;
			}; 
		if  `i'==7 | `i'==8{;
			local digit=1;
			}; 


		if `i'==1 {;
			local append_replace="replace";
			};
		if `i'!=1 {;
			local append_replace="append";
			};

sum `var' if treatment==0 & samplemodel==1;
	local mean_control=r(mean);


 xi: reg `var' treatment ${householdcontrols} ${respondentcontrols} i.paire if samplemodel==1;
outreg treatment using Tables_paper\TableB7_nonclustered_variables_summary_samplemodel_noweight.out, nocons nonote se `append_replace' nor2 
addstat("control mean", `mean_control')
adec(`digit') bdec(`digit')sigsymb(***,**,*);

local i=`i'+1;
}; 


	********************************************************;
	* Table B7 - Panel C: extended set of household controls;


	foreach var in borrowed_oformal2 borrowed_informal sale_livestock savings_livestock hadlost_livestockanim 
	share_inc_self share_inc_dep share_cons_income {;
	gen `var'_bl_d=(`var'_m==0);
		label var `var'_bl_d "1 if missing obs at baseline"; 
	};

	global extendedcontrols 
		borrowed_oformal2_bl borrowed_informal_bl sale_livestock_bl savings_livestock_bl hadlost_livestockanim_bl 
		share_inc_self_bl share_inc_dep_bl share_cons_income_bl
		borrowed_oformal2_bl_d borrowed_informal_bl_d sale_livestock_bl_d savings_livestock_bl_d hadlost_livestockanim_bl_d 
		share_inc_self_bl_d share_inc_dep_bl_d share_cons_income_bl_d;

	local i 1;
	foreach var of global variables_summary {;

		if `i'==1 {;
			local digit=3;
			};
		if `i'==2 | `i'==3 | `i'==4 | `i'==5  | `i'==6 | `i'==9 {;
			local digit=0;
			}; 
		if  `i'==7 | `i'==8{;
			local digit=1;
			}; 

		if `i'==1 {;
			local append_replace="replace";
			};
		if `i'!=1 {;
			local append_replace="append";
			};

sum `var' if treatment==0 & samplemodel==1;
	local mean_control=r(mean);

 xi: reg `var' treatment `var'_bl `var'_m ${extendedcontrols} ${householdcontrols} ${respondentcontrols} i.paire if samplemodel==1, cluster(demi_paire);
outreg treatment using Tables_paper\tableB7_morecontrols_variables_summary_samplemodel_noweight.out, nocons nonote se `append_replace' nor2 
addstat("control mean", `mean_control')
adec(`digit') bdec(`digit')sigsymb(***,**,*);

local i=`i'+1;
}; 

**************************************************************************;
** Table B9 - Comparison of borrowers sampled at baseline and at endline; 
**************************************************************************;

	*************************************;
	* Table B9- Panel A: Comparing Control and Treatment groups of borrowers sampled at endline; 
		* we use baseline mini-survey data;


gen vars="";
gen N=.;
gen n_control=.;
gen mean_control=.;
gen stdev_control=.;
gen coef_treat=.;
gen p_treat=.;

global variables_minisurvey
	m1 m2 m3 m4 m5 m6 m7c m8n m8s m9a m9b m9c m9d m9e m9fb m9fc m9g m10a m10b m11 m12 m13 m14;

local i 1;
foreach var of global variables_minisurvey {;
local i=`i'+1;

replace vars="`var'" if _n==`i';

sum `var' if treatment==0 & samplemodel==1 & _merge==1;
	replace n_control=r(N) if _n==`i';
	replace mean_control=r(mean) if _n==`i';
	replace stdev_control=r(sd) if _n==`i';


xi: reg `var' treatment i.paire if samplemodel==1 & _merge==1, cluster(demi_paire);
	replace N=e(N) if _n==`i';
	mat beta1=e(b);
	test treatment=0;
	replace p_treat=r(p) if _n==`i';
 	replace coef_treat=beta1[1,1] if _n==`i'; 
};

foreach var in mean_control stdev_control coef_treat p_treat {;
	replace `var'=round(`var', 0.001);
	};

outsheet vars N n_control mean_control stdev_control coef_treat p_treat 
	if _n>1 & _n<=100 using Tables_paper\TableB9_NewBorrowers_MiniSurvey_samplemodel_noweight.out, replace;

drop vars-p_treat;

	*************************************;
	* Table B9- Panel B: Comparing borrowers selected at endline with those selected at baseline; 
		*we use baseline mini-survey data;

#delimit;
gen endborrower=0 if samplemodel==1;
	replace endborrower=1 if samplemodel==1 & _merge==1;
	label var endborrower "1 if only-endline observation"; 
 
gen vars="";
gen N=.;
gen n_control=.;
gen mean_control=.;
gen stdev_control=.;
gen coef_end=.;
gen p_end=.;


local i 1;
foreach var of global variables_minisurvey {;
local i=`i'+1;

replace vars="`var'" if _n==`i';

sum `var' if endborrower==0 & samplemodel==1;
	replace n_control=r(N) if _n==`i';
	replace mean_control=r(mean) if _n==`i';
	replace stdev_control=r(sd) if _n==`i';


xi: reg `var' endborrower i.paire if samplemodel==1, cluster(demi_paire);
	replace N=e(N) if _n==`i';
	mat beta1=e(b);
	test endborrower=0;
	replace p_end=r(p) if _n==`i';
 	replace coef_end=beta1[1,1] if _n==`i'; 
};

foreach var in mean_control stdev_control coef_end p_end{;
	replace `var'=round(`var', 0.001);
	};

outsheet vars N n_control mean_control stdev_control coef_end p_end
	if _n>1 & _n<=100 using Tables_paper\TableB9_Borrowers_End_Base_MiniSurvey_samplemodel_noweight.out, replace;

drop vars-p_end;


**************************************************************************;
** Table B10 - Effect on borrowers sampled at endline; 
**************************************************************************;
tab treatment endborrower; 

gen T_endborrower = (treatment *  endborrower);
	label var T_endborrower "1 if only-endline observation and treated village"; 

gen T_baseborrower = 1 if treatment==1 & endborrower==0;
	replace T_baseborrower = 0 if endborrower==1 | treatment==0; 
	label var T_baseborrower "1 if observation sampled at baseline and treated village"; 

		
	local i 1;
	foreach var of global variables_summary {;

		if `i'==1 {;
			local digit=3;
			};
		if `i'==2 | `i'==3 | `i'==4 | `i'==5  | `i'==6 | `i'==9 {;
			local digit=0;
			}; 
		if  `i'==7 | `i'==8{;
			local digit=1;
			}; 

		if `i'==1 {;
			local append_replace="replace";
			};
		if `i'!=1 {;
			local append_replace="append";
			};

sum `var' if treatment==0 & samplemodel==1;
	local mean_control=r(mean);

sum `var' if treatment==0 & samplemodel==1 & endborrower==1;
	local mean_control_end=r(mean);

sum `var' if treatment==0 & samplemodel==1 & endborrower==0;
	local mean_control_base=r(mean);
	
xi: reg `var' T_endborrower T_baseborrower ${householdcontrols} ${respondentcontrols} i.paire if samplemodel==1, cluster(demi_paire);
	test T_endborrower=T_baseborrower;
 			local F_Tbase_Tend=r(F);
 			local p_Tbase_Tend=r(p);
 
outreg T_endborrower T_baseborrower using Tables_paper\TableB10_endbase_borrowers_modelsample_noweight.out, nocons nonote se `append_replace' nor2 
addstat("control mean", `mean_control', "control mean end", `mean_control_end', "control mean base", `mean_control_base',  
"F_Tbase_Tend", `F_Tbase_Tend',"Prob>F",`p_Tbase_Tend') 
adec(3) bdec(`digit') sigsymb(***,**,*);

local i=`i'+1;
};

	
/*;
***************************************************;
** TABLE 8 PANEL C: JOINT TEST EXTERNALITIES;
***************************************************;

global y1 assets_total output_total expense_total profit_total self_empl income_dep hours_self_age16_65 hours_outside_age16_65 consumption cons_social; 

keep if phat_low30==1 | phat_high30==1;
global Estimates;
global cons1;
global cons2;

gen vars = "";
gen pvalue = .;

*** regression without weights ***;
	foreach varT in T_phat_low30 T_phat_high30 {;
	capture drop `varT'_r;
	quietly xi: reg `varT' phat_low30 ${householdcontrols} ${respondentcontrols} i.paire if `varT'!=.;
	predict `varT'_r,r;
	};


	foreach var in $y1 {;

	quietly xi: reg `var' phat_low30 ${householdcontrols} ${respondentcontrols} i.paire;
	predict `var'_r,r;

	quietly xi: reg `var'_r T_phat_low30_r T_phat_high30_r;
	estimates store E_`var'; 
	global Estimates $Estimates E_`var';
	global cons1 $cons1  ([E_`var'_mean]T_phat_low30_r - [E_`var'_mean]T_phat_high30_r = 0);
	global cons2 $cons2  ([E_`var'_mean]T_phat_low30_r  = 0);
	};


suest $Estimates, cluster(demi_paire); 
test  $cons1;
	replace pvalue = r(p) if _n==1;
	replace vars = "p_Tlow_Thigh" if _n==1;
test  $cons2;
	replace pvalue= r(p) if _n==2;
	replace vars = "p_Tlow_0" if _n==2;

outsheet vars pvalue   
	if _n<=10 using Tables_paper\Table8C_externalities_jointtest.out, replace;

drop vars pvalue;
*/;




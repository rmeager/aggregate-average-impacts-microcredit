********************************************************************************************************************************************
**** DO FILE CONSTRUCS IMPACT OUTCOMES
**** INPUT fILES: Morocco_EL_mini_anonym.dta
**** date: February 2014
********************************************************************************************************************************************

# delimit; 
clear;
cap clear matrix;
set mem 500m; 
set maxvar 10000; 
set more off;

use Input\Microcredit_EL_mini_anonym.dta, clear;

************************;
** Respondent controls;
************************;
foreach section in socio activ consump women {;
	gen cm_resp_`section'=0;
	gen ccm_resp_`section'=0;
	gen other_resp_`section'=0;
	gen male_resp_`section'=.;
	};

global id_rep="id_rep";
foreach i of numlist 1/24 {;
	replace cm_resp_socio=1 if $id_rep==`i' & a3_`i'==1;
 	replace ccm_resp_socio=1 if $id_rep==`i' & a3_`i'==2;
 	replace other_resp_socio=1 if $id_rep==`i' & inrange(a3_`i',3,24);
	replace male_resp_socio=1 if $id_rep==`i' & a4_`i'==1;  
	replace male_resp_socio=0 if $id_rep==`i' & a4_`i'==2;  
	};

global id_rep="id_rep_d";
foreach i of numlist 1/24 {;
	replace cm_resp_activ=1 if $id_rep==`i' & a3_`i'==1;
 	replace ccm_resp_activ=1 if $id_rep==`i' & a3_`i'==2;
 	replace other_resp_activ=1 if $id_rep==`i' & inrange(a3_`i',3,24);
	replace male_resp_activ=1 if $id_rep==`i' & a4_`i'==1;  
	replace male_resp_activ=0 if $id_rep==`i' & a4_`i'==2;  
	};

global id_rep="id_rep_h2";
foreach i of numlist 1/24 {;
	replace cm_resp_consump=1 if $id_rep==`i' & a3_`i'==1;
 	replace ccm_resp_consump=1 if $id_rep==`i' & a3_`i'==2;
 	replace other_resp_consump=1 if $id_rep==`i' & inrange(a3_`i',3,24);
	replace male_resp_consump=1 if $id_rep==`i' & a4_`i'==1;  
	replace male_resp_consump=0 if $id_rep==`i' & a4_`i'==2;  
	};

global id_rep="id_rep_i";
foreach i of numlist 1/24 {;
	replace cm_resp_women=1 if $id_rep==`i' & a3_`i'==1;
 	replace ccm_resp_women=1 if $id_rep==`i' & a3_`i'==2;
 	replace other_resp_women=1 if $id_rep==`i' & inrange(a3_`i',3,24);
	replace male_resp_women=1 if $id_rep==`i' & a4_`i'==1;  
	replace male_resp_women=0 if $id_rep==`i' & a4_`i'==2;  
	};

foreach section in socio activ consump women {;
	egen miss_`section'=rsum(cm_resp_`section' ccm_resp_`section' other_resp_`section');
foreach var in cm_resp ccm_resp other_resp {;
	replace `var'_`section'=. if miss_`section'==0;
	}; 
	};
	drop miss_*;

********************************************************************************************************
********************************************************************************************************
** 1.IMPACT OUTCOMES
********************************************************************************************************
********************************************************************************************************

********************************************************************************************************
** Access to credit 
********************************************************************************************************; 
** I call formal the following: 1. Cr�dit agricole, 2. Autre banque, 4. Zakoura, 5. Fondation Cr�dit Agricole
6. Autre AMC (Association de Micro-cr�dit), 15. Coop�rative
** Informal is: 7. Usurier, 8. Bijoutier, 9. Famille, 10. Voisin 11. Amis, 12. Magasin, 13. Un client, 14. Un fournisseur, 

** NUMBER of ACTIVE loans : Al Amana , other formal , informal , total; 

* I assume that if there is nothing entered in question i3, the household does not have such a loan, so we set this to zero; 
foreach j of numlist 1(1)3 { ; 
gen alamana`j' = (i3_`j' == 3); 
gen other_formal`j' = (i3_`j' == 1 |i3_`j' == 2 |i3_`j' == 15 ); 
gen informal`j' = (i3_`j' == 7 |i3_`j' == 8 |i3_`j' == 9 |i3_`j' == 10 |i3_`j' == 11 |i3_`j' == 12 |i3_`j' == 13 |i3_`j' == 14);
gen branching`j' = (i3_`j' == 16 | i3_`j' == 17); 
gen other_amc`j' = (i3_`j' == 4 | i3_`j' == 5 | i3_`j' == 6); 
}; 

egen aloans_alamana = rowtotal(alamana1 alamana2 alamana3) ;
egen aloans_oformal = rowtotal(other_formal1 other_formal2 other_formal3) ; 
egen aloans_informal = rowtotal(informal1 informal2 informal3);
egen aloans_branching = rowtotal(branching1 branching2 branching3);  
egen aloans_oamc = rowtotal(other_amc1 other_amc2 other_amc3);

egen aloans_total = rowtotal(aloans_alamana aloans_oamc aloans_oformal aloans_informal aloans_branching); 

drop alamana* other_formal* informal* branching* other_amc* ; 


** AMOUNT of ACTIVE loans : Al Amana , other formal , informal , total; 

foreach j of numlist 1(1)3 { ; 
gen alamana`j' = 0 ; 
gen other_formal`j' = 0 ; 
gen informal`j' = 0; 
gen branching`j' = 0; 
gen other_amc`j' = 0; 

replace alamana`j' = i9_`j' if (i3_`j' == 3) &  i9_`j' >= 0 ;
replace alamana`j' = . if (i3_`j' == 3) & i9_`j' < 0 ;
replace other_formal`j' = i9_`j' if (i3_`j' == 1 |i3_`j' == 2 |i3_`j' == 15 ) & (i9_`j' >= 0) ; 
replace other_formal`j' = . if (i3_`j' == 1 |i3_`j' == 2 |i3_`j' == 15 ) & (i9_`j' < 0) ; 
replace informal`j' = i9_`j' if (i3_`j' == 7 |i3_`j' == 8 |i3_`j' == 9 |i3_`j' == 10 |i3_`j' == 11 |i3_`j' == 12 |i3_`j' == 13 |i3_`j' == 14 ) 
& (i9_`j' >= 0);
replace informal`j' = . if (i3_`j' == 7 |i3_`j' == 8 |i3_`j' == 9 |i3_`j' == 10 |i3_`j' == 11 |i3_`j' == 12 |i3_`j' == 13 |i3_`j' == 14 ) 
& (i9_`j' < 0);
replace branching`j' = i9_`j' if (i3_`j' == 16 | i3_`j' == 17) & i9_`j' >= 0 ; 
replace branching`j' = . if (i3_`j' == 16 | i3_`j' == 17) & i9_`j' < 0 ; 
replace other_amc`j' = i9_`j' if (i3_`j' == 4 | i3_`j' == 5 | i3_`j' == 6) & i9_`j' >= 0 ; 
replace other_amc`j' = . if (i3_`j' == 4 | i3_`j' == 5 | i3_`j' == 6) & i9_`j' < 0 ; 

}; 

egen aloansamt_alamana = rowtotal(alamana1 alamana2 alamana3) ;
egen aloansamt_oformal = rowtotal(other_formal1 other_formal2 other_formal3) ; 
egen aloansamt_informal = rowtotal(informal1 informal2 informal3);
egen aloansamt_branching = rowtotal(branching1 branching2 branching3);  
egen aloansamt_oamc = rowtotal(other_amc1 other_amc2 other_amc3);
egen aloansamt_oformal2= rowtotal(aloansamt_oformal aloansamt_oamc);

egen aloansamt_total = rowtotal(aloansamt_alamana aloansamt_oamc aloansamt_oformal aloansamt_informal aloansamt_branching); 

drop alamana* other_formal* informal* branching* other_amc*; 
  

** NUMBER of PAST loans : Al Amana ,  other formal , informal ,  total;

foreach j of numlist 4(1)5 { ; 
gen alamana`j' = (i3_`j' == 3); 
gen other_formal`j' = (i3_`j' == 1 |i3_`j' == 2 |i3_`j' == 15); 
gen informal`j' = (i3_`j' == 7 |i3_`j' == 8 |i3_`j' == 9 |i3_`j' == 10 |i3_`j' == 11 |i3_`j' == 12 |i3_`j' == 13 |i3_`j' == 14 );
gen branching`j' = (i3_`j' == 16 |i3_`j' == 17); 
gen other_amc`j' = (i3_`j' == 4 | i3_`j' == 5 | i3_`j' == 6); 
}; 

egen ploans_alamana = rowtotal(alamana4 alamana5);
egen ploans_oformal = rowtotal(other_formal4 other_formal5) ; 
egen ploans_informal = rowtotal(informal4 informal5);
egen ploans_branching = rowtotal(branching4 branching5);  
egen ploans_oamc = rowtotal(other_amc4 other_amc5);  
egen ploans_total = rowtotal(ploans_alamana ploans_oamc ploans_oformal ploans_informal ploans_branching); 
 
drop alamana* other_formal* informal* branching* other_amc*; 


** AMOUNT of PAST  loans : Al Amana , other formal , informal , total; 

foreach j of numlist 4(1)5 { ; 
gen alamana`j' = 0 ; 
gen other_formal`j' = 0 ; 
gen informal`j' = 0; 
gen branching`j' = 0; 
gen other_amc`j' = 0; 

replace alamana`j' = i9_`j' if (i3_`j' == 3) &  i9_`j' >= 0 ;
replace alamana`j' = . if (i3_`j' == 3) & i9_`j' < 0 ;
replace other_formal`j' = i9_`j' if (i3_`j' == 1 |i3_`j' == 2 |i3_`j' == 15 ) & (i9_`j' >= 0) ; 
replace other_formal`j' = . if (i3_`j' == 1 |i3_`j' == 2 |i3_`j' == 15 ) & (i9_`j' < 0) ; 
replace informal`j' = i9_`j' if (i3_`j' == 7 |i3_`j' == 8 |i3_`j' == 9 |i3_`j' == 10 |i3_`j' == 11 |i3_`j' == 12 |i3_`j' == 13 |i3_`j' == 14 ) 
& (i9_`j' >= 0);
replace informal`j' = . if (i3_`j' == 7 |i3_`j' == 8 |i3_`j' == 9 |i3_`j' == 10 |i3_`j' == 11 |i3_`j' == 12 |i3_`j' == 13 |i3_`j' == 14 ) 
& (i9_`j' < 0);
replace branching`j' = i9_`j' if (i3_`j' == 16 | i3_`j' == 17) & i9_`j' >= 0 ; 
replace branching`j' = . if (i3_`j' == 16 | i3_`j' == 17) & i9_`j' < 0 ; 
replace other_amc`j' = i9_`j' if (i3_`j' == 4 | i3_`j' == 5 | i3_`j' == 6) & i9_`j' >= 0 ; 
replace other_amc`j' = . if (i3_`j' == 4 | i3_`j' == 5 | i3_`j' == 6) & i9_`j' < 0 ; 
}; 

egen ploansamt_alamana = rowtotal(alamana4 alamana5);
egen ploansamt_oformal = rowtotal(other_formal4 other_formal5) ; 
egen ploansamt_informal = rowtotal(informal4 informal5); 
egen ploansamt_branching = rowtotal(branching4 branching5); 
egen ploansamt_oamc = rowtotal(other_amc4 other_amc5);  

egen ploansamt_total = rowtotal(ploansamt_alamana ploansamt_oamc ploansamt_oformal ploansamt_informal ploansamt_branching); 

drop alamana* other_formal* informal* branching* other_amc* ;


*** Total NUMBER of loans over the period ***;

egen loans_alamana=rowtotal(aloans_alamana ploans_alamana);
egen loans_oformal=rowtotal(aloans_oformal ploans_oformal);
egen loans_informal=rowtotal(aloans_informal ploans_informal);
egen loans_branching=rowtotal(aloans_branching ploans_branching);
egen loans_oamc=rowtotal(aloans_oamc ploans_oamc);
egen loans_total=rowtotal(aloans_total ploans_total);

egen loans_oformal2=rowtotal(loans_oformal loans_oamc);

egen loansamt_alamana = rowtotal(aloansamt_alamana  ploansamt_alamana );
egen loansamt_oformal= rowtotal(aloansamt_oformal ploansamt_oformal);
egen loansamt_informal= rowtotal(aloansamt_informal ploansamt_informal);
egen loansamt_branching = rowtotal(aloansamt_branching ploansamt_branching); 
egen loansamt_oamc = rowtotal(aloansamt_oamc ploansamt_oamc );
egen loansamt_total = rowtotal(aloansamt_total ploansamt_total ); 


*** DUMMY equal to 1 if had a loan over the period ***;

foreach var in alamana oformal informal branching oamc total oformal2{;
	gen borrowed_`var'=0 if loans_`var'!=.;
		replace borrowed_`var'=1 if loans_`var'>=1 & loans_`var'!=.;
	}; 


********************************************************************************************************
** Use of Alamana Credit
********************************************************************************************************; 
** Pour quel type d�utilisation�le cr�dit a-t-il �t� pris? 1= consommation, 2= achat d�intrants� 3=�quipement�;

foreach j of numlist 1(1)5 {; 
gen loans_cons_`j' = .; 
gen loans_inputs_`j' = . ; 
gen loans_equip_`j' = .; 
replace loans_cons_`j' = 1 if i25_`j' == 1; 
replace loans_cons_`j' = 0 if i25_`j' != 1 & i25_`j' != . & i25_`j' >= 0; 
replace loans_inputs_`j' = 1 if i25_`j' == 2; 
replace loans_inputs_`j' = 0 if i25_`j' != 2 & i25_`j' != . & i25_`j' >= 0; 
replace loans_equip_`j' = 1 if i25_`j' == 3; 
replace loans_equip_`j' = 0 if i25_`j' != 3 & i25_`j' != . & i25_`j' >= 0; 
}; 

egen loans_cons = rowmean(loans_cons_1 loans_cons_2 loans_cons_3 loans_cons_4 loans_cons_5); 
egen loans_inputs = rowmean(loans_inputs_1 loans_inputs_2 loans_inputs_3 loans_inputs_4 loans_inputs_5); 
egen loans_equip = rowmean(loans_equip_1 loans_equip_2 loans_equip_3 loans_equip_4 loans_equip_5); 

drop loans_cons_1 loans_cons_2 loans_cons_3 loans_cons_4 loans_cons_5
loans_inputs_1 loans_inputs_2 loans_inputs_3 loans_inputs_4 loans_inputs_5
loans_equip_1 loans_equip_2 loans_equip_3 loans_equip_4 loans_equip_5; 

foreach j of numlist 1(1)5 {; 
gen alamana_cons_`j' = .; 
gen alamana_inputs_`j' = . ; 
gen alamana_equip_`j' = .; 
replace alamana_cons_`j' = 1 if i25_`j' == 1 & i3_`j' == 3; 
replace alamana_cons_`j' = 0 if i25_`j' != 1 & i25_`j' != . & i25_`j' >= 0 & i3_`j' == 3; 
replace alamana_inputs_`j' = 1 if i25_`j' == 2 & i3_`j' == 3;  
replace alamana_inputs_`j' = 0 if i25_`j' != 2 & i25_`j' != . & i25_`j' >= 0 & i3_`j' == 3; 
replace alamana_equip_`j' = 1 if i25_`j' == 3 & i3_`j' == 3; 
replace alamana_equip_`j' = 0 if i25_`j' != 3 & i25_`j' != . & i25_`j' >= 0 & i3_`j' == 3; 
}; 

egen alamana_cons = rowmean(alamana_cons_1 alamana_cons_2 alamana_cons_3 alamana_cons_4 alamana_cons_5); 
egen alamana_inputs = rowmean(alamana_inputs_1 alamana_inputs_2 alamana_inputs_3 alamana_inputs_4 alamana_inputs_5); 
egen alamana_equip = rowmean(alamana_equip_1 alamana_equip_2 alamana_equip_3 alamana_equip_4 alamana_equip_5); 

drop alamana_cons_1 alamana_cons_2 alamana_cons_3 alamana_cons_4 alamana_cons_5
alamana_inputs_1 alamana_inputs_2 alamana_inputs_3 alamana_inputs_4 alamana_inputs_5
alamana_equip_1 alamana_equip_2 alamana_equip_3 alamana_equip_4 alamana_equip_5; 

********************************************************************************************************
** Credit constraints indicators
********************************************************************************************************; 
** why not try to borrow in the last 12 month
1. Pas besoin de cr�dit 
2. Peur que ma demande ne soit rejet�e
3. Montant des frais trop �lev� 
4. Taux d�int�r�t trop �lev�s 
5. Les antennes des organismes financiers sont trop loins de mon lieu de travail 
6. Montants propos�s par les organismes financiers trop faibles
7. Peur de ne pas pouvoir rembourser le cr�dit
8. Peur de perdre ma garantie 
9. Raisons religieuses
10. Pas de garantie � donner; 

gen nb_pasbesoin = i2701 == 1; 
gen nb_peurrejet = i2702 == 1; 
gen nb_frais = i2703 == 1; 
gen nb_taux = i2704 == 1; 
gen nb_distance = i2705 == 1; 
gen nb_montantinsuff = i2706 == 1; 
gen nb_peurremboursement = i2707 == 1; 
gen nb_peurpertegarantie = i2708 == 1; 
gen nb_religion = i2709 == 1; 
gen nb_pasgarantie= i2710 == 1; 
 
** credit demand refused; 
gen credit_refused = (i28==1); 
replace credit_refused = . if i28 == . | i28  <0 ; 

** need of credit not asked for; 
gen credit_notasked = (i32 == 1); 
replace credit_notasked = . if i32 == . | i32 < 0  ;

#delimit;
** need of credit today;
gen credit_need=i34==1;
replace credit_need=. if i34==.;

*** tried to take a credit in the last 12 months ;
gen try_credit=(i26==1);

** reason not to try;
gen nocredit_const=(nb_peurrejet==1|nb_montantinsuff==1|nb_pasgarantie==1);

********************************************************************************************************
** Credit Need
********************************************************************************************************; 

foreach var in fin form comm {;
foreach j of numlist 1(1)6 {;  
gen need_`var'_`j'  = .; 
};  
}; 

foreach j of numlist 1(1)6 {; 
replace need_fin_`j' = 1 if d5_`j' == 1 ; 
replace need_form_`j' = 1 if d5_`j' == 2; 
replace need_comm_`j' = 1 if d5_`j' == 3; 
replace need_fin_`j' = 0 if d5_`j' != 1 & d5_`j' != . & d5_`j' >= 0   ;  
replace need_form_`j' = 0 if d5_`j' != 2 & d5_`j' != . & d5_`j' >= 0   ;  
replace need_comm_`j' = 0 if d5_`j' != 3 & d5_`j' != . & d5_`j' >= 0   ;  
}; 


egen need_fin = rowmean(need_fin_1 need_fin_2 need_fin_3 need_fin_4 need_fin_5 need_fin_6); 
egen need_form = rowmean(need_form_1 need_form_2 need_form_3 need_form_4 need_form_5 need_form_6); 
egen need_comm = rowmean(need_comm_1 need_comm_2 need_comm_3 need_comm_4 need_comm_5 need_comm_6); 

foreach var in fin form comm {;
foreach j of numlist 1(1)6 {;  
drop need_`var'_`j' ; 
};  
}; 


********************************************************************************************************
** INVESTMENTS & STOCK OF CAPITAL (in dhs)
********************************************************************************************************; 
***********************;
** Agriculture;

* Investment in agricultural assets; 
gen inv_agri = 0; 
	foreach j of numlist 1(1)16 { ; 
	replace e5_`j' = . if e5_`j' > 100 ;
	};
 
	foreach j of numlist 1(1)16 { ; 
	replace inv_agri = inv_agri + e7_`j' if e7_`j'!= . & e7_`j' >= 0 & e3_`j' == 1; 
	replace inv_agri = inv_agri + e7_`j'*e5_`j'/100 if e7_`j'!= . & e7_`j' >= 0 & e5_`j'!= . & e5_`j' >= 0 & e3_`j' == 2; 
	}; 

* Stock of agricultural assets; 
	foreach j of numlist 1(1)16 { ;
	replace e2_`j'=0 if e3_`j'!=1 & e3_`j'!=2; 
	sum e7_`j', d;
	gen ag_`j'=r(p50)*e2_`j';
	sum e7_`j', d;
	replace ag_`j'= r(p50)*e2_`j'*e5_`j'/100 if e5_`j'!= . & e5_`j' >= 0; 
	};

egen asset_agri=rsum(ag_3-ag_16);
	  drop ag_1- ag_16;

** Land ; 
gen land_ha = c6;
	replace land_ha = 0 if c6 == . ;

	gen land2_ha=e9;
	replace land2_ha=0 if e9==.;
	recode land2_ha (-98=.);

	gen land_other_ha=e11;
	replace land_other_ha=0 if e11==.;

gen land = (c5 ==1);
	replace land = . if c5==.;  

egen land_expl_ha = rsum(land2_ha land_other_ha);  
	drop land2_ha land_other_ha; 

*************;
** Livestock;
* Investment in livestock assets;
	* Livestock animal assets, last year; 

	gen inv_livestockanim = 0; 
	foreach j of numlist 1(1)8 { ; 
	replace inv_livestockanim  = inv_livestockanim  + f13_`j' if f13_`j' != . & f13_`j' >= 0 ; 
	}; 

	************************;
	* Livestock material assets (last year) ; 

	gen inv_livestockmat = 0 ; 
	foreach j of numlist 1(1)5 { ; 
	replace inv_livestockmat = inv_livestockmat + f4_`j' if f4_`j' != . & f4_`j' >= 0; 
	}; 

egen inv_livestock = rowtotal(inv_livestockmat inv_livestockanim); * per last year; 


* Value of stock of livestock assets; 
	foreach j of numlist 1(1)3 { ;
		gen assetlive`j'=0;
		gen unitprice`j' = f4_`j' / f2_`j' if f4_`j'>0 & f4_`j'!=. & f2_`j'>0 & f2_`j'!=.;
		sum unitprice`j' if unitprice`j'>0, detail;
		replace assetlive`j'=r(p50)*f2_`j' if f2_`j'>0 & f2_`j'!=.;
		};
		gen assetlive4 = f4_4 if f4_4>0 & f4_4!=.;
		gen assetlive5 = f4_5 if f4_5>0 & f4_5!=.;

	egen asset_livestock = rsum(assetlive1-assetlive5);
		drop assetlive1-assetlive5;	 

*************;
** Business;

* Investment in business assets ; 
	* rule: if someone owns it alone, we consider full purchase amount as the investment. If someone owns it jointly with others, we assign him a fraction of the
	expense corresponding to his fraction of the ownership; 

gen inv_business_assets = 0; ** final measure is by year; * Careful to take only those assets which were purchased in the last year; 
	foreach j of numlist 1(1)13 { ; 
	replace inv_business_assets = inv_business_assets + g7_`j' if g7_`j' >= 0 & g7_`j' != . & g4_`j' == 1 & g6_`j' == 1; 
	replace inv_business_assets = inv_business_assets + g7_`j'*g5_`j'/100 if g7_`j' >= 0 & g7_`j' != . & g5_`j' >= 0 & g5_`j' != .  & g4_`j' == 2 & g6_`j' == 1; 
	}; 

* Value of stock of business assets ;
 
replace g2_4=. if g2_4==25;
replace g5_4=25 if g5_4==0.25;

	foreach j of numlist 1(1)9 { ;
		gen assetbus`j'=0;
		sum g7_`j' if g7_`j' > 0, detail;
		replace assetbus`j'=r(p50)*g2_`j' if g2_`j'>0 & g2_`j'!=. & g4_`j' == 1 ;
		replace assetbus`j'=r(p50)*g2_`j'*g5_`j'/100 if g2_`j'>0 & g2_`j'!=. & g4_`j' == 2 ;
		};
		gen assetbus10 = g7_10 if g7_10>0 & g7_10!=. & g4_10 == 1;
			replace assetbus10 = g7_10*g5_10/100 if g2_10>0 & g2_10!=. & g5_10 >= 0 & g5_10 != . & g4_10 == 2 ;
		gen assetbus11 = g7_11 if g7_11>0 & g7_11!=. & g4_11 == 1;
			replace assetbus11 = g7_11*g5_11/100 if g2_11>0 & g2_11!=. & g5_11 >= 0 & g5_11 != . & g4_11 == 2 ;
		gen assetbus12 = g7_12 if g7_12>0 & g7_12!=. & g4_12 == 1;
			replace assetbus12 = g7_12*g5_12/100 if g2_12>0 & g2_12!=. & g5_12 >= 0 & g5_12 != . & g4_12 == 2 ;
		gen assetbus13 = g7_13 if g7_13>0 & g7_13!=. & g4_13 == 1;
			replace assetbus13 = g7_13*g5_13/100 if g2_13>0 & g2_13!=. & g5_13 >= 0 & g5_13 != . & g4_13 == 2 ;

egen asset_business = rsum(assetbus1-assetbus13);
	drop assetbus1-assetbus13;	 


** Investment in business inputs  - this is by month ; 
gen inv_business_inputs = 0 ; 
	foreach j of numlist 1(1)6 {; 
	foreach i of numlist 1(1)5 {;  
	replace inv_business_inputs = inv_business_inputs + g18_`j'_`i'*g19_`j'_`i'*12 if g19_`j'_`i' != . & g19_`j'_`i' >= 0 & g18_`j'_`i' != . & g18_`j'_`i' >= 0 ;  
	}; 
	}; 

	foreach j of numlist 1(1)6 {; 
	foreach i of numlist 1(1)5 {;  
	replace inv_business_inputs = inv_business_inputs + g24_`j'_`i'*12 if g24_`j'_`i' != . & g24_`j'_`i' >= 0  ;
	}; 
	}; 


********************************************************************************************************
** Self-employment activities
********************************************************************************************************; 

** Activity types (Fishing is excluded); 
gen act_agri = 0; 
gen act_livestock =  0; 
gen act_business = 0 ; 

foreach j of numlist 1(1)6 {; 
	gen d1_100code`j' = d1_code`j'/100; 
	replace act_agri = 1 if d1_nom`j' != "" & d1_nom`j' != "." & d2_`j' == 1; 
	replace act_agri = 1 if d2_`j' == 1; 
	replace act_agri = 1 if d2_`j' == . & d1_nom`j' != "" & d1_nom`j' != "." & (d1_100code`j' < 2 & d1_100code`j' > 0); 

	replace act_livestock = 1 if d1_nom`j' != "" &  d1_nom`j' != "." & d2_`j' == 2; 
	replace act_livestock = 1 if d2_`j' == 2; 
	replace act_livestock = 1 if d2_`j' == . & d1_nom`j' != "" & d1_nom`j' != "." & (d1_100code`j' < 3 & d1_100code`j' >= 2); 

	replace act_business = 1 if d1_nom`j' != "" & d1_nom`j' != "." & (d2_`j' == 3 |d2_`j' == 4 |d2_`j' == 5 | d2_`j' == 6);
	replace act_business = 1 if d2_`j' == . & d1_nom`j' != "" & d1_nom`j' != "." & (d1_100code`j' < 7 & d1_100code`j' >= 3);
	drop d1_100code`j' ;
	}; 

** Number of self-employment activities; 
* Self-employment activities, as declared in Table D;
gen act_number = 0 ; 
	foreach j of numlist 1(1)6 {; 
	replace act_number = act_number + 1 if d1_nom`j' != "" & d1_nom`j' != "." & d2_`j' != 7 & d2_`j' != 8 ;
	};


* Diversification, as declared in specific production modules; 
	gen act_number_agri1 = 0 ; 
		foreach j of numlist 1(1)20 {;
		replace act_number_agri1 = act_number_agri1 + 1 if e15_`j' ==1 ;
		}; 

	gen act_number_agri2 = 0 ;
		foreach i of numlist 1(1)13 {; 
		replace act_number_agri2 = act_number_agri2 + 1 if e25_`i'>0 & e25_`i'!=. ;
		};

	gen act_number_agri3 = 0 ; 
		foreach j of numlist 1(1)8 {;
		replace act_number_agri3 = act_number_agri3 + 1 if e36_`j' ==1 ;
		};

egen act_total_agri=rsum(act_number_agri1  act_number_agri2 act_number_agri3); 
	drop act_number_agri1- act_number_agri3;

	gen act_number_live1 = 0 ; 
		foreach j of numlist 1(1)8 {;
		replace act_number_live1 = act_number_live1 + 1 if f8_0_`j' ==1 ;
		}; 

	gen act_number_live2 = 0 ; 
		foreach j of numlist 1(1)3 5 6 4 {;
		replace act_number_live2 = act_number_live2 + 1 if f15_0_`j' ==1 ;
		}; 

egen act_total_live=rsum(act_number_live1  act_number_live2 ); 
	drop act_number_live1- act_number_live2;


gen act_number_business = 0 ; 
	foreach j of numlist 1(1)6 {; 
	replace act_number_business = act_number_business + 1 if d1_nom`j' != "" & d1_nom`j' != "." & (d2_`j' == 3 |d2_`j' == 4 |d2_`j' == 5 | d2_`j' == 6) ;
	}; 	

** Dummies for self-employment activities;
	gen selfempl_agri = (act_total_agri!=0);
		replace selfempl_agri = . if act_total_agri==.;

	gen selfempl_livestock = (act_total_live!=0);
		replace selfempl_livestock=. if act_total_live==.;

	gen selfempl_business = (act_number_business!=0);
		replace selfempl_business = . if act_number_business==.;

	gen self_empl=(selfempl_agri==1 | selfempl_livestock==1 | selfempl_business==1);
 

*****************************************************************
** AGRICULTURAL sales: Monetary sales + Self-consumption;  
*****************************************************************; 
* We count as sales: total value sold before and after harvest, valued at their own price (we later add stock held); 
* Plus own consumption, valued at median sample price (median village price = actually generates too many missing values); 

sort id3; 
foreach j of numlist 1(1)20 {; 
egen price`j' = median(e23_`j') if e23_`j' >= 0 ;
egen price2`j' = median(e21_`j') if e21_`j' >= 0 ; 
replace price`j' = price2`j' if price`j' == . ; 
}; 

foreach j of numlist 1(1)20 {; 
count if e20_`j' > 0 & e20_`j' != . & e21_`j'== .  ;
count if e22_`j' > 0  &  e22_`j' != . & e23_`j'== .;
}; 

gen sale_cereal = 0; 
foreach j of numlist 1(1)20 {; 
replace sale_cereal = sale_cereal + e20_`j'*e21_`j' if e20_`j' >= 0 & e20_`j' != . & e21_`j' != . & e21_`j' >=0  ;
replace sale_cereal = sale_cereal + e20_`j'*price`j' if e20_`j' >= 0 & e20_`j' != . & (e21_`j' == . | e21_`j' <=0)  ;
replace sale_cereal = sale_cereal + e22_`j'*e23_`j' if e22_`j' >= 0  &  e22_`j' != . & e23_`j' != . & e23_`j' >=0 ;
replace sale_cereal = sale_cereal + e22_`j'*price`j' if e22_`j' >= 0  &  e22_`j' != . & (e23_`j' == . | e23_`j' <=0) ;
replace sale_cereal = sale_cereal + e19_`j'*price`j' if e19_`j' >= 0  &  e19_`j' != . & price`j' != . & price`j' >=0 ;
}; 

foreach j of numlist 1(1)20 {; 
gen sale_cereal_`j' =0;
replace sale_cereal_`j' = sale_cereal_`j' + e20_`j'*e21_`j' if e20_`j' >= 0 & e20_`j' != . & e21_`j' != . & e21_`j' >=0  ;
replace sale_cereal_`j' = sale_cereal_`j' + e20_`j'*price`j' if e20_`j' >= 0 & e20_`j' != . & (e21_`j' == . | e21_`j' <=0) ;  
replace sale_cereal_`j'  = sale_cereal_`j'  + e22_`j'*e23_`j' if e22_`j' >= 0  &  e22_`j' != . & e23_`j' != . & e23_`j' >=0 ;
replace sale_cereal_`j'  = sale_cereal_`j'  + e22_`j'*price`j' if e22_`j' >= 0  &  e22_`j' != . & (e23_`j' == . | e23_`j' <=0) ;
replace sale_cereal_`j'  = sale_cereal_`j'  + e19_`j'*price`j' if e19_`j' >= 0  &  e19_`j' != . & price`j' != . & price`j' >=0 ;
drop price`j' price2`j';
};

gen sale_tree = 0 ; 
foreach j of numlist 1(1)13 {; 
replace e30_`j' = . if e30_`j' == 0 & e29_`j' == 0 ; 
replace e32_`j' = . if e32_`j' == 0 & e31_`j' == 0 ; 
replace e34_`j' = . if e34_`j' == 0 & e33_`j' == 0 ; 
}; 

foreach j of numlist 1(1)13 {; 
egen price`j' = median(e30_`j') if e30_`j' >= 0 ;
egen price2`j' = median(e32_`j') if e32_`j' >= 0 ;
replace price`j' = price2`j' if price`j' == . ; 
}; 

foreach j of numlist 1(1)13 {; 
replace sale_tree = sale_tree + e29_`j'*e30_`j'  if e29_`j' >= 0  & e29_`j' != . & e30_`j' != . & e30_`j' >=0  ;
replace sale_tree = sale_tree + e29_`j'*price`j'  if e29_`j' >= 0  & e29_`j' != . & (e30_`j' == . | e30_`j' <= 0 ) ;
replace sale_tree = sale_tree + e31_`j'*e32_`j' if e31_`j' >= 0  & e31_`j' != . & e32_`j' != . & e32_`j' >=0  ;
replace sale_tree = sale_tree + e31_`j'*price`j' if e31_`j' >= 0  & e31_`j' != . & (e32_`j' == . | e32_`j' <= 0)  ;
replace sale_tree = sale_tree + e33_`j'*e34_`j' if e33_`j' >= 0  & e33_`j' != . & e34_`j' != . & e34_`j' >=0 ;  
replace sale_tree = sale_tree + e33_`j'*price`j' if e33_`j' >= 0  & e33_`j' != . & (e34_`j' == . | e34_`j' <= 0 ) ;  
replace sale_tree = sale_tree + e28_`j'*price`j' if e28_`j' >= 0  & e28_`j' != . & price`j' >= 0 & price`j' !=.  ;  
}; 

foreach j of numlist 1(1)13 {; 
gen sale_tree_`j'=0;   
replace sale_tree_`j' = sale_tree_`j' + e29_`j'*e30_`j'  if e29_`j' >= 0  & e29_`j' != . & e30_`j' != . & e30_`j' >=0  ;
replace sale_tree_`j' = sale_tree_`j' + e29_`j'*price`j'  if e29_`j' >= 0  & e29_`j' != . & (e30_`j' == . | e30_`j' <= 0 ) ;
replace sale_tree_`j' = sale_tree_`j' + e31_`j'*e32_`j' if e31_`j' >= 0  & e31_`j' != . & e32_`j' != . & e32_`j' >=0  ;
replace sale_tree_`j' = sale_tree_`j' + e31_`j'*price`j' if e31_`j' >= 0  & e31_`j' != . & (e32_`j' == . | e32_`j' <= 0)  ;
replace sale_tree_`j' = sale_tree_`j' + e33_`j'*e34_`j' if e33_`j' >= 0  & e33_`j' != . & e34_`j' != . & e34_`j' >=0 ;  
replace sale_tree_`j' = sale_tree_`j' + e33_`j'*price`j' if e33_`j' >= 0  & e33_`j' != . & (e34_`j' == . | e34_`j' <= 0) ;  
replace sale_tree_`j' = sale_tree_`j' + e28_`j'*price`j' if e28_`j' >= 0  & e28_`j' != . & price`j' >= 0 & price`j' !=.  ;  
drop price`j' price2`j'; 
}; 


gen sale_veg = 0 ; 
foreach j of numlist 1(1)8 {; 
replace e40_`j' = . if e40_`j' == 0 & e39_`j' == 0 ; 
}; 

foreach j of numlist 1(1)8 {; 
sum e40_`j' if e40_`j' > 0, detail ;  
gen price`j' = r(p50) ;
}; 

foreach j of numlist 1(1)8 {; 
replace sale_veg = sale_veg + e39_`j'*e40_`j' if e39_`j' != . & e39_`j' >= 0 & e40_`j' != . & e40_`j' >=0 ; 
replace sale_veg = sale_veg + e39_`j'*price`j' if e39_`j' != . & e39_`j' >= 0 & (e40_`j' == . | e40_`j' <= 0) ; 
replace sale_veg = sale_veg + e38_`j'*price`j' if e38_`j' != . & e38_`j' >= 0 & price`j' >= 0 & price`j' !=.  ; 
drop price`j'; 
}; 

** TOTAL AGRICULTURAL SALES;
gen sale_agri = sale_cereal + sale_tree + sale_veg ; 

	drop sale_cereal_1- sale_cereal_20  sale_tree_1- sale_tree_13; 

******************************************************;
** LIVESTOCK Sales: Monetary sales + Self-consumption; 
******************************************************; 

** LIVESTOCK animals;

foreach j of numlist 1(1)8 {; 
gen price`j'_temp = f9_`j'/f8_`j' if f8_`j' != . & f8_`j' >= 0 & f9_`j' != . & f9_`j' >= 0; 
}; 

foreach j of numlist 1(1)8 {; 
egen price`j' = median(price`j'_temp); 
}; 

gen sale_livestockanim = 0; 
	foreach j of numlist 1(1)8 {; 
		replace sale_livestockanim = sale_livestockanim + f9_`j' if f9_`j' != .  & f9_`j' >= 0 ; 
		replace sale_livestockanim = sale_livestockanim + f10_`j'*price`j' if f10_`j' >= 0  & f10_`j' != . & price`j'  != . & price`j' >= 0 ; 
		drop price`j' price`j'_temp; 
		}; 


** LIVESTOCK products: Monetary sales + Self-consumption;
	* Sale of milk, butter, honey, etc...; 
	* again, we count as sale the amount sold plus the amount auto consumed; 

	foreach j of numlist 1(1)6 {; 
	gen unitprice`j' = f18_`j'/f17_`j' if f18_`j' != . & f18_`j' >= 0 & f17_`j' != . & f17_`j' >= 0; 
	egen price`j' = median(unitprice`j') if unitprice`j' >= 0 ;
	drop unitprice`j' ; 
	}; 

gen sale_livestockprod = 0 ; 
	foreach j of numlist 1(1)6 {;
	replace sale_livestockprod = sale_livestockprod + f18_`j' if f18_`j' >= 0  & f18_`j' != . ; 
	replace sale_livestockprod = sale_livestockprod + f16_`j'*price`j' if f16_`j' >= 0  & f16_`j' != . ; 
	drop price`j';
	}; 

** TOTAL SALES from LIVESTOCK ACTIVITIES; 

gen sale_livestock = sale_livestockanim + sale_livestockprod; 

************************************************************************************************************
******** Business Sales; 
************************************************************************************************************; 
** Business sales (last week and last months), sale_business is yearly ; 

gen sale_business = 0; 
foreach j of numlist 1(1)6 {; 
foreach i of numlist 1(1)5 {; 
replace sale_business = sale_business + g28_`j'_`i' * g29_`j'_`i'*12 if g28_`j'_`i' != . & g29_`j'_`i' != . & g28_`j'_`i' >=0 & g29_`j'_`i' >=0 ; 
}; 
}; 

foreach j of numlist 1(1)6 {; 
foreach i of numlist 1(1)4 {; 
replace sale_business = sale_business + g35_`j'_`i'*12 if g35_`j'_`i' != . & g35_`j'_`i' >=0; 
}; 
}; 


********************************************************************************************************
** Expenses
********************************************************************************************************; 
** Agricultural Expenses; 

* Inputs expenses only; 
gen expense_agriinputs = 0 ; 

foreach j of numlist 1(1)12 {; 
replace expense_agriinputs  = expense_agriinputs + e49_`j' if e49_`j' != . & e49_`j'>=0 ; 
}; 

* Labor wages and expenses; 
gen expense_agrilabor =0 ; 
replace expense_agrilabor = expense_agrilabor + e44*e45*12 if e45 != . & e45 >= 0 & e44 != . & e45 >= 0 ; 
replace expense_agrilabor = expense_agrilabor + e46*(e47_r*e48_r) if e46 != . & e46 >= 0 &  e47_r != . & e47_r >= 0 & e48_r != . & e48_r >= 0  ;
replace expense_agrilabor = expense_agrilabor + e46*(e47_hr*e48_hr) if e46 != . & e46 >= 0 &  e47_hr != . & e47_hr >= 0 & e48_hr != . & e48_hr >= 0  ;


* Investment Expenses; 
gen expense_agriinv = inv_agri ;
	replace expense_agriinv = expense_agriinv /10 if expense_agriinv > 10000;
	* these are all purchases of tracteurs, we count one year out of ten as expense;
  
* Rental Expenses; 
gen expense_agrirent = 0; 
foreach j of numlist 1(1)16 {; 
replace expense_agrirent = expense_agrirent + e4_`j' if e4_`j' != . & e4_`j' >= 0 ; 
}; 


** TOTAL agricultural expenses; 
gen expense_agri = expense_agriinputs + expense_agrilabor + expense_agriinv + expense_agrirent; 
	

** Livestock Expenses; 
* Material rental expenses; 
gen expense_livestockmatrent = 0; 
foreach j of numlist 1(1)5 { ; 
replace expense_livestockmatrent = expense_livestockmatrent + f7_`j' if f7_`j' != . & f7_`j' >= 0; 
}; 

* Material investment expenses; 
gen expense_livestockmatinv = 0; 
foreach j of numlist 1(1)5 { ; 
replace expense_livestockmatinv = expense_livestockmatinv + f4_`j' if f4_`j' != . & f4_`j' >= 0 & f3_`j' == 1; 
}; 

* Animal investment expenses; 
gen expense_livestockaniminv = inv_livestockanim; 

* Labor expenses for livestock activity; 
gen expense_livestocklabor = 0; 
replace expense_livestocklabor = expense_livestocklabor + f22 *f23 *12 if f22 != . & f22 >= 0 & f23 != . & f23 >= 0 ; 
replace expense_livestocklabor = expense_livestocklabor + f24 *f25 *f26 if f24 != . & f24 >= 0 & f25 != . & f25 >= 0 & f26 != . & f26 >= 0 ; 

* Inputs expenses; 
gen expense_livestockinputs = 0; 
foreach j of numlist 1(1)3 {; 
replace expense_livestockinputs = expense_livestockinputs + f27_`j' *12 if f27_`j' != . & f27_`j' >= 0 ; 
replace expense_livestockinputs = expense_livestockinputs + f28_`j'  if f28_`j' != . & f28_`j' >= 0 ; 
}; 

foreach j of numlist 4(1)6 {; 
replace expense_livestockinputs = expense_livestockinputs + f27_`j'm *12 if f27_`j'm != . & f27_`j'm >= 0 ; 
replace expense_livestockinputs = expense_livestockinputs + f28_`j'm  if f28_`j'm != . & f28_`j'm >= 0 ; 
}; 

gen expense_livestock = expense_livestockmatrent + expense_livestockmatinv + expense_livestockaniminv + expense_livestocklabor + expense_livestockinputs; 


** Business Expenses; 

* Material Rental Expenses; 
gen expense_busrent = 0; 
foreach j of numlist 1(1)13 {; 
replace expense_busrent = expense_busrent + g9_`j' if g9_`j' != . & g9_`j' >= 0 ; 
}; 

gen expense_businv = inv_business_assets; 

gen expense_businputs = inv_business_inputs; 

gen expense_buslabor = 0; 
foreach j of numlist 1(1)6 {; 
replace expense_buslabor = expense_buslabor + g47_`j'*g48_`j'*12 if g47_`j'!= . & g48_`j'!= . & g47_`j'>= 0 & g48_`j'>= 0;  
replace expense_buslabor = expense_buslabor + g49_`j'*g50_`j'*g51_`j' if g49_`j'!= . & g50_`j'!= . & g51_`j'!= . & g49_`j'>= 0 & g50_`j'>= 0 & g51_`j'>= 0;  
}; 

gen expense_business_week = 0; 
foreach j of numlist 1(1)6 {; 
replace expense_business_week = expense_business_week + g14_`j'  if g14_`j' != . & g14_`j' >= 0 ; 
}; 

gen expense_business_month = 0; 
foreach j of numlist 1(1)6 {; 
replace expense_business_month = expense_business_month + g15_`j'  if g15_`j' != . & g15_`j' >= 0 ; 
}; 

gen expense_business = expense_busrent + expense_businputs + expense_businv + expense_buslabor; 


********************************************************************************************************
** Savings
********************************************************************************************************; 
** Savings agriculture;
**********************;
* In kind Savings, using the median prices (if we use village median prices, there are too many missing ones); 

foreach j of numlist 1(1)20 {; 
replace e21_`j' = . if e21_`j' < 0 ; 
egen price_`j' = median(e21_`j'); 
}; 

foreach j of numlist 1(1)20 {; 
replace e21_`j' = . if e21_`j' < 0; 
}; 
 
gen savings_cereal = 0; 
foreach j of numlist 1(1)20 {; 
replace savings_cereal = savings_cereal + e24_`j'*price_`j'  if e24_`j' != . & e24_`j' >= 0 & price_`j' >= 0 & price_`j' != . ; 
drop price_`j'; 
}; 

* e30 e32 and e34; 
foreach j of numlist 1(1)13 {; 
replace e30_`j' = . if e30_`j' < 0; 
replace e32_`j' = . if e32_`j' < 0; 
replace e34_`j' = . if e34_`j' < 0; 
egen price_sale_`j' = median(e30_`j') ; 
egen price_sale2_`j' = median(e32_`j');
egen price_sale3_`j' = median(e34_`j'); 
gen price_`j' = price_sale3_`j'; 
replace price_`j' = price_sale2_`j' if price_sale3_`j' == . & price_sale2_`j' != . ;
replace price_`j' = price_sale_`j' if price_sale3_`j' == . & price_sale2_`j' == . & price_sale_`j' != . ; 
}; 


gen savings_tree = 0; 
foreach j of numlist 1(1)13 {; 
replace savings_tree = savings_tree + e35_`j'*price_`j'  if e35_`j' != . & e35_`j' >= 0 & price_`j' >= 0 & price_`j' != . ; 
drop price_sale_`j' price_sale2_`j' price_sale3_`j' price_`j'; 
}; 

foreach j of numlist 1(1)8 {;
replace e40_`j' = . if e40_`j' < 0 ; 
egen price_sale_`j' = median(e40_`j'); 
gen price_`j' = price_sale_`j'; 
}; 

gen savings_veg = 0; 
foreach j of numlist 1(1)8 {; 
replace savings_veg = savings_veg + e40b`j'*price_`j'  if e40b`j' != . & e40b`j' >= 0 & price_`j' >= 0 & price_`j' != . ; 
drop price_sale_`j' price_`j'; 
}; 


* Total SAVINGS agriculture;
	gen savings_agri = savings_cereal + savings_tree + savings_veg ; 
		

***********************;
** Savings Livestock;
**********************;
* calculating in-kind savings in terms of cattle; 
* f13 = total purchase amount (so need to divide by animals sold to get the price);
* f12 = animals bought ; 
* f9 = total sale amount; 
* f8 = animals sold; 
* f14 = number of animals left; 

foreach j of numlist 1(1)8 {;
replace f13_`j' = . if f13_`j' < 0 ; 
replace f12_`j' = . if f12_`j' < 0 ;  
replace f9_`j' = . if f9_`j' <  0 ; 
replace f8_`j' = . if f8_`j' < 0 ; 
egen price1_`j' = median(f13_`j'/f12_`j') ;
egen price2_`j' = median(f9_`j'/f8_`j') ;  
}; 

gen savings_livestock = 0; 
foreach j of numlist 1(1)8 {; 
replace savings_livestock = savings_livestock + f14_`j'*price1_`j'  if f14_`j' != . & f14_`j' >= 0 & price1_`j' != .  ;
replace savings_livestock = savings_livestock + f14_`j'*price2_`j'  if f14_`j' != . & f14_`j' >= 0 & price1_`j' == . & price2_`j' != .   ;
}; 
	* we keep prices to value losses;

* Livestock production inventory; 

foreach j of numlist 1(1)6 {; 
replace f17_`j' = . if f17_`j' < 0 ; 
replace f18_`j' = . if f18_`j' < 0 ;  
egen price_`j' = median(f18_`j'/f17_`j'); 
}; 

gen savings_livestock_prod = 0; 
foreach j of numlist 1(1)6 {; 
replace savings_livestock_prod = savings_livestock_prod + f18b`j'*price_`j'  if f18b`j' != . & f18b`j' >= 0 & price_`j' >= 0  & price_`j' !=. ; 
drop price_`j'; 
}; 

***********************;
** Savings in terms of business inventory;
**********************;
gen savings_business = 0 ; 
foreach j of numlist 1(1)6 {; 
replace g39_`j' = . if g39_`j' < 0; 
replace savings_business = savings_business + g39_`j' if g39_`j' != . & g39_`j' >= 0 ; 
}; 

*********************;
** Declared monetary savings;

gen savings_mon=i52;
	replace savings_mon=0 if (savings_mon==.|savings_mon==99);
	replace savings_mon=. if savings_mon==-99;

****************************;
** Various outputs;
*****************************;
* Total OUTPUT agriculture;
	gen prod_agri= sale_agri + savings_agri;

* Total STOCK agriculture;
	gen stock_agri=savings_agri + asset_agri;
	
** Total ASSETS livestock;
	gen astock_livestock = savings_livestock + asset_livestock ;

** Another measure of BUSINESS ASSETS;
	gen astock_business = savings_business + asset_business ;

** TOTAL OUTPUT;
	gen output_total= prod_agri + sale_livestock + sale_business ;

** TOTAL ASSETS;
	gen assets_total = asset_agri + astock_livestock + asset_business ; 

** TOTAL INVESTMENT ;
	egen inv_total=rsum(inv_business_assets inv_livestock inv_agri);

** TOTAL EXPENSES;
	gen expense_total= expense_agri + expense_business  + expense_livestock;


********************************************************************************************************
** Profits
********************************************************************************************************; 
gen profit_agri = prod_agri-expense_agri;
gen profit_livestock = sale_livestock - expense_livestock; 
gen profit_business = sale_business - expense_business ; 

gen profit_total=profit_agri + profit_livestock + profit_business;


********************************************************************************************************
** Income
********************************************************************************************************; 
* Self-employment income;
gen income_agri = profit_agri ; 
gen income_livestock = profit_livestock; 
gen income_business = profit_business; 

** Wage & salary (Section H2); 
gen income_dep = 0 ; 
foreach j of numlist 1(1)22 {;
replace income_dep = income_dep + h11_`j' if h11_`j' != . & h11_`j' >= 0 ;
replace income_dep = income_dep + h11c`j' if h11c`j' != . & h11c`j' >= 0 ;
}; 

gen daily_dum=0;
replace daily_dum=1 if income_dep!=0 & income_dep!=.;

** Remittances;
gen income_remittance = 0 ; 
foreach j of numlist 1(1)3 {; 
replace income_remittance = income_remittance + h17_4_`j' if h17_4_`j' != . & h17_4_`j' >= 0 ;
}; 

** Other income;
gen income_other = 0 ; 
	foreach j of numlist 1(1)4 {; 
	replace income_other = income_other + h15_`j' if h15_`j' != . & h15_`j' >= 0 ;
	};
	foreach i of numlist 1(1)22 {;
	replace income_other = income_other + h13_`i' if h13_`i' != . & h13_`i' >= 0 ;
	}; 

	* Of which;
	gen income_pension=0;
		foreach i of numlist 1(1)22 {;
		replace income_pension = income_pension + h13_`i' if h13_`i' != . & h13_`i' >= 0 ;
		}; 

	gen income_assetsales = 0 ;
		replace income_assetsales = income_assetsales + h15_1 if h15_1 != . & h15_1 >= 0 ;

	gen income_gvt = 0; 
		replace income_gvt = income_gvt + h15_2 if h15_2 != . & h15_2 >= 0 ;  

	gen income_othoth = 0;
		foreach j of numlist 3(1)4 {; 
		replace income_othoth = income_othoth + h15_`j' if h15_`j' != . & h15_`j' >= 0 ;
		};

* YEARLY AGGREGATE HOUSEHOLD INCOME;

gen income = income_agri + income_livestock + income_business + income_dep + income_remittance + income_other ;

	** Main income source declared by the HH;

	gen income_source_ag=h16==1;
	gen income_source_live=h16==2;
	gen income_source_bus=(h16==3|h16==4|h16==5|h16==6);
	gen income_source_dep=(h16==7|h16==8);
	gen income_source_other=(h16==9|h16==10);


** Other income variables;
egen income_work = rsum(income_agri income_livestock income_business income_dep);
	replace income_work =. if income==.; 

egen income_other_new = rsum(income_remittance income_pension income_gvt income_othoth);

*******************************************;
** Livestock Savings/Stock: 12 MONTHS AGO;
* We first need to get the value of lost livestock;
	* we value it the same way we valued current stock (using the purchase price and, if not available, the sale price); 

gen lost_livestockanim = 0; 
foreach j of numlist 1(1)8 {; 
replace lost_livestockanim  = lost_livestockanim  + f11_`j'*price1_`j'  if f11_`j' != . & f11_`j' >= 0 & price1_`j' != . ;
replace lost_livestockanim  = lost_livestockanim  + f11_`j'*price2_`j'  if f11_`j' != . & f11_`j' >= 0 & price1_`j' == . & price2_`j' != . ;
drop price1_`j' price2_`j'; 
}; 

gen hadlost_livestockanim = 0 if lost_livestockanim!=.;
	replace hadlost_livestockanim = 1 if lost_livestockanim>0 & lost_livestockanim!=.;   

****************;
** SHOCKS;
***************;
gen shock1=0;
replace shock1=1 if (f29_1==1|f29_2==1|f29_3==1|e50_1==1|e50_2==1);

gen shock1agri=0;
replace shock1agri=1 if (e50_1==1|e50_2==1);

gen shock1live=0;
replace shock1live=1 if (f29_1==1|f29_2==1|f29_3==1);

gen shock2=0;
replace shock2=1 if (h6_5>0 & h6_5!=.)|(h6_6>0 & h6_6!=.)|(h6_7>0 & h6_7!=.);


********************************************************************************************************
** Consumption
********************************************************************************************************; 

** Monthly FOOD consumption; 

gen cons_food = 0 ; 
	foreach j of numlist 1(1)14 17 18 {; 
	replace cons_food = cons_food + h1_`j' if h1_`j' != . & h1_`j' >= 0  ; 
	replace cons_food = cons_food + h2_`j'  if h2_`j' != . & h2_`j'  >= 0 ;
	}; 
	replace cons_food = cons_food*4.345 ; 

gen cons_food_usual = h8*4.345; 
	replace cons_food_usual = . if h8 == . | h8 < 0; 

** Monthly DURABLES consumption (I use SECTION C which has it per 12 months); 

gen cons_durables = 0; 
	foreach j of numlist 1(1)31 {; 
	replace cons_durables = cons_durables + c4_`j' if c4_`j' >= 0  & c4_`j' != . ; 
	}; 
	replace cons_durables = cons_durables/12 ; 


** Monthly NON-DURABLES consumption; 
gen cons_nondurables = 0 ; 
	replace cons_nondurables = cons_nondurables + cons_food if cons_food!=. & cons_food>=0; 
	foreach j of numlist 15 16 19(1)21 {; 
	replace cons_nondurables = cons_nondurables + h1_`j'*4.345 if h1_`j' != . & h1_`j' >= 0 ; 
	}; 
	foreach j of numlist 15 19(1)21 {; 
	replace cons_nondurables = cons_nondurables + h2_`j'*4.345 if h2_`j' != . & h2_`j' >= 0 ; 
	}; 
	foreach j of numlist 1(1)9 11(1)17 { ; 
	replace cons_nondurables = cons_nondurables + h3_`j' if h3_`j' != . & h3_`j' >= 0 ; 
	} ; 
	foreach j of numlist 1(1)10 { ; 
	replace cons_nondurables = cons_nondurables + h4_`j'/12 if h4_`j' != . & h4_`j' >= 0  ; 
	} ; 
	foreach j of numlist 1(1)9 { ; 
	replace cons_nondurables = cons_nondurables + h6_`j'/12 if h6_`j' != . & h6_`j' >= 0  ; 
	} ; 

** OF WHICH, some specific items of the non-durables (all these items are already included in the non-durables);
	* Schooling consumption monthly;
		gen cons_schooling = 0 ; 
		replace cons_schooling = cons_schooling + h4_1/12 if h4_1 != . & h4_1 >= 0 ; 

	* Health consumption (monthly);
		gen cons_health = 0; 
		replace cons_health = cons_health + h3_8  if  h3_8 != . & h3_8 >= 0 ;
 
	* Temptation & entertainment; 
		gen cons_temp = 0 ; 
		foreach j of numlist 7(1)9 16(1)18 {; 
		replace cons_temp = cons_temp + h1_`j'*4.345 if h1_`j' != . & h1_`j' >= 0 ; 
		}; 
		replace cons_temp = cons_temp + h3_14 if h3_14 >= 0 & h3_14 != . ; 
		replace cons_temp = cons_temp + h3_12 if h3_12 >= 0 & h3_12 != . ;
		replace cons_temp = cons_temp + h3_13 if h3_13 >= 0 & h3_13 != . ;

	* Festivals and celebrations;
		gen cons_ramadan = 0; 
		replace cons_ramadan = cons_ramadan + h4_5/12 if h4_5 >= 0 & h4_5 != . ; 
 
		gen cons_aid = 0; 
		replace cons_aid = cons_aid + h4_6/12 if h4_6 >= 0 & h4_6 != . ; 

		gen cons_social= 0; 
		replace cons_social= cons_social + cons_ramadan + cons_aid ; 
		replace cons_social= cons_social + h3_11 if h3_11!=. & h3_11>=0;  

		foreach j of numlist 1 2 3 4 6 {; 
		replace cons_social = cons_social + h6_`j'/12 if h6_`j'!=. & h6_`j'>=0 ; 
		}; 


** Credit repayment (this is not included in total consumption);
	gen cons_repay= 0; 
	replace cons_repay= cons_repay + h3_10 if h3_10!=. & h3_10>=0;  


*** TOTAL CONSUMPTION (monthly); 
egen consumption = rowtotal(cons_durables cons_nondurables); 


** Now, for each household with zero food consumption or total consumption, we will put all items to missing; 
replace cons_food = . if cons_food == 0 ;  

foreach var in durables nondurables schooling health temp ramadan aid social {; 
	replace cons_`var' = . if cons_food == . ; 
	}; 
	replace consumption=. if cons_food == . ;   

*** PER CAPITA CONSUMPTION (monthly);

** Total number of household members;
	gen members = 0; 
		foreach j of numlist 1(1)15 {; 
		replace members = members + 1 if a2_`j' != ""; 
		}; 
	gen members_resid = 0; 
		foreach j of numlist 1(1)15 {; 
		replace members_resid = members_resid + 1 if ((a5_`j' == 3 | a5_`j'==5))| ((a5_`j' != 3 & a5_`j'!=5) & a3_`j' == 1) ; 
		}; 
	gen nadults_resid = 0; 
		foreach j of numlist 1(1)17 {; 
		replace nadults_resid = nadults_resid + 1 if (((a5_`j' == 3 | a5_`j'==5))| ((a5_`j' != 3 & a5_`j'!=5) & a3_`j' == 1)) & a7_`j' >= 16 & a7_`j' != . ; 
		};
	gen nchildren_resid = 0;
		foreach j of numlist 1(1)17 {;
		replace nchildren_resid = nchildren_resid + 1 if (((a5_`j' == 3 | a5_`j'==5))| ((a5_`j' != 3 & a5_`j'!=5) & a3_`j' == 1)) & a7_`j' < 16 & a7_`j' >= 0; 
		};

** Per capita consumption/poverty;
	* We compute per capita consumption and we adjust wave 1 and wave 2 aggregates to 2009 prices;

	foreach var in food schooling health temp social nondurables durables {;
		gen conspc_`var' = cons_`var' / members_resid;
		replace conspc_`var'= conspc_`var' * 186.512/184.717 if wave ==1 | wave ==2; 
		};
	gen consumption_pc = consumption / members_resid;
		replace consumption_pc = consumption_pc * 186.512 / 184.717 if wave ==1 | wave ==2; 

	gen poor = (consumption_pc < 311.99 );
		replace poor = . if consumption_pc == .;

********************************************************************************************************
** Home Durable Assets
********************************************************************************************************; 
** create dummies for all assets in the questionnaire in C4; 
	foreach j of numlist 1(1)28 {; 
	gen asset_`j' = (c1_`j' == 1); 
	}; 

*** create a simple index;
	pca asset_1-asset_28, factor(1);
	predict score;
	matrix e= r(scoef)';
	svmat e, name(coef); 
	matrix list e;
	forvalues i=1(1)28{;
	scalar define xcoef`i'=coef`i';
	};
	forvalues j=1(1)28{;
	sum asset_`j', detail;
	scalar xa = r(mean); 
	scalar xb = r(sd); 
	gen score_`j'=xcoef`j'*(asset_`j'-xa)/xb;
	scalar drop xa xb;
	};
	egen assetindex=rsum(score_1-score_28);  
		drop asset_1- asset_28 coef1- coef28 score score_1-score_28;



********************************************************************************************************
** Employees
********************************************************************************************************; 
** Agricultural Employees; 
gen empl_agri = 0; 
replace empl_agri = empl_agri + e44*365  if e44 != . & e44 >= 0 ; * for permanent workers, we count them as working full day. Hence we take number of workers times 365 days; 
replace empl_agri = empl_agri + e46*e47_r if e46 != . & e46 >= 0 & e47_r != . & e47_r >= 0; 
replace empl_agri = empl_agri + e46 * e47_hr if e46 != . & e46 >= 0 & e47_hr != . & e47_hr >= 0; 

** Livestock Activities Employees; 
gen empl_livestock = 0 ; 
replace empl_livestock = empl_livestock + f22 * 365 if f22 !=. & f22 >= 0 ; 
replace empl_livestock = empl_livestock + f24 * f25 if f24 !=. & f24 >= 0 & f25 !=. & f25 >= 0 ; 

** Business Activities Employees; 
gen empl_business = 0; 
foreach j of numlist 1(1)6 {; 
replace empl_business = empl_business + g47_`j' * 365 if g47_`j' != . & g47_`j' >= 0 ; 
replace empl_business = empl_business + g49_`j' * g50_`j'  if g49_`j' != . & g50_`j' >= 0 & g49_`j' >=0 & g50_`j' != 0   ; 
};

** Total employed people in full people-days per year; 
egen empl_total = rowtotal(empl_agri empl_livestock empl_business); 


********************************************************************************************************
** Work of family members in own activities; 
********************************************************************************************************; 

* First measure: total person-days per household OVER THE PAST 12 MONTHS; 
	
* Agricultural Work;
 * days members worked during the last year during and out of harvest;  
gen work_agri = 0 ;  
foreach j of numlist 1(1)11 {; 
replace work_agri = work_agri + e42_`j' if e42_`j' != . & e42_`j' >= 0 ; 
replace work_agri = work_agri + e43_`j' if e43_`j' != . & e43_`j' >= 0 ; 
}; 

* Livestock work; 
* we want full work days, so we count full work day as 7 hours. We convert all work times into hours and divide by 7 to get person-days; 
gen work_livestock = 0 ; 
foreach j of numlist 1(1)13 {; 
replace work_livestock = work_livestock + f20_`j' * f21_`j' if f20_`j'  !=. & f20_`j'  >= 0 & f21_`j'  != . & f21_`j' >= 0 ;  
}; 
replace work_livestock = work_livestock/7; 


* Business Activities; 

* For wave 1; 
gen work_business = 0; 
foreach i of numlist 1(1)6 {;
foreach j of numlist 1(1)11 {; 
replace work_business = work_business + g45_`i'_`j'*12 if g45_`i'_`j' != . & g45_`i'_`j' >= 0  & wave == 1; 
}; 
}; 

* For waves 2, 3 and 4; 
**je compte comme activite du menage le 5 (compte propre) et 7 (aide dans une activite familiale), mais pas le 8 (Aide dans une activit� r�mun�r�e d�un membre du m�nage); 
foreach j of numlist 1(1)15 {; 
foreach i of numlist 1(1)3 {; 
replace work_business = work_business + h012_a`i'_`j'*12 if h012_a`i'_`j' != . & h012_a`i'_`j' >=0 & (h011_a`i'_`j' == 3| h011_a`i'_`j'== 4|h011_a`i'_`j' == 5|
h011_a`i'_`j' == 6|h011_a`i'_`j' == 7 | h011_a`i'_`j' == 9) & (h011_r`i'_`j' == 5 |h011_r`i'_`j' == 7) & (wave == 2 | wave == 34); 
}; 
}; 
egen work_total = rowtotal(work_business work_agri work_livestock); 


**** outliers in work variables;
foreach var in agri livestock business{;
_pctile work_`var', p(99.7); 
gen work_`var'_d =(work_`var'> r(r1)); 
};

foreach var in agri livestock business{; 
	replace work_`var' = . if work_`var'_d == 1; 
	summ work_`var' if work_`var'>0, detail; 
	replace work_`var' = r(p50) if work_`var'_d == 1; 
	}; 


***********************************;
** Weekly hours worked - past 7 days; 
***********************************; 
* Different construction for wave 1, versus waves 2, 3 and 4 (because of differences in survey instrument); 
* Constructed for those aged between 6 and 65;

* Waves 2, 3 and 4;
* We first identify questionnaires with no data for this section;
	gen nonmiss234=0 if (wave == 2 | wave == 34);
		foreach j of numlist 1(1)24 {; 
			replace nonmiss234 = nonmiss234 + 1 if h01_`j'!=. | h02_`j'==1 | h02_`j'==2 | h03_a1_`j'!=. ;
			};

	gen nonmiss1=0 if (wave == 1);
		foreach j of numlist 1(1)11 {; 
			replace nonmiss1 = nonmiss1 + 1 if a12_m`j'!=. | a12_p`j'!=. | a13_`j'!=. | a14_`j'!=.  ;
			};

** CODES RELATION DE TRAVAIL
	1 = Journalier
	2 = Salari� permanent
	3 = Apprenti r�mun�r�
	4 = Apprenti non-r�mun�r� 
	5 = Compte propre
	6 = M�tayer
	7 = Aide dans une activit� familiale
	8 = Aide dans une activit� r�mun�r�e d�un membre du m�nage
	9 = Autre; 

* We first create the groups we want to look at;

	foreach j of numlist 1(1)24 {; 
		gen age16_65`j' = (a7_`j' <=65 & a7_`j' >= 16);
		gen age6_65`j' = (a7_`j' <=65 & a7_`j' >= 6);

		gen age6_16`j' = (a7_`j' <16 & a7_`j' >=6);
		gen age16_20`j' = (a7_`j' <=20 & a7_`j' >=16);
		gen age20_50`j' = (a7_`j' <=50 & a7_`j' >20);
		gen age50_65`j' = (a7_`j' <=65 & a7_`j' >50);

		gen female`j' = (a4_`j'==2);
			replace female`j' = . if a4_`j'!=2 & a4_`j'!=1;  
		gen male`j' = (a4_`j'==1);
			replace male`j' = . if a4_`j'!=2 & a4_`j'!=1;  

		foreach i of numlist 1/3 {;
			gen self`j'`i'=(h03_r`i'_`j' == 5 | h03_r`i'_`j' == 7 );
				replace self`j'`i'=. if h03_r`i'_`j'<=0 & h03_r`i'_`j'>9;	
			gen outside`j'`i'= (inrange(h03_r`i'_`j',1,4) | h03_r`i'_`j'==6 | inrange(h03_r`i'_`j',8,9));
				replace outside`j'`i'=. if self`j'`i'==.; 
			gen self_agri`j'`i'=self`j'`i'==1 & (h03_a`i'_`j' == 1); 
			gen self_live`j'`i'=self`j'`i'==1 & (h03_a`i'_`j' == 2);
			gen self_other`j'`i'=self`j'`i'==1 & (inrange(h03_a`i'_`j',3,10));
			}; 
			}; 

* Now, we create same set of variables for each group;
	
	foreach group in age16_65 age6_65 age6_16 age16_20 age20_50 age50_65 female male {; 

		* we compute whether it exists at least one member in the HH that belongs to a given group;
		gen member_`group'=0;
		foreach j of numlist 1(1)24 {; 
			replace member_`group' = 1 if `group'`j'==1;
			};

		* we create these variables to compute the share later;
		gen nmember_`group'=0;
			label var nmember_`group' "number of members `group'";  
		foreach j of numlist 1(1)24 {; 
			replace nmember_`group' = nmember_`group' + 1 if `group'`j'==1;
			};



		*WARNING: do not change the following order in which variables are created - we later compute weekly hours worked per member based on this order;  

		foreach act in all self_out self self_agri self_live self_other outside chores {;
		foreach j of numlist 1(1)24 {;
			gen hours_`act'_`group'`j' = 0 if `group'`j'==1 ; 
			};
			};
			};



* WAVES 2,3 & 4: We now add time worked at the household level for each of the variables;

foreach group in age16_65 age6_65 age6_16 age16_20 age20_50 age50_65 female male {; 
foreach j of numlist 1(1)24 {; 
foreach act in self self_agri self_live self_other outside {;
foreach i of numlist 1(1)3 {; 

	replace hours_`act'_`group'`j' = hours_`act'_`group'`j' + h05_a`i'_`j'*h04_a`i'_`j' if (h05_a`i'_`j' <18 & h05_a`i'_`j' >= 0 & h04_a`i'_`j' <= 7 & h04_a`i'_`j' >= 0)    
	& (wave == 2 | wave == 34) & `act'`j'`i' == 1 & `group'`j'==1; 
	}; 
	}; 

	replace hours_chores_`group'`j' = hours_chores_`group'`j' + h07_`j'*h08_`j' if (h07_`j' <= 7 & h07_`j' >= 0 & h08_`j' < 18 & h08_`j' >= 0) 
	& (wave == 2 | wave == 34) & `group'`j'==1 ; 

	replace hours_all_`group'`j' = hours_self_`group'`j' + hours_outside_`group'`j' + hours_chores_`group'`j' if (wave == 2 | wave == 34) ; 
	replace hours_self_out_`group'`j' = hours_self_`group'`j' + hours_outside_`group'`j' if (wave == 2 | wave == 34) ; 
	}; 
	}; 



* WAVE 1; 
	* In wave 1 survey we asked for the total number of hours worked over the past 7 days;
	* For wave 1, we do not have time worked by sector;

foreach group in age16_65 age6_65 age6_16 age16_20 age20_50 age50_65 female male {; 
foreach j of numlist 1(1)11 {;
	replace hours_self_`group'`j' = hours_self_`group'`j'+ a12_p`j' if (a12_p`j' > 0 & a12_p`j' < 126 ) & wave == 1;
	replace hours_self_`group'`j' = hours_self_`group'`j'+ a12_m`j' if (a12_m`j' > 0 & a12_m`j' < 126 ) & wave == 1;
	replace hours_outside_`group'`j' = hours_outside_`group'`j' + a14_`j' if (a14_`j' > 0 & a14_`j' < 126 ) & wave == 1;
	replace hours_chores_`group'`j' = hours_chores_`group'`j' + a13_`j' if (a13_`j' > 0 & a13_`j' < 126 ) & wave == 1;

	replace hours_all_`group'`j' = hours_self_`group'`j' + hours_outside_`group'`j' + hours_chores_`group'`j' if wave == 1 ;
	replace hours_self_out_`group'`j' = hours_self_`group'`j' + hours_outside_`group'`j' if wave == 1 ;

	}; 
	}; 



* We replace by missing time worked higher than 18*7=126 hours;
foreach group in age16_65 age6_65 age6_16 age16_20 age20_50 age50_65 female male {; 
foreach j of numlist 1(1)24 {; 
foreach act in self self_agri self_live self_other outside chores {;
		replace hours_`act'_`group'`j' =. if hours_all_age6_65`j' >126 & hours_all_age6_65`j'!=.;
		};
		replace hours_all_`group'`j' =. if hours_all_age6_65`j' >126 & hours_all_age6_65`j'!=.;
		replace hours_self_out_`group'`j' =. if hours_all_age6_65`j' >126 & hours_all_age6_65`j'!=.;
		};
		};

* We now compute the SUM of hours, to get the total number of hours worked by HH members in a week; 
	foreach group in age16_65 age6_65 age6_16 age16_20 age20_50 age50_65 female male {; 
	foreach act in all self_out self self_agri self_live self_other outside chores {;
			egen hours_`act'_`group'=rsum(hours_`act'_`group'1-hours_`act'_`group'24);
			replace hours_`act'_`group'=. if nonmiss234 == 0 | nonmiss1 == 0; 
				label var hours_`act'_`group' "total hours worked in `act' by hh members `group'";  
			};
			};

* We put as missing group variables when the total is missing;
	foreach group in age16_65 age6_65 age6_16 age16_20 age20_50 age50_65 female male {; 
	foreach act in all self_out self self_agri self_live self_other outside chores {;
		replace hours_`act'_`group'=. if hours_all_age6_65==.;
		};
		};
	 sum hours_all_age16_65-hours_chores_male;

	foreach group in age16_65 age6_65 age6_16 age16_20 age20_50 age50_65 {; 
		replace nmember_`group'=. if hours_all_age6_65==.;
		};

* Wave 1; 
* We do not have info on hours worked over the past 7 days by sector, therefore we replace by missing;
	foreach group in age16_65 age6_65 age6_16 age16_20 age20_50 age50_65 female male {; 
	foreach act in self_agri self_live self_other {;
		gen hours_`act'_`group'_d = 0 ;
		replace hours_`act'_`group' = . if wave ==1 ;
		replace hours_`act'_`group'_d = 1 if wave ==1 ;
		};
		};


* we compute the share of members by group;
	foreach group in age6_16 age16_20 age20_50 age50_65 {;
		gen share_`group'= nmember_`group' / nmember_age6_65;
			label var share_`group' "share of members `group'";  
		replace share_`group'= 0 if hours_all_age6_65!=. & share_`group'==.;  
		replace share_`group'=. if hours_all_age6_65==.; 
		};


order nmember_age*;
drop nonmiss234- hours_chores_male24;
order ident- hours_self_other_male_d nmember_age16_65- nmember_age50_65;


********************************************************************************************************
********************************************************************************************************
** Impact on non-targeted Outcomes 
********************************************************************************************************
********************************************************************************************************; 

********************************************************************************************************
** Access to infrastructure: water and Sanitation 
********************************************************************************************************; 
** Sanitation; 
gen san_toilet = b10_1 == 1; 
gen san_bath = b10_2 == 1; 
gen san_toiletbath = (b10_1 == 1 & b10_2 == 1);   
gen san_good = (b14_1 == 1 | b14_2 == 1); 

** Water; 
gen water_good = (b13_1 == 1 | b13_2 == 1); 


********************************************************************************************************
** Schooling
********************************************************************************************************; 
gen sharekids_inschool = 0 ; 
	foreach j of numlist 1(1)24 {; 
	replace sharekids_inschool = sharekids_inschool + 1 if a15_`j' == 1 & a7_`j' <16 & a7_`j' >= 6; * variables a15 only recorded for those aged between 5 and 16; 
	}; 

	* counting kids between the ages of 6 and 15;
	gen members_6_15 = 0 ;  
	foreach j of numlist 1(1)24 {; 
	replace members_6_15 = members_6_15 + 1 if a7_`j' <16 & a7_`j' >= 6;
	}; 

	replace sharekids_inschool = sharekids_inschool / members_6_15;
	replace sharekids_inschool = 0 if members_6_15 == 0; 

gen shareteens_inschool = 0 ;	
	foreach j of numlist 1/24 {;
		replace shareteens_inschool = shareteens_inschool + 1 if a11_`j'==13 & a10_`j'!=2 & inrange(a7_`j',16,20);
		};
	* counting teenagers: aged 16 to 20;
	gen members_16_20 = 0 ;  
	foreach j of numlist 1(1)24 {; 
	replace members_16_20 = members_16_20 + 1 if inrange(a7_`j',16,20);
	}; 

	replace shareteens_inschool = shareteens_inschool / members_16_20;
	replace shareteens_inschool = 0 if members_16_20 == 0;

********************************************************************************************************
** Women's empowerment
********************************************************************************************************; 
* Questions J1 to J9;
	foreach i of numlist 1(1)9 {;  
		gen women_`i' = 0; 
	foreach j of numlist 1(1)11 {; 
		replace women_`i' = 1 if (j`i'_`j' == 1);
		}; 
		};
 
* Questions J9b;
	gen women_10 = 0; 
	foreach j of numlist 1(1)11 {; 
		replace women_10 = 1 if (j9b`j' == 1);
		}; 

* Questions J10 to J13;
	* we count as "allowed to do " something if the response says "plutot d'accord" ou "tout a fait d'accord"; 
	gen women_11 = (j10 == 3 | j10 == 4); 
	gen women_12 = (j11 == 3 | j11 == 4);
	gen women_13 = (j12 == 3 | j12 == 4);
	gen women_14 = (j13 == 3 | j13 == 4);

** Constructing a women empowerment index; 
	foreach i of numlist 1/14 {;
		summ women_`i', detail; 
		local mean = r(mean); 
		local std_s = r(sd); 
		gen outcome`i' = (women_`i'-`mean')/`std_s'; 
		}; 

egen women_index = rowtotal(outcome1 outcome2 outcome2 outcome4 outcome5 outcome6 outcome7 outcome8 outcome9 outcome10 outcome11 outcome12 outcome13 outcome14); 
	drop  women_1- outcome14;

** Household activities managed by women;
gen women_act_number = 0;
	foreach j of numlist 1(1)6 {;
	foreach i of numlist 1(1)24 {;
		replace women_act_number = women_act_number + 1 if d3_`j' == `i' & a4_`i' == 2;
		};
		};

gen women_act = (women_act_number > 0);


********************************************************************************************************
********************************************************************************************************
** 2.INTERACTIONS MECHANISMS CONTROL VARIABLES
********************************************************************************************************
********************************************************************************************************

********************************************************************************************************
** Household Head;
********************************************************************************************************; 

gen head_male = 0; 
	foreach j of numlist 1(1)16 {; 
	replace head_male = 1 if a3_`j' == 1 & a4_`j' == 1; 
	}; 

gen head_age = .; 
	foreach j of numlist 1(1)16 {; 
	replace head_age = a7_`j' if a3_`j' == 1 & a7_`j' != . & a7_`j' >= 0; 
	}; 

** Dummies for household head education; 
foreach i of numlist 1(1)16 {; 
	gen head_educ_`i' = 0; 
	foreach j of numlist 1(1)24 {; 
		replace head_educ_`i' = 1 if a3_`j' == 1 & a8_`j' == `i'; 
		}; 
		}; 


********************************************************************************************************
** Location
********************************************************************************************************; 
** Distance to the closest souk in km;
	egen distance_soukkm = rmin(b3_1 b3_2 b3_3); 

** in minutes, with public transportation; 
	egen distance_soukpt = rmin(b4_1_1 b4_1_2 b4_1_3); 

** in minutes, without public transportation; 
	egen distance_soukmin = rmin(b4_2_1 b4_2_2 b4_2_3); 


*****************************************************************************************************;
*****************************************************************************************************;
* WE LABEL OUTCOMES; 

* Respondent;
label var cm_resp_socio "1 if head responded to HH characteristics section";
label var ccm_resp_socio "1 if spouse of head responded to HH characteristics section"; 
label var other_resp_socio "1 if other member responded to HH characteristics section";   
label var male_resp_socio "1 if male responded to HH characteristics section";
label var cm_resp_activ "1 if head responded to self-employment section";                  
label var ccm_resp_activ "1 if spouse of head responded to self-employment section";       
label var other_resp_activ "1 if other member responded to self-employment section";
label var male_resp_activ "1 if male responded to self-employment section";
label var cm_resp_consump "1 if head responded to expenses section";
label var ccm_resp_consump "1 if spouse of head responded to expenses section";
label var other_resp_consump "1 if other member responded to expenses section";  
label var male_resp_consump "1 if male responded to expenses section";  
label var cm_resp_women "1 if head responded to females section";
label var ccm_resp_women "1 if spouse of head responded to females section";
label var other_resp_women "1 if other member responded to females section";
label var male_resp_women "1 if male responded to females section";
                
*Loans;
label var aloans_alamana "number of outstanding loans: alamana"; 
label var aloans_oformal "number of outstanding loans: other formal source"; 
label var aloans_informal "number of outstanding loans: informal source"; 
label var aloans_branching "number of outstanding loans: utility company";
label var aloans_oamc "number of outstanding loans: other mfi";
label var aloans_total "number of outstanding loans: total";
            
label var aloansamt_alamana "amount in MAD of outstanding loans: alamana"; 
label var aloansamt_oformal "amount in MAD of outstanding loans: other formal source"; 
label var aloansamt_informal "amount in MAD of outstanding loans: informal source";
label var aloansamt_branching "amount in MAD of outstanding loans: utility company";
label var aloansamt_oamc "amount in MAD of outstanding loans: other mfi";
label var aloansamt_oformal2 "amount in MAD of outstanding loans: other formal source";
label var aloansamt_total "amount in MAD of outstanding loans: total";  

label var ploans_alamana "number of loans that matured during the past 12 months: alamana"; 
label var ploans_oformal  "number of loans that matured during the past 12 months: other formal source"; 
label var ploans_informal "number of loans that matured during the past 12 months: informal source"; 
label var ploans_branching "number of loans that matured during the past 12 months: utility company";
label var ploans_oamc "number of loans that matured during the past 12 months: other mfi";
label var ploans_total "number of loans that matured during the past 12 months: total";
            
label var ploansamt_alamana "amount (in MAD) of loans that matured during the past 12 months: alamana"; 
label var ploansamt_oformal "amount (in MAD) of loans that matured during the past 12 months: other formal source"; 
label var ploansamt_informal "amount (in MAD) of loans that matured during the past 12 months: informal source";
label var ploansamt_branching "amount (in MAD) of loans that matured during the past 12 months: utility company";
label var ploansamt_oamc "amount (in MAD) of loans that matured during the past 12 months: other mfi";
label var ploansamt_total "amount (in MAD) of loans that matured during the past 12 months: total";  

label var loans_alamana "number of loans: alamana"; 
label var loans_oformal  "number of loans: other formal source"; 
label var loans_informal "number of loans: informal source"; 
label var loans_branching "number of loans: utility company";
label var loans_oamc     "number of loans: other mfi";
label var loans_total    "number of loans: total";
label var loans_oformal2  "number of loans: other formal source"; 

label var loansamt_alamana "amount (in MAD) of loans: alamana"; 
label var loansamt_oformal "amount (in MAD) of loans: other formal source"; 
label var loansamt_informal "amount (in MAD) of loans: informal source";
label var loansamt_branching "amount (in MAD) of loans: utility company";
label var loansamt_oamc  "amount (in MAD) of loans: other mfi";
label var loansamt_total "amount (in MAD) of loans: total";  

label var borrowed_alamana "1 if borrowed from alamana";               
label var borrowed_oformal "1 if borrowed from other formal source";                  
label var borrowed_informal "1 if borrowed from informal source";     
label var borrowed_branching "1 if borrowed from utility company";          
label var borrowed_oamc "1 if borrowed from other mfi";
label var borrowed_total "1 if borrowed from any source";      
label var borrowed_oformal2 "1 if borrowed from other formal source";

label var loans_cons "share of loans uptaken for consumption";
label var loans_inputs "share of loans uptaken to buy inputs";
label var loans_equip "share of loans uptaken to buy capital";
label var alamana_cons "share of alamana loans uptaken for consumption";
label var alamana_inputs "share of alamana loans uptaken to buy inputs";               
label var alamana_equip "share of alamana loans uptaken to buy capital";

label var nb_pasbesoin "1 if reason loan not requested: does not need it";
label var nb_peurrejet "1 if reason loan not requested: fear of being rejected"; 
label var nb_frais "1 if reason loan not requested: high fees";
label var nb_taux "1 if reason loan not requested: high interest rate";           
label var nb_distance "1 if reason loan not requested: branches far away";         
label var nb_montantinsuf "1 if reason loan not requested: amounts offered are small";          
label var nb_peurremboursement "1 if reason loan not requested: fear of not being able to reimburse"; 
label var nb_peurpertegarantie "1 if reason loan not requested: fear of loosing collateral"; 
label var nb_religion "1 if reason loan not requested: religious reasons";
label var nb_pasgarantie "1 if reason loan not requested: had no collateral"; 

label var credit_refused  "1 if loan request rejected during the past 12 months"; 
label var credit_notasked "1 if loan need during the past 12 month but not requested"; 
label var credit_need "1 if current loan need"; 
label var try_credit "1 if requested a loan during the past 12 months";

label var need_fin "share of own activities: need financing ";
label var need_form "share of own activities: need training ";
label var need_comm "share of own activities: need marketing ";                

* Self-employment activities;
label var inv_agri "agriculture: purchases of assets past 12 months (in MAD)";
label var asset_agri "agriculture: current stock of assets (in MAD)"; 
label var land_ha "surface area of land owned (in hectars)";
label var land "1 if owns land";
label var land_expl_ha "superficie of land exploited (in hectars)";

label var inv_livestockanim "animal husbandry: purchases of livestock past 12 months (in MAD)";
label var inv_livestockmat "animal husbandry: purchases of equipment past 12 months (in MAD)";
label var inv_livestock "animal husbandry: purchases of assets past 12 months (in MAD)";
label var asset_livestock "animal husbandry: current stock of assets (in MAD)";

label var inv_business_assets "Non-agricultural business: purchases of assets past 12 months (in MAD)";   
label var asset_business "Non-agricultural business: current stock of assets (in MAD)"; 
label var inv_business_inputs "Non-agricultural business: purchases of inputs past 12 months (in MAD)";

label var act_agri "1 if declared agricultural self-employment activity";
label var act_livestock "1 if declared animal husbandry self-employment activity";
label var act_business "1 if declared non-agricultural self-employment activity";

label var act_number "number of declared self-employment activities";
label var act_total_agri "number of types of agricultural self-employment activities";
label var act_total_live "number of types of animal husbandry self-employment activities";
label var act_number_business "number of non-agricultural self-employment activities";

label var selfempl_agri "1 if agricultural self-employment activity";
label var selfempl_livestock "1 if animal husbandry self-employment activity";
label var selfempl_business "1 if non-agricultural self-employment activity";
label var self_empl "1 if self-employment activity";

label var sale_cereal "sales and self-consumption of cereals past 12 months (in MAD)";
label var sale_tree "sales and self-consumption of tree fruits past 12 months (in MAD)";
label var sale_veg "sales and self-consumption of vegetables past 12 months (in MAD)";
label var sale_agri "sales and self-consumption of agriculture past 12 months (in MAD)"; 

label var sale_livestockanim "sales and self-consumption livestock animals past 12 months (in MAD)";    
label var sale_livestockprod "sales and self-consumption livestock products past 12 months (in MAD)";
label var sale_livestock "sales and self-consumption animal husbandry past 12 months (in MAD)";

label var sale_business "sales and self-consumption non-agricultural business past 12 months (in MAD)";
 
label var expense_agriinputs "agriculture: expenses in inputs past 12 months (in MAD)"; 
label var expense_agrilabor "agriculture: expenses in labor past 12 months (in MAD)"; 
label var expense_agriinv "agriculture: expenses in investment past 12 months (in MAD)"; 
label var expense_agrirent "agriculture: expenses in rent past 12 months (in MAD)"; 
label var expense_agri "agriculture: total expenses past 12 months (in MAD)";              

label var expense_livestockmatrent "animal husbandry: expenses in rent past 12 months (in MAD)"; 
label var expense_livestockmatinv "animal husbandry: expenses in equipment investment past 12 months (in MAD)";  
label var expense_livestockaniminv "animal husbandry: expenses in animal investment past 12 months (in MAD)"; 
label var expense_livestocklabor "animal husbandry: expenses in labor past 12 months (in MAD)"; 
label var expense_livestockinputs "animal husbandry: expenses in inputs past 12 months (in MAD)"; 
label var expense_livestock "animal husbandry: total expenses past 12 months (in MAD)";

label var expense_busrent "non-agricultural business: expenses in rent past 12 months (in MAD)";
label var expense_businv "non-agricultural business: expenses in investment past 12 months (in MAD)"; 
label var expense_businputs "non-agricultural business: expenses in inputs past 12 months (in MAD)";
label var expense_buslabor "non-agricultural business: expenses in labor past 12 months (in MAD)";
label var expense_business "non-agricultural business: total expenses past 12 months (in MAD)";

label var savings_cereal "cereals: current stock (in MAD)";
label var savings_tree "tree fruits: current stock (in MAD)";
label var savings_veg "vegetables: current stock (in MAD)";
label var savings_agri "agriculture: current stock (in MAD)";
label var savings_livestock "livestock: current stock(in MAD)"; 
label var savings_livestock_prod "livestock products: current stock (in MAD)";
label var savings_business "non-agricultural business: current stock (in MAD)";
label var savings_mon "current monetary savings (in MAD)"; 

label var prod_agri "agriculture output past 12 months: sales, self-consumption and current stock (in MAD)";  
label var stock_agri "agriculture: assets and current output stock ";
label var astock_livestock "animal husbandry: assets and livestock stock (in MAD)";
label var astock_business "non-agricultural business: assets and output stock (in MAD)";
label var output_total "total output from self-employment activities past 12 months";

label var assets_total "total current stock of assets of self-employment activities, including livestock stock (in MAD)";
label var inv_total "total purchases of assets self-employment activitites past 12 months (in MAD)";
label var expense_total "total expenses self-employment activitites past 12 months (in MAD)";

label var profit_agri "agriculture: profit past 12 months (in MAD)";
label var profit_livestock "animal husbandry: profit past 12 months (in MAD)";
label var profit_business "non-agricultural business: profit past 12 months (in MAD)";
label var profit_total "self-employment activities: total profit past 12 months (in MAD)";

label var income_agri "income from agriculture past 12 months (in MAD)";
label var income_livestock "income from animal husbandry past 12 months (in MAD)";
label var income_business "income from non-agricultural business past 12 months (in MAD)";
label var income_dep "income from day labor and salaried past 12 months (in MAD)"; 
label var daily_dum "1 if has a self-employment activity";
label var income_remittance "income from remittances past 12 months (in MAD)"; 
label var income_other "income from pension, asset sales, goverment and other source past 12 month (in MAD)";
label var income_pension "income from retirement and pension past 12 months (in MAD)";
label var income_assetsales "income from asset sales past 12 months (in MAD)"; 
label var income_gvt "income from gov program past 12 months (in MAD)";
label var income_othoth "income from other source past 12 months (in MAD)"; 
label var income  "total income past 12 months (in MAD)";

label var income_source_ag "1 if declared main income source agriculture";
label var income_source_live "1 if declared main income source animal husbandry"; 
label var income_source_bus "1 if declared main income source non-agricultural business"; 
label var income_source_dep "1 if declared main income source day work and salaried"; 
label var income_source_other "1 if declared main income source other";

label var income_work "income from self-employment activities, day work and salaried past 12 months (in MAD)";
label var income_other_new "income from remittances, pension, goverment and other source past 12 month (in MAD)";

label var lost_livestockanim "livestock lost past 12 months (in MAD)"; 
label var hadlost_livestockanim "1 if lost livestock past 12 months"; 

* Shocks;
label var shock1 "1 if shock to agriculture or animal husbandry production";
label var shock1agri "1 if prevented to use more than half of the land or lost more than half of the harvest";
label var shock1live "1 if lost more than half of livestock";
label var shock2 "1 if health or house damage incident"; 

* Consumption;
label var cons_food "monthly food consumption (in MAD)";
label var cons_food_usual "monthly usual food consumption (in MAD)";
label var cons_durables "monthly expenditure on durables (in MAD)";
label var cons_nondurables "monthly expenditure on non-durables (in MAD)";
label var cons_schooling "monthly expenditure on education (in MAD)";
label var cons_health "monthly  expenditure on health (in MAD)";
label var cons_temp "monthly  expenditure on temptation & entertainement(in MAD)";
label var cons_ramadan "monthly  expenditure on ramadan celebration (in MAD)";
label var cons_aid "monthly  expenditure on aid el kbir celebration (in MAD)";
label var cons_social "monthly  expenditure on festivals and celebrations (in MAD)";
label var cons_repay "monthly loan repayments (in MAD)";
label var consumption "total monthly consumption (in MAD)";

* Per capita consumption;
label var members "number of people listes in hh roster";       
label var members_resid "number of hh members";
label var nadults_resid "number of members 16 years old or older";
label var nchildren_resid "number of membres younger than 16 years old";

label var conspc_food "monthly food per capita consumption (in MAD)";
label var conspc_schooling "monthly per capita expenditure on education (in MAD)";
label var conspc_health "monthly per capita expenditure onhealth (in MAD)";
label var conspc_temp "monthly per capita expenditure on temptation & entertainement(in MAD)";
label var conspc_social "monthly per capita expenditure on festivals and celebrations (in MAD)";
label var conspc_nondurables "monthly per capita expenditure on non-durables (in MAD)";
label var conspc_durables "monthly per capita expenditure on durables (in MAD)";
label var consumption_pc "total monthly per capita consumption (in MAD)";
label var poor "1 if household is poor";      
label var assetindex "index of home durable assets";

* Labor self-employment activities;
label var empl_agri "agriculture: days worked by employees past 12 months";          
label var empl_livestock "animal husbandry: days worked by employees past 12 months";          
label var empl_business "non-agricultural business: days worked by employees past 12 months";          
label var empl_total "all self-employment activities: days worked by employees past 12 months";          

label var work_agri "agriculture: days worked by members past 12 months";           
label var work_livestock "animal husbandry: days worked by members past 12 months";           
label var work_business "non-agricultural business: days worked by members past 12 months";           
label var work_total "all self-employment activities: days worked by members past 12 months";           

* Labor hh members past 7 days;
* with var creation;

* Sanitation and water;
label var san_toilet "1 if has a toilet";
label var san_bath "1 if has a bath or shower";
label var san_toiletbath "1 if has a toilet or a bath/shower";

label var san_good "1 if sewage system or septic tank";
label var water_good "1 if individual or tap connection"; 

* Social; 
label var sharekids_inschool "share of kids aged 6-15 in school"; 
label var members_6_15 "number of children aged 6-15"; 
label var shareteens_inschool "share of teenagers aged 16-20 in school"; 
label var members_16_20 "number of members aged 16-20";

label var women_index "Index of women independence";
label var women_act_number "number of self-employment activities managed by women"; 
label var women_act "1 if self-employment activity managed by women"; 

* Controls;
label var head_male "if male head"; 
label var head_age "head age";

label var head_educ_1 "1 if educational attainment: none";                 
label var head_educ_2 "1 if educational attainment: koranic";                
label var head_educ_3 "1 if educational attainment: 1st grade";
label var head_educ_4 "1 if educational attainment: 2nd grade";
label var head_educ_5 "1 if educational attainment: 3rd grade";
label var head_educ_6 "1 if educational attainment: 4th grade";
label var head_educ_7 "1 if educational attainment: 5th grade";
label var head_educ_8 "1 if educational attainment: 6th grade";
label var head_educ_9 "1 if educational attainment: 7th grade";
label var head_educ_10 "1 if educational attainment: 8th grade";
label var head_educ_11 "1 if educational attainment: 9th grade";
label var head_educ_12 "1 if educational attainment: 10th grade";
label var head_educ_13 "1 if educational attainment: 11th grade";
label var head_educ_14 "1 if educational attainment: 12th grade";
label var head_educ_15 "1 if educational attainment: higher educ/university";
label var head_educ_16 "1 if educational attainment: professional training";

label var distance_soukkm "distance to souk (in kilometers)";                  
label var distance_soukpt "distance to souk by public transportation (in minutes)";                  
label var distance_soukmin "distance to souk without public transportation (in minutes)";


****************************************************;
****************************************************;
** SAVING DATASET + OUTCOMES;
****************************************************;
sort ident; 
save Output\endline_minienquete_outcomes.dta, replace; 



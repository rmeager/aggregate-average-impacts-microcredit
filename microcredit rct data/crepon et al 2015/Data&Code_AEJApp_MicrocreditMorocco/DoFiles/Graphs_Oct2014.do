************************************************
* This do file creates graphs 
* PART 1 Quantiles
************************************************
clear 
capture clear matrix
set mem 500m
set maxvar 10000
cap log close

set more off

*** SELECT MAIN DIRECTORY
global MAIN_DIR "C:\Users\Florencia Devoto\Dropbox\MicroCredit\Data"

***********************

cd "$MAIN_DIR"

***********************
use "Output/endline_baseline_outcomes.dta",replace


tab1 paire,gen(p)

*** sample 

keep if samplemodel==1
drop if trimobs==1

capture program drop NewregqM
prog def NewregqM, rclass
args var
clear matrix
sca drop _all
local sca var Varb Varbs n
count if treatment==1
sca ntot1=r(N)
count if treatment==0
sca ntot0=r(N)
matrix T=J(81,1,1)#(0\1)
matrix P=I(81)#J(2,1,1)
matrix X=(T,P)
matrix Isig=J(162,162,0)
foreach i of numlist 1 2 3 4 5 {
        matrix Sig`i'=J(162,162,0)
        }
matrix  q_0=J(5,1,0)
matrix  q_1=J(5,1,0)
foreach nump of numlist 1/81 {
        foreach val of numlist 0 1 {
                quietly centile `var' if p`nump'==1 & treatment==`val',centile(10 25 50 75 90) level(95)
                sca n`val'=r(N)
                matrix q_`val'=q_`val'+n`val'*(r(c_1)\r(c_2)\r(c_3)\r(c_4)\r(c_5))/ntot`val'
                foreach i of numlist 1 2 3 4 5 {
                        matrix Isig[(`nump'-1)*2+`val'+1,(`nump'-1)*2+`val'+1]=n`val'
                        matrix theta`i'=nullmat(theta`i')\r(c_`i')
                        sca var`i'=((r(ub_`i')-r(lb_`i'))/(invnormal((1+.95)/2)-invnormal((1-.95)/2) ))
                        matrix Vecvar`i'=nullmat(Vecvar`i')\var`i'
                        matrix Sig`i'[(`nump'-1)*2+`val'+1,(`nump'-1)*2+`val'+1]=var`i'
                        }
                }
        }

foreach i of numlist 1 2 3 4 5 {
        matrix b=invsym(X'*Isig*X)*X'*Isig*theta`i'
		matrix b1=b[1,1]
		disp `i'
		matrix list b1 
        matrix uhat=theta`i'-X*b

        matrix uhat2=hadamard(uhat,uhat)
        matrix uhatres=uhat2-Vecvar`i'
        matrix RegT=(J(162,1,1),T)
        matrix Sig01=invsym(RegT'*RegT)*RegT'*uhatres
        matrix Dsig01=J(2,2,0)
        matrix Dsig01[1,1]=Sig01[1,1]
        matrix Dsig01[2,2]=Sig01[1,1]+Sig01[2,1]
        matrix Sig2=Sig`i'+I(81)#Dsig01
        matrix Isig2=invsym(Sig2)

        matrix Sig3=J(162,162,0)
        foreach nump of numlist 1/81 {
		
                foreach val of numlist 0 1 {
                        matrix Sig3[(`nump'-1)*2+`val'+1,(`nump'-1)*2+`val'+1]=uhat2[(`nump'-1)*2+`val'+1,1]
                        }
                }


        matrix b2=invsym(X'*Isig2*X)*X'*Isig2*theta`i'
        matrix Varb2R=invsym(X'*Isig2*X)*X'*Isig2*Sig3*Isig2*X*invsym(X'*Isig2*X)
        sca b2=b2[1,1]
        sca Varb2R=Varb2R[1,1]
        sca sb2R=sqrt(Varb2R)
        matrix res=nullmat(res)\(b2,sb2R,b2/sb2R,2*(1-normal(abs(b2/sb2R))))
        }
matrix res=((10\25\50\75\90),(res,q_0,((res[1,1]/q_0[1,1])\(res[2,1]/q_0[2,1])\(res[3,1]/q_0[3,1])\(res[4,1]/q_0[4,1])\(res[5,1]/q_0[5,1]))))


end


gen id=.
gen vars=""
gen N=.
gen n_surveyed=.
gen mean_surveyed=.


gen coef10=.
gen coef25=.
gen coef50=.
gen coef75=.
gen coef90=.

gen se10=.
gen se25=.
gen se50=.
gen se75=.
gen se90=.

global variables_quantile profit_total assets_total consumption income income_dep




local i 0

foreach var of global variables_quantile{
local i=`i'+1

replace vars="`var'" if _n==`i'
replace id=`i' if _n==`i'


sum `var' if samplemodel==1 
	replace n_surveyed=r(N) if _n==`i'

NewregqM `var'
	


replace coef10=res[1,2] if _n==`i'
replace coef25=res[2,2] if _n==`i'
replace coef50=res[3,2] if _n==`i'
replace coef75=res[4,2] if _n==`i'
replace coef90=res[5,2] if _n==`i'  

replace se10=res[1,3] if _n==`i'
replace se25=res[2,3] if _n==`i'
replace se50=res[3,3] if _n==`i'
replace se75=res[4,3] if _n==`i'
replace se90=res[5,3]  if _n==`i'

	}
	
keep id- se90
keep in 1/5	

reshape long coef se /*ci_minus_se ci_plus_se*/, i(id) j(Quantile)

gen quantile=Quantile
replace quantile=1 if Quantile==10
replace quantile=2 if Quantile==25
replace quantile=3 if Quantile==50
replace quantile=4 if Quantile==75
replace quantile=5 if Quantile==90

 label define quantile 1 "10" 2 "25" 3 "50" 4 "75" 5 "90"
label values quantile quantile


serrbar coef se quantile if id==1, scale (1.96) title("Profits") yline(0) b2(95% Confidence Interval ) xlab(1(1)5) xlabel(, valuelabel)
graph export "Tables_paper/Quantile_profits.eps", replace 
serrbar coef se quantile if id==2, scale (1.96) title("Assets (stock)") yline(0) b2(95% Confidence Interval ) xlab(1(1)5) xlabel(, valuelabel)
graph export "Tables_paper/Quantile_assets.eps", replace 
serrbar coef se quantile if id==3, scale (1.96) title("Consumption") yline(0) b2(95% Confidence Interval ) xlab(1(1)5) xlabel(, valuelabel)
graph export "Tables_paper/Quantile_consumption.eps", replace 
serrbar coef se quantile if id==4, scale (1.96) title("Income Work") yline(0) b2(95% Confidence Interval ) xlab(1(1)5) xlabel(, valuelabel)
graph export "Tables_paper/Quantile_income_work.eps", replace 
serrbar coef se quantile if id==5, scale (1.96) title("Daily labor & salaried") yline(0) b2(95% Confidence Interval ) xlab(1(1)5) xlabel(, valuelabel)
graph export "Tables_paper/Quantile_income_dep.eps", replace 



*******************************************
* This do file creates graphs - PART 2 CDF
******************************************
clear
clear matrix
set memory 700m 
set matsize 10000
set more off

***********************

use "Output/endline_baseline_outcomes.dta",replace

*****************
keep if samplemodel==1      

*** profits***

cap program drop qiv_profit_total
program define qiv_profit_total
clear matrix
cap drop p1 p0 p11 p10
cap drop y0 y02 y01
gen p1=(treatment==1)
gen p0=(treatment==0)
replace p1=. if profit_total==.
replace p0=. if profit_total==.
gen p11=client==1 & treatment==1
replace p11=. if profit_total==.
gen p10=client==0 & treatment==1 
replace p10=. if profit_total==.


sum p1 , d
scalar p1_mean=r(mean)

sum p0 , d
scalar p0_mean=r(mean)

sum p11 , d
scalar p11_mean=r(mean)

sum p10 , d
scalar p10_mean=r(mean)

sca y0p=0
sca y1p=0


forvalues i= -400000(1000)912000 {

cap drop y1
gen y1=(profit_total<`i' & treatment==1 & client==1  )
replace y1 =. if profit_total==. 
replace y1=y1/(p11_mean)
cap drop y0 y01 y02
gen y01=(profit_total<`i' & treatment==0 )
gen y02=(profit_total<`i' & treatment==1 & client==0)
gen y0=(y01/(p0_mean)-y02/(p1_mean))*p1_mean/p11_mean

replace y0=. if profit_total==. 
quietly sum y0
sca y0m=r(mean)
sca y0m=max(y0m,y0p)
sca y0p=y0m
quietly sum y1
sca y1m=r(mean)
sca y1m=max(y1m,y1p)
sca y1p=y1m
matrix b=(`i', y0m,y1m)
matrix B=nullmat(B)\b

}
mat nr=rowsof(B)
sca nr=nr[1,1]
sca B0nr=B[nr,2]
sca B1nr=B[nr,3]

mat scale=(1,0,0)\(0,1/B0nr,0)\(0,0,1/B1nr)
mat B=B*scale
end

qiv_profit_total

preserve 
clear
svmat B
rename B1 profit 
rename B2 CDF0
rename B3 CDF1
keep if profit>=-100000 & profit<=200000

matrix list B

twoway ///
 (line CDF0 profit, color(black)  lpattern(dash) /*msize(vsmall)*/)  ///
  (line CDF1 profit, color(black)  lpattern(solid) /*msize(vsmall)*/), ///
legend(label(2 "Treatment") label(1 "Control")  order(1 2) position(6) /*r(1)*/)  /// 
xscale(range(-100000 200000)) ///
xlabel(-100000[50000]200000) ///
yscale(range(0 1)) ///
ylabel(0[0.2]1) ///
ytitle(Compliers CDF) /// 
xtitle(Amount in moroccan dirhams) ///
title("Profits")
graph export "Tables_paper/CDF_profit.eps", replace 
drop  profit CDF0 CDF1  
restore




*** assets***

cap program drop qiv_assets
program define qiv_assets
clear matrix
cap drop p1 p0 p11 p10
cap drop y0 y02 y01
gen p1=(treatment==1)
gen p0=(treatment==0)
replace p1=. if assets_total==.
replace p0=. if assets_total==.
gen p11=client==1 & treatment==1
replace p11=. if assets_total==.
gen p10=client==0 & treatment==1 
replace p10=. if assets_total==.


sum p1 , d
scalar p1_mean=r(mean)

sum p0 , d
scalar p0_mean=r(mean)

sum p11 , d
scalar p11_mean=r(mean)

sum p10 , d
scalar p10_mean=r(mean)

sca y0p=0
sca y1p=0


forvalues i= 0(100)150000 {
cap drop y1
gen y1=(assets_total<`i' & treatment==1 & client==1  )
replace y1 =. if assets_total==. 
replace y1=y1/(p11_mean)
cap drop y0 y01 y02
gen y01=(assets_total<`i' & treatment==0 )
gen y02=(assets_total<`i' & treatment==1 & client==0)
gen y0=(y01/(p0_mean)-y02/(p1_mean))*p1_mean/p11_mean
replace y0=. if assets_total==. 
quietly sum y0
sca y0m=r(mean)
sca y0m=max(y0m,y0p)
sca y0p=y0m
quietly sum y1
sca y1m=r(mean)
sca y1m=max(y1m,y1p)
sca y1p=y1m
matrix b=(`i', y0m,y1m)
matrix B=nullmat(B)\b

}

mat nr=rowsof(B)
sca nr=nr[1,1]
sca B0nr=B[nr,2]
sca B1nr=B[nr,3]

mat scale=(1,0,0)\(0,1/B0nr,0)\(0,0,1/B1nr)
mat B=B*scale

end
qiv_assets


preserve 
clear
svmat B
rename B1 assets 
rename B2 CDF0
rename B3 CDF1

matrix list B
keep if assets >0
twoway ///
 (line CDF0 assets, color(black)  lpattern(dash) /*msize(vsmall)*/)  ///
  (line CDF1 assets, color(black)  lpattern(solid) /*msize(vsmall)*/), ///
legend(label(2 "Treatment") label(1 "Control")  order(1 2) position(6) /*r(1)*/)  /// 
xscale(range(0 150000)) ///
xlabel(0[50000]150000) ///
yscale(range(0 1)) ///
ylabel(0[0.2]1) ///
ytitle(Compliers CDF) ///
xtitle(Amount in moroccan dirhams) /// 
title("Assets (stocks)")
graph export "Tables_paper/CDF_assets.eps", replace 
drop  assets CDF0 CDF1  
restore

*** income_dep***

cap program drop qiv_income_dep
program define qiv_income_dep
clear matrix
cap drop p1 p0 p11 p10
cap drop y0 y02 y01
gen p1=(treatment==1)
gen p0=(treatment==0)
replace p1=. if income_dep==.
replace p0=. if income_dep==.
gen p11=client==1 & treatment==1
replace p11=. if income_dep==.
gen p10=client==0 & treatment==1 
replace p10=. if income_dep==.


sum p1 , d
scalar p1_mean=r(mean)

sum p0 , d
scalar p0_mean=r(mean)

sum p11 , d
scalar p11_mean=r(mean)

sum p10 , d
scalar p10_mean=r(mean)

sca y0p=0
sca y1p=0


forvalues i= 0(50)80000 {
cap drop y1
gen y1=(income_dep<`i' & treatment==1 & client==1  )
replace y1 =. if income_dep==. 
replace y1=y1/(p11_mean)
cap drop y0 y01 y02
gen y01=(income_dep<`i' & treatment==0 )
gen y02=(income_dep<`i' & treatment==1 & client==0)
gen y0=(y01/(p0_mean)-y02/(p1_mean))*p1_mean/p11_mean
replace y0=. if income_dep==. 
quietly sum y0
sca y0m=r(mean)
sca y0m=max(y0m,y0p)
sca y0p=y0m
quietly sum y1
sca y1m=r(mean)
sca y1m=max(y1m,y1p)
sca y1p=y1m
matrix b=(`i', y0m,y1m)
matrix B=nullmat(B)\b

}
mat nr=rowsof(B)
sca nr=nr[1,1]
sca B0nr=B[nr,2]
sca B1nr=B[nr,3]

mat scale=(1,0,0)\(0,1/B0nr,0)\(0,0,1/B1nr)
mat B=B*scale

end
qiv_income_dep


preserve 
clear
svmat B
rename B1 income_dep 
rename B2 CDF0
rename B3 CDF1
keep if income_dep>0
matrix list B

twoway ///
 (line CDF0 income_dep, color(black)  lpattern(dash) /*msize(vsmall)*/)  ///
  (line CDF1 income_dep, color(black)  lpattern(solid) /*msize(vsmall)*/), ///
legend(label(2 "Treatment") label(1 "Control")  order(1 2) position(6) /*r(1)*/)  /// 
xscale(range(0 80000)) ///
xlabel(0[20000]80000) ///
yscale(range(0 1)) ///
ylabel(0[0.2]1) ///
ytitle(Compliers CDF) /// 
xtitle(Amount in moroccan dirhams) ///
title("Income from Daily Labor and Salaried Work")
graph export "Tables_paper/CDF_income_dep.eps", replace 
drop  income_dep CDF0 CDF1  
restore


*** income_work ***
cap program drop qiv_income_work
program define qiv_income_work
clear matrix
cap drop p1 p0 p11 p10
cap drop y0 y02 y01
gen p1=(treatment==1)
gen p0=(treatment==0)
replace p1=. if income_work ==.
replace p0=. if income_work ==.
gen p11=client==1 & treatment==1
replace p11=. if income_work ==.
gen p10=client==0 & treatment==1 
replace p10=. if income_work==.


sum p1 , d
scalar p1_mean=r(mean)

sum p0 , d
scalar p0_mean=r(mean)

sum p11 , d
scalar p11_mean=r(mean)

sum p10 , d
scalar p10_mean=r(mean)

sca y0p=0
sca y1p=0


forvalues i= -54000(500)223000{
cap drop y1
gen y1=(income_work <`i' & treatment==1 & client==1  )
replace y1 =. if income_work==. 
replace y1=y1/(p11_mean)
cap drop y0 y01 y02
gen y01=(income_work <`i' & treatment==0 )
gen y02=(income_work <`i' & treatment==1 & client==0)
gen y0=(y01/(p0_mean)-y02/(p1_mean))*p1_mean/p11_mean
replace y0=. if income_work ==. 
quietly sum y0
sca y0m=r(mean)
sca y0m=max(y0m,y0p)
sca y0p=y0m
quietly sum y1
sca y1m=r(mean)
sca y1m=max(y1m,y1p)
sca y1p=y1m
matrix b=(`i', y0m,y1m)
matrix B=nullmat(B)\b

}
mat nr=rowsof(B)
sca nr=nr[1,1]
sca B0nr=B[nr,2]
sca B1nr=B[nr,3]

mat scale=(1,0,0)\(0,1/B0nr,0)\(0,0,1/B1nr)
mat B=B*scale

end
qiv_income_work


preserve 
clear
svmat B
rename B1 income_work 
rename B2 CDF0
rename B3 CDF1

matrix list B
keep if income_work>=-50000 & income_work<=200000

twoway ///
 (line CDF0 income_work, color(black)  lpattern(dash) /*msize(vsmall)*/)  ///
  (line CDF1 income_work, color(black)  lpattern(solid) /*msize(vsmall)*/), ///
legend(label(2 "Treatment") label(1 "Control")  order(1 2) position(6) /*r(1)*/)  /// 
xscale(range(-50000 200000)) ///
xlabel(-50000[50000]200000) ///
yscale(range(0 1)) ///
ylabel(0[0.2]1) ///
ytitle(Compliers CDF) /// 
xtitle(Amount in moroccan dirhams) ///
title("Income from Work")
graph export "Tables_paper/CDF_income_work.eps", replace 
drop  income_work CDF0 CDF1  
restore



*** consumption ***

cap program drop qiv_consumption 
program define qiv_consumption 
clear matrix
cap drop p1 p0 p11 p10
cap drop y0 y02 y01
gen p1=(treatment==1)
gen p0=(treatment==0)
replace p1=. if consumption ==.
replace p0=. if consumption ==.
gen p11=client==1 & treatment==1
replace p11=. if consumption ==.
gen p10=client==0 & treatment==1 
replace p10=. if consumption==.


sum p1 , d
scalar p1_mean=r(mean)

sum p0 , d
scalar p0_mean=r(mean)

sum p11 , d
scalar p11_mean=r(mean)

sum p10 , d
scalar p10_mean=r(mean)

sca y0p=0
sca y1p=0


forvalues i= 0(10)15000 {
cap drop y1
gen y1=(consumption <`i' & treatment==1 & client==1  )
replace y1 =. if consumption ==. 
replace y1=y1/(p11_mean)
cap drop y0 y01 y02
gen y01=(consumption <`i' & treatment==0 )
gen y02=(consumption <`i' & treatment==1 & client==0)
gen y0=(y01/(p0_mean)-y02/(p1_mean))*p1_mean/p11_mean
replace y0=. if consumption ==. 
quietly sum y0
sca y0m=r(mean)
sca y0m=max(y0m,y0p)
sca y0p=y0m
quietly sum y1
sca y1m=r(mean)
sca y1m=max(y1m,y1p)
sca y1p=y1m
matrix b=(`i', y0m,y1m)
matrix B=nullmat(B)\b

}
mat nr=rowsof(B)
sca nr=nr[1,1]
sca B0nr=B[nr,2]
sca B1nr=B[nr,3]

mat scale=(1,0,0)\(0,1/B0nr,0)\(0,0,1/B1nr)
mat B=B*scale

end
qiv_consumption 


preserve 
clear
svmat B
rename B1 consumption 
rename B2 CDF0
rename B3 CDF1

matrix list B

twoway ///
 (line CDF0 consumption, color(black)  lpattern(dash) /*msize(vsmall)*/)  ///
  (line CDF1 consumption, color(black)  lpattern(solid) /*msize(vsmall)*/), ///
legend(label(2 "Treatment") label(1 "Control")  order(1 2) position(6) /*r(1)*/)  /// 
xscale(range(0 15000)) ///
xlabel(0[5000]15000) ///
yscale(range(0 1)) ///
ylabel(0[0.2]1) ///
ytitle(Compliers CDF) /// 
xtitle(Amount in moroccan dirhams) ///
title("Consumption")
graph export "Tables_paper/CDF_consumption.eps", replace 
drop  consumption CDF0 CDF1  
restore


















Option Explicit
Option Private Module

Function strParam(row As Integer, name As String) As String
	Dim param As String

	param = Trim$(LCase$(V("A", row)))
	If param = "" Then
		Range("A" + LTrim$(Str$(row))).Formula = "no"
		param = "no"
	ElseIf param <> "yes" And param <> "no" Then
		MsgBox "The " + name + " option must be either ""yes"" or ""no"".", vbCritical
		param = ""
	End If

	strParam = param
End Function

Sub FormatTables()
	Const inDirRow As Integer = 3
	Const outDirRow As Integer = 7
	Const keepOpenRow As Integer = 11
	Const appendRow As Integer = 15
	Const appendNameRow As Integer = 18
	Const saveAppendOnlyRow As Integer = 21
	Const keepAppendOnlyRow As Integer = 24

	' "nShs" for "number of sheets"
	Dim nShs As Integer
	Dim startWb As String, startSh As String, inDir As String, outDir As String, _
		keepOpen As String, append As String, appendName As String, _
		saveAppendOnly As String, keepAppendOnly As String, file As String, _
		lastSh As String, newName As String
	Dim anyFiles As Boolean, wbOpen As Boolean, noKill As Boolean
	Dim appendWb As Workbook, wb As Workbook

	noKill = False

	Application.ScreenUpdating = False
	Application.DisplayAlerts = False

	startWb = ActiveWorkbook.name
	startSh = ActiveSheet.name
	ThisWorkbook.Activate

	' Import parameters.

	On Error GoTo noParametersErr
	' Activate the parameters sheet. See "FormatTables parameters sheet
	' example.csv" for an example sheet.
	Sheets("Parameters").Activate
	On Error GoTo 0

	inDir = V("A", inDirRow)
	If inDir = "" Then
		MsgBox "Input directory is required.", vbCritical
		GoTo endSub
	End If
	On Error GoTo inDirErr
	ChDir inDir
	On Error GoTo 0

	outDir = V("A", outDirRow)
	If outDir = "" Then
		MsgBox "Output directory is required.", vbCritical
		GoTo endSub
	End If
	On Error GoTo outDirErr
	ChDir outDir
	On Error GoTo 0

	If inDir = outDir Then
		MsgBox "Input and output directories cannot be the same.", vbCritical
		GoTo endSub
	End If

	keepOpen = strParam(keepOpenRow, "keep open")
	If keepOpen = "" Then GoTo endSub

	append = strParam(appendRow, "append .xlsx files")
	If append = "" Then
		GoTo endSub
	ElseIf append = "yes" Then
		' Open the appended .xlsx file.
		appendName = V("A", appendNameRow)
		If Right$(appendName, 5) <> ".xlsx" Then appendName = appendName + ".xlsx"
		nShs = Application.SheetsInNewWorkbook
		Application.SheetsInNewWorkbook = 1
		Set appendWb = Workbooks.Add
		Application.SheetsInNewWorkbook = nShs
		lastSh = "__temp"
		Sheets("Sheet1").name = lastSh
		ChDir outDir
		On Error Resume Next
		Workbooks(appendName).Close False
		On Error GoTo saveAppendedErr
		appendWb.SaveAs appendName, FileFormat:=xlOpenXMLWorkbook
		On Error GoTo 0
		ThisWorkbook.Activate
	End If

	saveAppendOnly = strParam(saveAppendOnlyRow, "save appended .xlsx only")
	If saveAppendOnly = "" Then GoTo endSub

	keepAppendOnly = strParam(keepAppendOnlyRow, "keep open appended .xlsx only")
	If keepAppendOnly = "" Then GoTo endSub

	' Format tables.

	anyFiles = False
	ChDir inDir
	file = Dir("*.csv")
	While file <> ""
		On Error GoTo openErr
		Set wb = Workbooks.Open(file)
		On Error GoTo 0
		wbOpen = True

		If FormatCsv = -1 Then
			GoTo endSub
		Else
			If append = "yes" Then
				ActiveSheet.Copy After:=appendWb.Sheets(lastSh)
				lastSh = ActiveSheet.name
			End If

			If append = "no" Or saveAppendOnly = "no" Then
				ChDir outDir
				newName = Left$(wb.name, Len(wb.name) - Len(".csv")) + ".xlsx"
				On Error Resume Next
				Workbooks(newName).Close False
				On Error GoTo saveErr
				wb.SaveAs newName, FileFormat:=xlOpenXMLWorkbook
				On Error GoTo 0
				ChDir inDir
			End If

			If keepOpen = "no" Or (append = "yes" And _
				(saveAppendOnly = "yes" Or keepAppendOnly = "yes")) Then
				wb.Close
			End If
			wbOpen = False
			anyFiles = True

			file = Dir
		End If
	Wend

	' End.

	If Not anyFiles Then MsgBox "No table files found.", vbExclamation

	GoTo endSub

	noParametersErr:
	MsgBox """Parameters"" sheet not found.", vbCritical
	GoTo endSub

	inDirErr:
	MsgBox "Invalid input directory.", vbCritical
	GoTo endSub

	outDirErr:
	MsgBox "Invalid output directory.", vbCritical
	GoTo endSub

	saveAppendedErr:
	MsgBox "An error occurred while saving the appended .xlsx file """ + appendName + _
		""". Try closing all other Excel windows.", vbCritical
	noKill = True
	GoTo endSub

	openErr:
	MsgBox "An error occurred while opening table file """ + file + """. Try closing all other Excel windows.", vbCritical
	GoTo endSub

	saveErr:
	MsgBox "An error occurred while saving reformatted table file """ + file + """ as """ + newName + _
		""". Try closing all other Excel windows. If option append is set to yes, make sure the name of the appended " + _
		".xlsx file does not conflict with the name of a .csv file.", vbCritical
	GoTo endSub

	endSub:

	' Close the .csv file if it is still open because of an error.
	If wbOpen Then wb.Close

	If append = "yes" Then
		' If wbOpen = True, there's been an error, and the appended .xlsx file should be deleted.
		If anyFiles And Not wbOpen Then
			appendWb.Sheets("__temp").Delete
			appendWb.Sheets(1).Activate
			appendWb.Save
			If keepOpen = "no" Then appendWb.Close
		Else
			appendWb.Close
			If Not noKill Then
				ChDir outDir
				Kill appendName
			End If
		End If
	End If

	If append = "no" Or keepOpen = "no" Then
		Workbooks(startWb).Activate
		Sheets(startSh).Activate
	End If

	Application.ScreenUpdating = True
End Sub

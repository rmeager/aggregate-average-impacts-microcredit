Option Explicit
Option Private Module

' Prepare the sheet for printing.
Sub PreparePrint()
	' Give the sheet a landscape orientation, and scale it to a single page.
    Application.PrintCommunication = False
    With ActiveSheet.PageSetup
        .PrintTitleRows = ""
        .PrintTitleColumns = ""
    End With
    Application.PrintCommunication = True
    ActiveSheet.PageSetup.PrintArea = ""
    Application.PrintCommunication = False
    With ActiveSheet.PageSetup
        .LeftHeader = ""
        .CenterHeader = ""
        .RightHeader = ""
        .LeftFooter = ""
        .CenterFooter = ""
        .RightFooter = ""
        .LeftMargin = Application.InchesToPoints(0.75)
        .RightMargin = Application.InchesToPoints(0.75)
        .TopMargin = Application.InchesToPoints(1)
        .BottomMargin = Application.InchesToPoints(1)
        .HeaderMargin = Application.InchesToPoints(0.5)
        .FooterMargin = Application.InchesToPoints(0.5)
        .PrintHeadings = False
        .PrintGridlines = False
        .PrintComments = xlPrintNoComments
        .PrintQuality = 600
        .CenterHorizontally = False
        .CenterVertically = False
        .Orientation = xlLandscape
        .Draft = False
        .PaperSize = xlPaperLetter
        .FirstPageNumber = xlAutomatic
        .Order = xlDownThenOver
        .BlackAndWhite = False
        .Zoom = False
        .FitToPagesWide = 1
        .FitToPagesTall = 1
        .PrintErrors = xlPrintErrorsDisplayed
        .OddAndEvenPagesHeaderFooter = False
        .DifferentFirstPageHeaderFooter = False
        .ScaleWithDocHeaderFooter = True
        .AlignMarginsHeaderFooter = True
        .EvenPage.LeftHeader.Text = ""
        .EvenPage.CenterHeader.Text = ""
        .EvenPage.RightHeader.Text = ""
        .EvenPage.LeftFooter.Text = ""
        .EvenPage.CenterFooter.Text = ""
        .EvenPage.RightFooter.Text = ""
        .FirstPage.LeftHeader.Text = ""
        .FirstPage.CenterHeader.Text = ""
        .FirstPage.RightHeader.Text = ""
        .FirstPage.LeftFooter.Text = ""
        .FirstPage.CenterFooter.Text = ""
        .FirstPage.RightFooter.Text = ""
    End With
    Application.PrintCommunication = True
End Sub

Function FormatCsv() As Integer
	Const maxMerges As Integer = 100
	Const maxArray = 100

	Dim rv As Integer, nRows As Integer, nCols As Integer, nMerges As Integer, _
		space As Integer, pos As Integer, i As Integer, j As Integer, _
		k As Integer
	' "strA" for "string array"
	Dim value As String, format As String, code As String, subcode As String, _
		start As String, strA(1 To maxArray) As String, super As String
	Dim unknown As Boolean
	Dim thisR As Range, mergeR As Range, merges(1 To maxMerges) As Range
	Dim br As Border

	rv = 0

	' Sheet options
	ActiveSheet.Cells.Font.name = "Times New Roman"
	PreparePrint
	'Range("A1", Range("A1").Offset(nRows - 1, nCols - 1)).NumberFormat = "@"

	' Number of rows
	nRows = 2
	While V("A", nRows) <> ""
		nRows = nRows + 1
	Wend
	nRows = nRows - 1

	' Number of columns
	nCols = 2
	While Range("A1").Offset(0, nCols - 1).value <> ""
		nCols = nCols + 1
	Wend
	nCols = nCols - 1

	' Row heights
	For i = 2 To nRows
		Rows(LTrim$(Str$(i)) + ":" + LTrim$(Str$(i))).EntireRow.RowHeight = V("A", i)
	Next i

	' Column widths
	For i = 2 To nCols
		Range("A:A").Offset(0, i - 1).ColumnWidth = Range("A1").Offset(0, i - 1).value
	Next i

	' Remove row heights column and column widths row.
	Range("A:A").Delete shift:=xlLeft
	Rows("1:1").EntireRow.EntireRow.Delete shift:=xlUp
	nRows = nRows - 1
	nCols = nCols - 1

	' Apply formatting codes.
	nMerges = 0
	For i = 1 To nRows
		For j = 1 To nCols
			Set thisR = Range("A1").Offset(i - 1, j - 1)

			' Ad-hoc replacements that are Matt-specific.
			' Sorry for the big ol' kludge, couldn't think of a better quick
			' solution.
			thisR.value = Replace$(thisR.value, "ASCII_10", Chr(10))
			thisR.value = Replace$(thisR.value, "GREEK_ALPHA", ChrW(&H3B1))

			If Not thisR.MergeCells Then
				value = thisR.value
				format = LCase$(Mid$(value, 2, InStr(value, "}") - 2))
				value = Right$(value, Len(value) - InStr(value, "}"))
				thisR.value = value
				If thisR.value <> value Then
					thisR.Formula = "=""" + Replace$(value, """", """""") + """"
				End If

				While format <> ""
					space = InStr(format, " ")
					If space > 0 Then
						code = Left$(format, space - 1)
						format = Right$(format, Len(format) - space)
					Else
						code = format
						format = ""
					End If

					start = "Row" + Str$(i) + ", column" + Str$(j) + ": code """ + code + """: "
					' **********
					' Font size
					' **********
					If Left$(code, 4) = "size" Then
						code = Right$(code, Len(code) - 4)
						If Val(code) <= 0 Then
							MsgBox start + "invalid font size", vbCritical
							GoTo exErr
						End If
						thisR.Font.Size = Val(code)
					' **********
					' Bold, italics, underline
					' **********
					ElseIf code Like "bold*" Or code Like "italic*" Or _
						code Like "underline*" Then
						strA(1) = "bold"
						strA(2) = "italic"
						strA(3) = "underline"
						unknown = True
						k = 1
						While k <= 3 And unknown
							If Left$(code, Len(strA(k))) = strA(k) Then
								unknown = False
								subcode = Right$(code, Len(code) - Len(strA(k)))
								code = strA(k)
							End If

							k = k + 1
						Wend
						If unknown Then
							MsgBox start + "unknown code", vbCritical
							GoTo exErr
						End If

						Select Case code
							Case "bold"
								If subcode = "" Then
									thisR.Font.Bold = True
								Else
									thisR.Characters(1, Val(subcode)).Font.Bold = True
								End If
							Case "italic"
								If subcode = "" Then
									thisR.Font.Italic = True
								Else
									thisR.Characters(1, Val(subcode)).Font.Italic = True
								End If
							Case "underline"
								If subcode = "" Then
									thisR.Font.Underline = True
								Else
									thisR.Characters(1, Val(subcode)).Font.Underline = True
								End If
						End Select
					' **********
					' Format
					' **********
					ElseIf Left$(code, 4) = "form" Then
						If Left$(code, 6) = "format" Then
							code = Right$(code, Len(code) - 6)
						Else
							code = Right$(code, Len(code) - 4)
						End If
						thisR.NumberFormat = code
					' **********
					' Borders
					' **********
					ElseIf Left$(code, 2) = "br" Then
						code = Right$(code, Len(code) - 2)

						strA(1) = "top"
						strA(2) = "bottom"
						strA(3) = "left"
						strA(4) = "right"
						unknown = True
						k = 1
						While k <= 4 And unknown
							If Left$(code, Len(strA(k))) = strA(k) Then
								unknown = False
								subcode = Right$(code, Len(code) - Len(strA(k)))
								code = strA(k)
							End If

							k = k + 1
						Wend
						If unknown Then
							MsgBox start + "unknown border code", vbCritical
							GoTo exErr
						End If

						Select Case code
							Case "top"
								Set br = thisR.Borders(xlEdgeTop)
							Case "bottom"
								Set br = thisR.Borders(xlEdgeBottom)
							Case "left"
								Set br = thisR.Borders(xlEdgeLeft)
							Case "right"
								Set br = thisR.Borders(xlEdgeRight)
						End Select

						Select Case subcode
							' LineStyle
							Case "none"
								br.LineStyle = xlLineStyleNone
							Case "cont"
								br.LineStyle = xlContinuous
							Case "continuous"
								br.LineStyle = xlContinuous
							Case "double"
								br.LineStyle = xlDouble
							Case "dash"
								br.LineStyle = xlDash
							Case "dashdot"
								br.LineStyle = xlDashDot
							Case "dashdotdot"
								br.LineStyle = xlDashDotDot
							Case "dot"
								br.LineStyle = xlDot
							Case "slant"
								br.LineStyle = xlSlantDashDot
							Case "slantdashdot"
								br.LineStyle = xlSlantDashDot
							' Weight
							Case "hairline"
								br.Weight = xlHairline
								If br.LineStyle = xlLineStyleNone Then br.LineStyle = xlContinuous
							Case "thin"
								br.Weight = xlThin
								If br.LineStyle = xlLineStyleNone Then br.LineStyle = xlContinuous
							Case "medium"
								br.Weight = xlMedium
								If br.LineStyle = xlLineStyleNone Then br.LineStyle = xlContinuous
							Case "thick"
								br.Weight = xlThick
								If br.LineStyle = xlLineStyleNone Then br.LineStyle = xlContinuous
							' Default
							Case ""
								br.LineStyle = xlContinuous
								br.Weight = xlThin
							Case Else
								MsgBox start + "unknown border code", vbCritical
								GoTo exErr
						End Select
					' **********
					' Fill
					' **********
					ElseIf Left$(code, 4) = "fill" Then
						code = Right$(code, Len(code) - 4)
						Select Case code
							Case "green2"
								With thisR.Interior
									.Pattern = xlSolid
									.PatternColorIndex = xlAutomatic
									.ThemeColor = xlThemeColorAccent3
									.TintAndShade = 0.799981688894314
									.PatternTintAndShade = 0
								End With
							Case "green3"
								With thisR.Interior
									.Pattern = xlSolid
									.PatternColorIndex = xlAutomatic
									.ThemeColor = xlThemeColorAccent3
									.TintAndShade = 0.599993896298105
									.PatternTintAndShade = 0
								End With
							Case "green4"
								With thisR.Interior
									.Pattern = xlSolid
									.PatternColorIndex = xlAutomatic
									.ThemeColor = xlThemeColorAccent3
									.TintAndShade = 0.399975585192419
									.PatternTintAndShade = 0
								End With
							Case "red2"
								With thisR.Interior
									.Pattern = xlSolid
									.PatternColorIndex = xlAutomatic
									.ThemeColor = xlThemeColorAccent2
									.TintAndShade = 0.799981688894314
									.PatternTintAndShade = 0
								End With
							Case "red3"
								With thisR.Interior
									.Pattern = xlSolid
									.PatternColorIndex = xlAutomatic
									.ThemeColor = xlThemeColorAccent2
									.TintAndShade = 0.599993896298105
									.PatternTintAndShade = 0
								End With
							Case "red4"
								With thisR.Interior
									.Pattern = xlSolid
									.PatternColorIndex = xlAutomatic
									.ThemeColor = xlThemeColorAccent2
									.TintAndShade = 0.399975585192419
									.PatternTintAndShade = 0
								End With
							Case Else
								MsgBox start + "unknown color", vbCritical
								GoTo exErr
						End Select
					' **********
					' Merge
					' **********
					ElseIf Left$(code, 5) = "merge" Then
						nMerges = nMerges + 1
						If nMerges > maxMerges Then
							MsgBox "Too many merges.", vbCritical
							GoTo exErr
						End If

						code = Right$(code, Len(code) - 5)

						strA(1) = "right"
						strA(2) = "left"
						strA(3) = "down"
						strA(4) = "up"
						unknown = True
						k = 1
						While k <= 4 And unknown
							If Left$(code, Len(strA(k))) = strA(k) Then
								unknown = False
								subcode = Right$(code, Len(code) - Len(strA(k)))
								code = strA(k)
							End If

							k = k + 1
						Wend

						If unknown Then
							Set mergeR = Range(code)
						Else
							Select Case code
								Case "right"
									If subcode = "" Then
										Set mergeR = thisR.End(xlToRight)
									Else
										Set mergeR = thisR.Offset(0, Val(subcode))
									End If
								Case "left"
									If subcode = "" Then
										Set mergeR = thisR.End(xlToLeft)
									Else
										Set mergeR = thisR.Offset(0, -Val(subcode))
									End If
								Case "down"
									If subcode = "" Then
										Set mergeR = thisR.End(xlDown)
									Else
										Set mergeR = thisR.Offset(Val(subcode), 0)
									End If
								Case "up"
									If subcode = "" Then
										Set mergeR = thisR.End(xlUp)
									Else
										Set mergeR = thisR.Offset(-Val(subcode), 0)
									End If
							End Select
						End If

						Set merges(nMerges) = Range(thisR, mergeR)
					' **********
					' Other
					' **********
					Else
						Select Case code
							Case "left"
								thisR.HorizontalAlignment = xlLeft
							Case "center"
								thisR.HorizontalAlignment = xlCenter
							Case "right"
								thisR.HorizontalAlignment = xlRight
							Case "justify"
								thisR.HorizontalAlignment = xlJustify
							Case "bottom"
								thisR.VerticalAlignment = xlBottom
							Case "vcenter"
								thisR.VerticalAlignment = xlCenter
							Case "top"
								thisR.VerticalAlignment = xlTop
							Case "wrap"
								thisR.WrapText = True
							Case "autofitrow"
								Rows(LTrim$(Str$(thisR.row)) + ":" + LTrim$(Str$(thisR.row))).EntireRow.AutoFit
							Case "freeze"
								thisR.Select
								ActiveWindow.FreezePanes = True
							Case "freezefirstrow"
								With ActiveWindow
									.SplitColumn = 0
									.SplitRow = 1
								End With
								ActiveWindow.FreezePanes = True
							Case "freezefirstcol"
								With ActiveWindow
									.SplitColumn = 1
									.SplitRow = 0
								End With
								ActiveWindow.FreezePanes = True
							Case "unfreeze"
								ActiveWindow.FreezePanes = False
							Case "grid"
								ActiveWindow.DisplayGridlines = True
							Case "nogrid"
								ActiveWindow.DisplayGridlines = False
							Case Else
								MsgBox start + "unknown code", vbCritical
								GoTo exErr
						End Select
					End If
				Wend
			End If

			' Compartamos-specific
			' Adjustment stars
			super = "_SUPERSCRIPT_"
			strA(1) = "AAA"
			strA(2) = "AA"
			strA(3) = "A"
			k = 1
			Do
				pos = InStr(thisR.Value, super + strA(k))
				If pos > 0 Then
					thisR.Value = Replace(thisR.Value, _
						super + strA(k), strA(k), 1, 1)
					With thisR.Characters(pos, Len(strA(k)))
						.Font.Superscript = True
					End With
				End If

				k = k + 1
			Loop While k <= 3 And pos = 0
			' Notes on adjustment stars
			If thisR.Value = "A q<0.10, AA q<0.05, AAA q<.01 (adjusting critical values following the approach by Benjamini & Hochberg)" Then
				pos = 0
				Do
					pos = InStr(pos + 1, thisR.Value, "A")
					If pos > 0 Then
						thisR.Characters(pos, 1).Font.Superscript = True
					End If
				Loop While pos > 0
			End If
		Next j
	Next i

	' Merge.
	For i = 1 To nMerges
		merges(i).Merge

		' Autofit the merged range. Code adapted from
		' https://groups.google.com/forum/#!topic/microsoft.public.excel.misc/IpOsTmNzxNM
		If merges(i).WrapText Then
			Dim cWdth As Double, MrgeWdth As Double, NewRwHt As Double
			Dim c As Range, ma As Range, cc As Range

			Set c = merges(i).Cells(1, 1)
			cWdth = c.ColumnWidth
			Set ma = c.MergeArea
			For Each cc In ma.Cells
				MrgeWdth = MrgeWdth + cc.ColumnWidth + 1
			Next
			ma.MergeCells = False
			c.ColumnWidth = MrgeWdth
			c.EntireRow.AutoFit
			NewRwHt = c.RowHeight
			c.ColumnWidth = cWdth
			ma.MergeCells = True
			ma.RowHeight = NewRwHt
			cWdth = 0: MrgeWdth = 0
		End If
	Next i

	GoTo endSub

	' "exErr" for "exit error"
	exErr:
	rv = -1

	endSub:
	FormatCsv = rv
End Function

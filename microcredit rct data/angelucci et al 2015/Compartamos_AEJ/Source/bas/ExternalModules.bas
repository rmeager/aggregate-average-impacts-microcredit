' To use Compartamos Subs in Excel, paste this code into an .xlsm file after
' modifying DIR_COMPRADO.

Option Explicit

Const DIR_COMPRADO = "...\Source\"

Sub RemoveExternalModules()
	Dim i As Integer

	On Error Resume Next

	With ThisWorkbook.VBProject.VBComponents
		For i = 2 To 10
			.Remove .Item("Module" + LTrim(Str(i)))
		Next
	End With

	On Error GoTo 0
End Sub

Sub AddExternalModules()
	RemoveExternalModules

	With ThisWorkbook.VBProject.VBComponents
		.Import Filename:=DIR_COMPRADO + "bas\Range.bas"
		' FormatTables
		.Import Filename:=DIR_COMPRADO + "bas\FormatTables\FormatTables.bas"
		.Import Filename:=DIR_COMPRADO + "bas\FormatTables\FormatCsv.bas"
	End With
End Sub

' "Ext" for "external"
Sub ExtFormatTables()
	AddExternalModules
	Application.Run "FormatTables"
End Sub

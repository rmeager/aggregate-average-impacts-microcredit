* Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
* Syntax: matatypes
* Defines the type macros used in Mata.
pr matatypes
	* Standard types
	c_local RS	real scalar
	c_local RR	real rowvector
	c_local RC	real colvector
	c_local RM	real matrix
	c_local SS	string scalar
	c_local SR	string rowvector
	c_local SC	string colvector
	c_local SM	string matrix

	* Sample types

	loc SampCode		real
	c_local SampCode	`SampCode'
	c_local SampCodeS	`SampCode' scalar
	c_local SampCodeR	`SampCode' scalar

	loc CompSample			compartamos_sample
	c_local CompSample		`CompSample'
	c_local CompSampleS		class `CompSample' scalar
	c_local CompSampleR		class `CompSample' rowvector
end

* Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
pr dschar, rclass
	vers 9

	syntax anything, [Regex]

	gettoken char anything : anything
	cap conf name `char'
	if _rc | `:list sizeof char' > 1 {
		di as err "invalid characteristic name"
		ex 198
	}

	gettoken pattern anything : anything

	if `:length loc anything' ///
		err 198

	* Implement -regex-.
	loc fcn = cond("`regex'" != "", "regex", "strmatch")

	foreach var of varl _all {
		mata: st_local("match", strofreal( ///
			`fcn'(st_global("`var'[`char']"), st_local("pattern"))))
		if `match' ///
			loc varlist : list varlist | var
	}

	loc indent	0
	loc pad		2
	DisplayInCols txt `indent' `pad' 0 `varlist'

	ret loc varlist `varlist'
end

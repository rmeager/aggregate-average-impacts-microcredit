* Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
* Syntax: unknown_spec spec
* Issues an error message that spec is an unknown specification name,
* exiting with return code 9.
pr unknown_spec
	vers 9

	di as err "unknown specification name `0'"
	ex 9
end

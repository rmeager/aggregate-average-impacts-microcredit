/*
Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
Syntax: stars x, p(p)
Adds the number of signficance stars to x that is appropriate given the value of
p. -stars- returns r(stars), the stars themselves, as well as r(starred), x
formatted as %24.3f with the stars appended to it.
*/
pr stars, rclass
	vers 9

	syntax anything(name=x id=x), p(real)

	* Check `x'.
	conf n `x'

	* Check -p()-.
	assert inrange(`p', 0, 1)

	foreach threshold of numlist .1 .05 .01 {
		if `p' < `threshold' ///
			loc stars `stars'*
	}

	ret loc stars `stars'
	ret loc starred = strofreal(`x', "%24.3f") + "`stars'"
end

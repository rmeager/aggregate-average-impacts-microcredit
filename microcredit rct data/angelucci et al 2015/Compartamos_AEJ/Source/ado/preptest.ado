/*
Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
Syntax: preptest
Prepares program testing by dropping Stata and Mata objects and setting system
parameters. Adopted from -cscript-. -preptest- should be run before each set of
tests so that one set does not interact with another.
*/
pr preptest
	vers 9

	* Stata objects
	drop _all
	lab drop _all
	sca drop _all
	mat drop _all
	constraint drop _all
	discard

	* Drop Mata variables but not functions.
	mata: st_local("matavars", invtokens(direxternal("*")'))
	foreach var of loc matavars {
		mata: mata drop `var'
	}

	* System parameters
	set type float
	set linesize 80
end

/*
Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
Syntax: missgen [new variable] = [numeric expression], miss([numeric variable])
-missgen- creates a new variable. If the variable specified to option -miss()-
is sysmiss, the new variable will be sysmiss. If it is extended missing, the new
variable will be .a. If it is nonmissing, the new variable will equal the
numeric expression. For example:

sysuse auto, clear
replace foreign = .  in 1
replace foreign = .b in 2
missgen heavy = weight >= 4000, miss(foreign)
*/
pr missgen
	vers 9

	syntax newvarname =/exp, miss(varname num)

	qui gen `typlist' `varlist' = ///
		cond(`miss' == ., ., ///
		cond(`miss' > ., .a, ///
		`exp'))
end

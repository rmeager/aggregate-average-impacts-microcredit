vers 10.1

loc RS	real scalar
loc RR	real rowvector
loc SS	string scalar
loc SR	string rowvector

mata:

void select_list(`SS' _list, `SS' _select)
{
	`RS' n, i
	`RR' select
	`SR' list
	transmorphic t

	select = strtoreal(tokens(st_local(_select)))
	assert(all(select :== 0 :| select :== 1))

	t = tokeninit()
	tokenset(t, st_local(_list))
	list = select(tokengetall(t), select)

	n = length(list)
	for (i = 1; i <= n; i++) {
		if (!strpos(list[i], `"""'))
			list[i] = "`" + `"""' + list[i] + `"""' + "'"
	}

	st_local(_list, invtokens(list))
}

end

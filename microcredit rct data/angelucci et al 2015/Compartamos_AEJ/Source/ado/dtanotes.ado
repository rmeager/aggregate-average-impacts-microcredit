* Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
* Purpose: Add dataset notes.
pr dtanotes
	vers 10.1

	syntax, creator(str)

	lab data "See notes."

	loc creator "`"`creator'"'"
	loc creator : list clean creator
	note: Dataset created by `creator'.
	loc date : di %td date(c(current_date), "DMY")
	note: Dataset created on `date' at `c(current_time)'.
	note: Dataset created on computer `:environment computername' ///
		by user `c(username)'.

	note: Note removed.
	note: Note removed.

	* Drop -egen anycount()- notes.
	qui ds, has(char note1)
	foreach var in `r(varlist)' {
		loc note : char `var'[note1]
		assert `:length loc note' <= 244
		if regexm("`note'", "^(.*) == 1") {
			cap conf var `=regexs(1)', exact
			if !_rc ///
				qui note drop `var' in 1
		}
	}

	note
end

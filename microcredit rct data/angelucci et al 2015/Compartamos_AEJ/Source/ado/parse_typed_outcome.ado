/*
Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
Syntax: parse_typed_outcome typed_outcome
Parses a typed outcome into three parts:
s(depvar)		the dependent variable
s(types)		all regression types as a single prefix
s(type1)		the first regression type: x or ""
s(type2)		the second regression type: bo, lg, or ""
See "Set category properties.do" for the definition of a typed outcome.
*/
pr parse_typed_outcome, sclass
	vers 9

	syntax anything(name=typed id="typed outcome")

	assert `:list sizeof typed' == 1

	* Regression type 1
	if regexm("`typed'", "^(x)_") {
		loc type1 = regexs(1)
		loc types `type1'_
		loc typed = subinstr("`typed'", regexs(0), "", 1)
	}

	* Regression type 2
	if regexm("`typed'", "^(bo|lg)_") {
		loc type2 = regexs(1)
		loc types `types'`type2'_
		loc typed = subinstr("`typed'", regexs(0), "", 1)
	}

	conf name `typed'

	sret loc depvar `typed'
	sret loc types  `types'
	sret loc type1  `type1'
	sret loc type2  `type2'
end

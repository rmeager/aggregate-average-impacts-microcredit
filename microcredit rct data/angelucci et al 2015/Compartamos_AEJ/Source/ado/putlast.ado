* Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
* Syntax: putlast name =exp
* Set the value of variable name in the last observation to that specified by
* =exp, creating name if necessary.
pr putlast
	vers 9

	gettoken name exp : 0, p("=")

	assert `:list sizeof name' == 1
	conf name `name'
	cap conf var `name', exact
	loc generate = cond(_rc, "generate", "replace")
	`generate' `name' `exp' in L
end

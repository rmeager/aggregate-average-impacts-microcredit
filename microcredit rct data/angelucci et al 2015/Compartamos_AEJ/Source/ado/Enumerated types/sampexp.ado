/*
Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
Syntax: sampexp sample1 [sample2 [... ]]]
Returns in r(exp) the -if- qualifier expression associated with the specified
sample codes. If multiple samples are specified, -sampexp- returns an
expression indicating their intersection.
*/
pr sampexp, rclass
	vers 10.1

	enumtypes

	cap numlist `"`0'"'
	if _rc {
		di as err "invalid sample"
		ex 198
	}
	loc samples `r(numlist)'

	* In general, -sampexp- requires Stata 11.2+, but for replication purposes,
	* important samples will be hard-coded for access in earlier versions.
	if c(stata_version) < 11.2 {
		assert "`samples'" == "`SampleEndline'"
		loc exp (survey == "Endline")
	}
	else {
		foreach sample of loc samples {
			if `:length loc exp' ///
				mata: st_local("exp", st_local("exp") + " & ")
			mata: st_local("exp", st_local("exp") + sampexp(`sample'))
		}
	}

	if `:list sizeof samples' > 1 {
		mata: st_local("exp", sprintf("(%s)", st_local("exp")))
	}

	ret loc exp "`macval(exp)'"
end

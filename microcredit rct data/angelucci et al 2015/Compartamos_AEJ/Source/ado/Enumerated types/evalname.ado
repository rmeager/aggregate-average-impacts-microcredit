* Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
* Syntax: evalname enum int
* Returns in r(name) the name of the enumerated value associated with enumerated
* type enum and integer int.
pr evalname
	vers 9

	args enum int
	assert `:list sizeof enum' == 1
	conf name `enum'
	conf integer n `int'
	assert !`:length loc 3'

	enumtypes
	mata: st_global("r(name)", evalname("`enum'", `int'))
end

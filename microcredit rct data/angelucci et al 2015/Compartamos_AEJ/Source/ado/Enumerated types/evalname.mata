* Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org

vers 9

loc RS	real scalar
loc SS	string scalar
loc SC	string colvector
loc SM	string matrix

mata:

`SS' evalname(`SS' _enum, `RS' _int)
{
	`SC' name
	`SM' map

	map = enumtype(_enum)
	name = select(map[,1], map[,2] :== strofreal(_int))
	if (!length(name)) {
		_error(sprintf("value %s not found", strofreal(_int)))
	}

	return(name)
}

end

* Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org

vers 9

matatypes

mata:

// Returns the map of names to positive integers for enumerated type name.
`SM' enumtype(`SS' name)
{
	// "posint" for "positive integer"
	`RS' n, nonmiss, posint, unique, i
	`RC' numvals
	`SM' map

	stata("cap conf name " + name)
	if (c("rc") | length(tokens(name)) > 1) {
		_error(sprintf("%s invalid name", name))
	}

	map = st_dir("local", "macro", name + "*")
	n = length(map)
	if (!n) {
		_error(sprintf("enumerated type %s not found", name))
	}
	map = subinstr(map, name, "", 1)
	if (any(map :== "")) {
		_error(sprintf("invalid enumerated type local \`%s'", name))
	}

	map = map, J(n, 1, "")
	for (i = 1; i <= n; i++) {
		map[i, 2] = st_local(name + map[i, 1])
	}

	numvals = strtoreal(map[,2])
	nonmiss = !any(numvals :>= .)
	posint = all(numvals :> 0 :& numvals :== floor(numvals))
	unique = length(uniqrows(numvals)) == n
	if (!nonmiss)
		_error("invalid enumerated type: missing values not allowed")
	if (!posint)
		_error("invalid enumerated type: values must be positive integers")
	if (!unique)
		_error("invalid enumerated type: duplicate integers found")

	return(map)
}

end

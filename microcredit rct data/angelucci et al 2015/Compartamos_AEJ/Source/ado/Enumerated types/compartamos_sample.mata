* Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org

vers 11.2

matatypes
enumtypes

mata:


/* -------------------------------------------------------------------------- */
					/* class definition		*/

class `CompSample' {
	public:
		// getters and setters
		`SS' exp()
		`SampCodeS' code()
		pointer(`CompSampleS') scalar parent(), bq()
		pointer(`CompSampleS') rowvector parents()
		void set_code(), set_parents(), set_bq(), set_exp(), set()

		`RS' is_subsample()
		`SS' name()

	private:
		`SS' exp
		`SampCodeS' code
		// For endline samples, the baseline equivalent sample.
		// NULL if the sample is not endline or if there is no baseline
		// equivalent.
		pointer(`CompSampleS') scalar bq
		pointer(`CompSampleS') rowvector parents

		`SS' _exp()
}

`SampCodeS' `CompSample'::code()
{
	// Check that code has been defined.
	(void) evalname("Sample", code)

	return(code)
}

void `CompSample'::set_code(`SampCodeS' newcode)
{
	// Check that newcode is a valid `SampCodeS'.
	(void) evalname("Sample", newcode)
	code = newcode
}

pointer(`CompSampleS') rowvector `CompSample'::parents()
	return(parents)

pointer(`CompSampleS') scalar `CompSample'::parent(`RS' i)
	return(parents[i])

void `CompSample'::set_parents(pointer(`CompSampleS') rowvector newparents)
{
	`RS' n, i

	// Check newparents.
	n = length(newparents)
	for (i = 1; i <= n; i++)
		(void) newparents[i]->code()

	parents = newparents
}

pointer(`CompSampleS') scalar `CompSample'::bq()
	return(bq)

void `CompSample'::set_bq(pointer(`CompSampleS') scalar newbq)
{
	if (newbq != NULL) {
		if (!is_subsample(`SampleEndline'))
			_error("sample is not an endline sample")
		if (!newbq->is_subsample(`SampleBaseline'))
			_error("bq specified is not a baseline sample")
	}

	bq = newbq
}

`SS' `CompSample'::_exp(`RS' paren)
{
	`RS' n, i
	`SS' fullexp

	fullexp = sprintf("(%s)", exp)

	n = length(parents)
	if (!n)
		return(fullexp)
	else {
		for (i = n; i >= 1; i--) {
			/* parents[i]->_exp(0) rather than _exp(1) because _exp(0) will
			return either "(parent_exp)", if parents[i] has no parent, or
			"grandparent_exp & (parent_exp)", if it does, in which case an
			additional pair of parentheses enclosing the entire expression is
			not necessary, as the only top-level operator is &. _exp(1) would
			return "(grandparent_exp & (parent_exp))" with the extra
			parentheses. */
			fullexp = sprintf("%s & %s", parents[i]->_exp(0), fullexp)
		}
		if (paren)
			fullexp = sprintf("(%s)", fullexp)
		return(fullexp)
	}
	/*NOTREACHED*/
}

`SS' `CompSample'::exp()
	return(_exp(1))

void `CompSample'::set_exp(`SS' newexp)
	exp = newexp

void `CompSample'::set(`SampCodeS' newcode,
	pointer(`CompSampleS') rowvector newparents,
	pointer(`CompSampleS') scalar newbq, `SS' newexp)
{
	set_code(newcode)
	set_parents(newparents)
	set_bq(newbq)
	set_exp(newexp)
}

// Returns 1 if the sample is a subsample of supersamp and 0 if not.
`RS' `CompSample'::is_subsample(`SampCodeS' supersamp)
{
	`RS' n, i

	if (code == supersamp)
		return(1)
	else {
		n = length(parents)
		for (i = 1; i <= n; i++) {
			if (parents[i]->is_subsample(supersamp))
				return(1)
		}

		return(0)
	}
	/*NOTREACHED*/
}

`SS' `CompSample'::name()
	return(evalname("Sample", code))


/* -------------------------------------------------------------------------- */
					/* sample definitions	*/

// Functions that return predefined instances of `CompSample'

// Returns a `CompSample' rowvector of all the instances of `CompSample' used in
// Compartamos.
`CompSampleR' samples()
{
	`RS' n, i
	`SR' expected_names, names
	`CompSampleR' samps
	pointer(`CompSampleS') scalar baseend, baseline, endline, bqmarried, bqchild
	pointer(`CompSampleS') rowvector nopar

	expected_names = "BQChild6To17", "BQMaybeMarried", "BaseEnd", "Baseline",
		"BizEver", "Child4To17", "EconActivity", "Endline", "MaybeMarried",
		"MaybeOtherAdult", "Panel", "Phone", "Targeted"

	names = sort(enumtype("Sample")[,1], 1)'
	if (names != expected_names) {
		names
		_error("unknown sample name")
	}

	n = length(expected_names)
	samps = `CompSample'(n)
	i = 1

	// `SampleBaseEnd'
	nopar = J(1, 0, NULL)
	samps[i].set(`SampleBaseEnd', nopar, NULL,
		`"inlist(survey, "Baseline only", "Endline")"')
	baseend = &samps[i++]
	// `SampleBaseline'
	samps[i].set(`SampleBaseline', baseend, NULL,
		`"survey == "Baseline only" | InPanel"')
	baseline = &samps[i++]
	// `SampleEndline'
	samps[i].set(`SampleEndline', baseend, baseline, `"survey == "Endline""')
	endline = &samps[i++]
	// `SamplePanel'
	samps[i++].set(`SamplePanel', (endline, baseline), NULL, "1")
	// `SampleTargeted': baseline sample targeted for endline surveying.
	samps[i++].set(`SampleTargeted', baseline, NULL, "!mi(attrited)")
	// `SamplePhone'
	samps[i++].set(`SamplePhone', nopar, NULL, `"survey == "Telephone""')

	// Baseline samples
	// "Maybe" because those for whom BQ1_2 is nonresponse are included in this
	// sample (7 observations).
	samps[i].set(`SampleBQMaybeMarried', baseline, NULL,
		"inlist(BQ1_2, 1, 2) | BQ1_2 > 6")
	bqmarried = &samps[i++]
	samps[i].set(`SampleBQChild6To17', baseline, NULL, "BQ7_1 > 0 & !mi(BQ7_1)")
	bqchild = &samps[i++]

	// Endline samples
	// Q12_2 has no baseline equivalent.
	samps[i++].set(`SampleEconActivity', endline, NULL, "Q12_2 == 1")
	// "Maybe" because those for whom Q1_2 is missing are included in this
	// sample (2 observations).
	samps[i++].set(`SampleMaybeMarried', endline, bqmarried,
		"inlist(Q1_2, 1, 2) | mi(Q1_2)")
	// "Maybe" because those for whom Q2_1 == . are included in this sample (5
	// observations).
	samps[i++].set(`SampleMaybeOtherAdult', endline, NULL,
		"!(Q2_1 == 1 | Q2_1 == 1 + Q5_1 + Q5_2)")
	samps[i++].set(`SampleChild4To17', endline, bqchild, "Q5_2 > 0")
	samps[i++].set(`SampleBizEver', endline, NULL, "Q11_1 == 1")

	if (i != n + 1)
		_error("too few samples defined")

	for (i = 1; i <= n; i++) {
		names[i] = samps[i].name()
	}
	names = sort(names', 1)'
	if (names != expected_names) {
		// This could be sample duplication or something else.
		_error("error in sample definition")
	}

	return(samps)
}

// Returns a pointer to the `CompSample' instance corresponding to code.
pointer(`CompSampleS') scalar get_sample(`SampCodeS' code)
{
	`RS' n, i
	`CompSampleR' samps

	// Check that code is a valid `SampCodeS'.
	(void) evalname("Sample", code)

	samps = samples()
	n = length(samps)
	for (i = 1; i <= n; i++) {
		if (samps[i].code() == code)
			return(&samps[i])
	}

	errprintf("sample code %s not found", evalname("Sample", code))
}


/* -------------------------------------------------------------------------- */
					/* Stata getters		*/

// Functions used to convert a `SampCodeS' to a `CompSample' member

// Returns the result of exp() for the sample associated with code _code.
`SS' sampexp(`RS' _code)
{
	pointer(`CompSampleS') scalar samp

	samp = get_sample(_code)
	return(samp->exp())
}

// For the sample associated with code _code, returns "" if bq is NULL;
// otherwise it returns the code associated with bq.
`SS' sampbq(`SampCodeS' _code)
{
	pointer(`CompSampleS') scalar samp, bq

	samp = get_sample(_code)
	bq = samp->bq()
	if (bq == NULL)
		return("")
	else
		return(strofreal(bq->code()))
	/*NOTREACHED*/
}


/* -------------------------------------------------------------------------- */
					/* interface with Stata		*/

// Other functions used in Stata ado-files

// Returns 1 if the sample associated with the code _code is a subsample of the
// sample associated with _supersamp; otherwise, it returns 0.
`RS' is_subsample(`SampCodeS' _code, `SampCodeS' _supersamp)
{
	pointer(`CompSampleS') scalar samp

	if (_code == _supersamp)
		return(1)

	samp = get_sample(_code)
	return(samp->is_subsample(_supersamp))
}

end

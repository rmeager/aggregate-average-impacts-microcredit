* Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
* Syntax: enumtypes
* Defines the enumerated types.
pr enumtypes
	syntax, [trace]

	if "`trace'" == "" {
		loc setting = c(trace)
		set trace off
	}

	* See <http://www.stata.com/statalist/archive/2003-12/msg00385.html> on
	* -c_local-.

	* Papers
	c_local PaperWorking	1
	c_local PaperAEJ		2

	* The list of outcome categories, each of which corresponds to a table
	* "Cat" prefix for "category"
	* Working paper
	c_local CatOldCredit			1
	c_local CatOldBusiness			2
	c_local CatOldConsumption		3
	c_local CatOldIncome			4
	c_local CatOldWelfare			5
	* AEJ
	c_local CatCredit				6
	c_local CatCreditChimera		14
	c_local CatCreditAmount			12
	c_local CatSelfEmploy			7
	c_local CatIncome				8
	c_local CatLabor				9
	c_local CatConsumption			10
	c_local CatConsumptionChimera	16
	c_local CatSocial				11
	c_local CatSocialChimera		15
	c_local CatWelfare				13
	c_local CatWelfareChimera		17

	* Regression samples
	c_local SampleEndline			1
	c_local SamplePanel				2
	c_local SampleTargeted			3
	c_local SampleEconActivity		4
	c_local SamplePhone				5
	c_local SampleMaybeMarried		6
	c_local SampleMaybeOtherAdult	7
	c_local SampleChild4To17		8
	c_local SampleBQMaybeMarried	9
	c_local SampleBQChild6To17		10
	c_local SampleBizEver			11
	c_local SampleBaseline			12
	c_local SampleBaseEnd			13

	* Calculation types
	c_local CalcRaw				1
	c_local CalcSum				2
	c_local CalcOther			3
	//c_local CalcLogic			4
	//c_local CalcDotProduct	5
	c_local CalcDiff			6
	c_local CalcRescale			7
	c_local CalcQuotient		8
	c_local	CalcNot				9
	c_local CalcUnion			10
	c_local CalcGT				11
	c_local CalcIndex			12
	c_local CalcMatch			13
	c_local CalcProduct			14
	c_local CalcToMiss			15
	c_local CalcFromMiss		16
	c_local CalcMissToMiss		17
	c_local CalcCond			18
	c_local CalcIsMiss			19
	c_local CalcCopy			20
	c_local CalcIntersection	21
	c_local CalcLTE				22
	c_local CalcGTE				23
	c_local CalcLT				24

	* General type of the treatment variable set: AIT or HTE.
	c_local TreatTypeAIT	1
	c_local TreatTypeHTE	2

	* For HTEs, the specific type of the treatment variable set: Treatment + TX,
	* Treatment + TX_No, or TX + TX_No.
	c_local TXTypeTreatTX		1
	c_local TXTypeTreatTXNo		2
	c_local TXTypeTXTXNo		3

	* Type of an individual treatment variable: Treatment, TX, or TX_No.
	c_local TreatVarTypeTreat	1
	c_local TreatVarTypeTX		2
	c_local TreatVarTypeTXNo	3

	if "`trace'" == "" ///
		set trace `setting'
end

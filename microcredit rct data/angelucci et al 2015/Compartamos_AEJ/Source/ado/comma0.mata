* Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org

vers 9

mata:

// Syntax: comma0()
// Prefixes `0' with a comma.
void comma0()
	st_local("0", ", " + st_local("0"))

end

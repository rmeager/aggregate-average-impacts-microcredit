/*
Authors:
	Andrew Hillis, Innovations for Poverty Action
	Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
Purpose: Define a number of locals that store the properties of the outcome
category, which when combined with the specification properties determine what
analysis is completed and what output is produced. The locals are defined based
on the specification name, specified to option -spec()-, and the outcome
category, specified to -category()-. The following locals are defined:

`serror'	Cluster variable
`families'	List of outcome families for the p-value adjustments

Outcome groups
--------------
Each outcome category is associated with a number of outcome groups.
Every outcome group is identified by an integer between 1 and `maxgroups'
(defined below), represented by # below.

`g#'	"Typed outcomes" of outcome group #, where a typed outcome has two
		parts, regression types and dependent variable. Regression types are
		indicated by prefixes on the dependent variable name, and they are
		located in up to two slots. Thus, a typed outcome name has the following
		format:

		typed outcome: (type slot 1: "[a-z]_")(type slot 2: "[a-z]+_")(depvar)

		In other words, a typed outcome name is a dependent variable name with
		up to two prefixes, each of which indicates something about the
		regression. Regression types are optional: many typed outcome names are
		simply variable names. Slots are also independent: slot 1 may appear
		without 2, and vice versa. Below is a list of possible types for each
		slot:

		Slot 1
		------
		One lowercase letter followed by an underscore

		x	Exclude the dependent variable from analysis.

		Former types in slot 1
		----------------------
		q	Quantile regression for quantiles .05(.05).95
		z	Quantile regression for the quantile .95
		a	Quantile regression for the quantile .05
		m	Use -mlogit-.

		Slot 2
		------
		Two lowercase letters followed by an underscore

		bo	Restrict the sample to business owners.
		lg	Take the log of the dependent variable.

		Examples
		--------
		Q10_any					Run an OLS regression of variable Q10_any
		q_Q13_totalincome		Run a quantile regression of variable
								Q13_totalincome for the quantile .05.
		q_bo_Q10_4_tot_helpers	Run a quantile regression of variable
								Q10_4_tot_helpers for the quantile .05, with
								non-business owners excluded from the sample.

		Unless otherwise specified, an outcome is assumed to be untyped, i.e.,
		to have no regression type prefixes.

`business#'			List of dependent variables in `g#' for which panels B and C
					should be restricted to business owners
`tg#'				Table title of outcome group #
`mg#'				Specification of -esttab, mgroups()- for outcome group #
`outcome_notes#'	Notes to add to the table about outcome group #

Each local or set of locals is saved back into the parent do-file as a local.
The names of the locals in the parent do-file are specified as options. */

/*
Syntax
------

{do|run} "Set category properties", spec() category() sampcode() [options]

Options
-------

Main
----
spec()				Specification name
category()			Outcome category
sampcode()			Regression sample code

Local macro names
-----------------
clustvar()			Local macro name of `serror'
adj_families()		Local macro name of `families'
groups()			Local macro name of the full list of `g*'
business()			Local macro name of the full list of `business*'
titles()			Local macro name of the full list of `tg*'
mgroups()			Local macro name of the full list of `mg*'
outcome_notes()		Local macro name of the full list of `outcome_notes*'
chimeras()			Local macro name of the full list of `chimera*'
*/

mata: st_local("0", ", " + st_local("0"))
loc lclname name local
#d ;
syntax,
	/* inputs */
	spec(name) CAtegory(integer) sampcode(integer)
	/* outputs */
	[clustvar(`lclname') adj_families(`lclname')]
		/* outcome groups */
	[groups(`lclname') chimeras(`lclname') business(`lclname')
	titles(`lclname') mgroups(`lclname') outcome_notes(`lclname')]
;
#d cr

enumtypes

di "`spec'"
di `category'

* Check -category()-.
evalname Cat `category'
di "`r(name)'"

* Check -sampcode()-.
sampexp `sampcode'
* Necessary check for the definition of `Pan' below
assert inlist(`sampcode', `SampleEndline', `SamplePanel')

* Set default values for some of the locals.
loc serror cluster

* AEJ
if `category' == `CatCredit' {
	loc g1 in_admin Q21_3_anymfi Q21_3_comp Q21_3_othmfi Q21_3_bank ///
		Q21_3_othformal Q21_3_informal2 Q21_3_oth Q21_anyloan ///
		A_ever_late_not_cond Q20_2
	loc families Q21_3_anymfi Q21_3_comp Q21_3_othmfi Q21_3_bank ///
		Q21_3_othformal Q21_3_informal2 Q21_3_oth Q21_anyloan Q20_2
	loc tg1 DO NOT USE THIS OUTCOME CATEGORY FOR A TABLE: USE CHIMERA.
	loc outcome_notes1 DO NOT USE THIS TABLE: USE CHIMERA.
}
else if `category' == `CatCreditChimera' {
	loc g1 Q21_3_anymfi in_admin Q21_3_comp Q21_3_othmfi Q21_3_bank ///
		Q21_3_othformal Q21_3_informal2 Q21_3_oth Q21_anyloan ///
		A_ever_late_not_cond
	loc chimera1 `CatCredit' `CatCredit' `CatCredit' `CatCredit' `CatCredit' ///
		`CatCredit' `CatCredit' `CatCredit' `CatCredit' `CatCredit'
	loc tg1 Table 2a: Credit Access
	#d ;
	loc outcome_notes1 The dependent variables in Columns 2 and 10 are from
		administrative data and refer to all the respondent's loans from
		Compartamos from April 2009 to February 2012. Columns 1 and 3-9 are
		self-reported and refer to the 3 most recent loans of the last 2 years,
		first among the respondent's loans and then within the household.
		Column 7 refers to loans from money lenders, pawnshops, relatives, and
		friends. Column 8 includes merchandise not paid for in the amount of
		purchase and loans from employers and other sources. The adjusted
		critical values were calculated by treating Columns 1 and 3-9 of this
		table and Column 7 of Table 7 as an outcome family.
	;
	#d cr
}
else if `category' == `CatCreditAmount' {
	loc g1 Q21_5_anymfi Q21_5_comp Q21_5_othmfi Q21_5_bank Q21_5_othformal ///
		Q21_5_informal2 Q21_5_oth Q21_5_all
	loc families Q21_5_anymfi Q21_5_comp Q21_5_othmfi Q21_5_bank ///
		Q21_5_othformal Q21_5_informal2 Q21_5_oth Q21_5_all
	loc tg1 Table 2b: Loan Amounts
	#d ;
	loc outcome_notes1 All columns refer to the 3 most recent loans of the last
		2 years, first among the respondent's loans and then within the
		household. Column 6 refers to loans from money lenders, pawnshops,
		relatives, and friends. Column 7 includes merchandise not paid for in
		the amount of purchase and loans from employers and other sources. The
		adjusted critical values were calculated by treating all outcomes in the
		table as one outcome family.
	;
	#d cr
}
else if `category' == `CatSelfEmploy' {
	loc g1 Q10_9_totrev Q10_8_totexp Q10_9_toprof Q10_any Q10_1 bizstart12mo ///
		bizclose
	#d ;
	loc families "
		"Q10_9_totrev Q10_8_totexp Q10_9_toprof"
		"Q10_any Q10_1"
		"bizstart12mo bizclose"
	";
	#d cr
	loc business1 Q10_9_totrev Q10_8_totexp Q10_9_toprof
	loc tg1 Table 3: Self-Employment Activities
	#d ;
	loc outcome_notes1 Business profits (Column 3) are calculated by subtracting
		responses for expenses from responses for revenues of the businesses.
		The adjusted critical values were calculated by treating Columns 1-3,
		4-5, and 6-7 each as a separate family of outcomes. Two alternative
		families of outcomes gave the same results: (1) Columns 1-3 and 4-7
		as separate families and (2) all columns as one family.
	;
	#d cr
}
else if `category' == `CatIncome' {
	loc g1 Q13_3_amount Q13_jobincome remitandtrans Q13_5_amount_mo
	loc families Q13_3_amount Q13_jobincome remitandtrans Q13_5_amount_mo
	loc business1 Q13_3_amount
	loc tg1 Table 4: Income
	#d ;
	loc outcome_notes1 "Income in Column 1 is calculated from a question asking
		an explicit, all-in question about household income from business or
		productive activity. Column 2 includes salaried jobs with a fixed
		schedule as well as jobs without a fixed salary. Column 3 includes gifts
		or help in the last month from a family member, neighbor, or friend that
		is not a member of the household; as well as remittances in the last 6
		months, divided by 6 to adjust to monthly values. Column 4 is government
		subsidies or aid in the last 2 months, divided by 2 to adjust to monthly
		values. The adjusted critical values were calculated by treating all
		outcomes in the table as one outcome family."
	;
	#d cr
}
else if `category' == `CatLabor' {
	loc g1 Q12_2 Q5_frac_working Q10_4_tot_family
	loc families Q12_2 Q5_frac_working Q10_4_tot_family
	loc business1 Q10_4_tot_family
	loc tg1 Table 5: Labor Supply
	#d ;
	loc outcome_notes1 Anyone reporting having a job or a business is classified
		as participating in an economic activity (Column 1). Number of family
		employees in Column 3 is calculated by summing the number of family
		employees for each of 4 businesses of the respondent's. The adjusted
		critical values were calculated by treating all outcomes in the table
		as one outcome family.
	;
	#d cr
}
else if `category' == `CatConsumption' {
	loc g1 Q9_3_totalcategories Q9_totvalue Q7_nonfood_nondur foodconsump ///
		Q18_2_2_amount_wk Q18_2_1_amount_wk Q7_tempt Q18_2_3_amount_wk ///
		Q9_4_soldloan_none Q9_4_anycategories
	loc families Q9_3_totalcategories Q9_totvalue Q7_nonfood_nondur ///
		foodconsump Q18_2_2_amount_wk Q18_2_1_amount_wk Q7_tempt ///
		Q18_2_3_amount_wk Q9_4_soldloan_none Q9_4_anycategories
	loc tg1 DO NOT USE THIS OUTCOME CATEGORY FOR A TABLE: USE CHIMERA.
	loc outcome_notes1 DO NOT USE THIS TABLE: USE CHIMERA.
}
else if `category' == `CatConsumptionChimera' {
	loc g1 Q9_3_totalcategories Q9_totvalue Q7_nonfood_nondur foodconsump ///
		Q18_2_2_amount_wk Q18_2_1_amount_wk Q7_tempt Q18_2_3_amount_wk
	loc chimera1 `CatConsumption' `CatConsumption' `CatConsumption' ///
		`CatConsumption' `CatConsumption' `CatConsumption' `CatConsumption' ///
		`CatConsumption'
	loc tg1 Table 6: Assets and Weekly Expenditures
	#d ;
	loc outcome_notes1 "The survey instrument did not include details about the
		value of assets bought and sold unless they were bought or sold in
		relation to a loan. Consequently, Column 1 reports the count of
		categories from which assets were purchased. Column 2 reports an
		approximate of the total value of assets purchased: for each asset
		category of purchase, the respondent's total includes the mean value of
		assets in the category purchased with a loan. The total assumes that no
		more than one asset was purchased from each category; see the Data
		Appendix for details. The amounts in Columns 3-8 are weekly. Column 3
		includes cigarettes and transportation in the last week, as well as
		electricity, water, gas, phone, cable, and Internet in the last month,
		adjusted to weekly values. Column 4 is the sum of amount spent on food
		eaten out in the last week and amount spent on groceries in the last 2
		weeks divided by 2. Columns 5-6 were asked for the last year and were
		adjusted to weekly values. Column 7 includes cigarettes, sweets, and
		soda from the last week. Column 8 refers to amount spent in the last
		year on important events such as weddings, baptisms, birthdays,
		graduations, or funerals, adjusted to weekly values. The adjusted
		critical values were calculated by treating all outcomes in the table
		and Columns 7-8 of Table 8 as one outcome family."
	;
	#d cr
}
else if `category' == `CatSocial' {
	* Specify the correct indices to examine (either panel or full sample).
	loc Pan = cond(`sampcode' == `SamplePanel', "Pan", "")
	assert "`Pan'" == ""
	loc g1 Q5_fraction Q16_tot_somepower_cond Q16_tot_aconf_cond ///
		Q2_3_sing_any `Pan'Q15_2_mean_formal `Pan'Q15_2_mean_people
	loc families Q16_tot_somepower_cond Q16_tot_aconf_cond Q2_3_sing_any
	loc tg1 DO NOT USE THIS OUTCOME CATEGORY FOR A TABLE: USE CHIMERA.
	loc outcome_notes1 DO NOT USE THIS TABLE: USE CHIMERA.
}
else if `category' == `CatSocialChimera' {
	* Specify the correct indices to examine (either panel or full sample).
	loc Pan = cond(`sampcode' == `SamplePanel', "Pan", "")
	assert "`Pan'" == ""
	loc g1 Q5_fraction Q16_tot_somepower_cond Q16_tot_aconf_cond ///
		Q2_3_sing_any `Pan'Q15_2_mean_formal `Pan'Q15_2_mean_people Q20_2
	loc chimera1 `CatSocial' `CatSocial' `CatSocial' `CatSocial' `CatSocial' ///
		`CatSocial' `CatCredit'
	loc tg1 Table 7: Social Effects
	#d ;
	loc outcome_notes1 "Columns 2-4 include only married respondents living with
		another adult. The issues in Columns 2 and 3 are:
		whether to buy an appliance or not for the home;
		in what way household members may work outside the home;
		whether to financially support family members; and
		whether to save for the future.
		Higher values in the indices in Columns 5-6 denote
		beneficial outcomes. In Column 5, institutions include government
		workers, financial workers, and banks. Trust in people in Column 6
		includes questions about trust in family, neighbors, personal
		acquaintances, people just met, business acquaintances, people who
		borrow money, strangers, and a question about whether people would be
		generally fair. The adjusted critical values were calculated by treating
		Columns 2-4 as one outcome family and Column 7 of this table and Columns
		1 and 3-9 of Table 2a as another outcome family."
	;
	#d cr
}
else if `category' == `CatWelfare' {
	* Specify the correct indices to examine (either panel or full sample).
	loc Pan = cond(`sampcode' == `SamplePanel', "Pan", "")
	assert "`Pan'" == ""
	loc g1 `Pan'Q22_mean `Pan'stress_index `Pan'Q17_AverageLocus ///
		`Pan'sat_index Q12_1_2_sat Q3_1_sat
	loc tg1 DO NOT USE THIS OUTCOME CATEGORY FOR A TABLE: USE CHIMERA.
	loc outcome_notes1 DO NOT USE THIS TABLE: USE CHIMERA.
}
else if `category' == `CatWelfareChimera' {
	* Specify the correct indices to examine (either panel or full sample).
	loc Pan = cond(`sampcode' == `SamplePanel', "Pan", "")
	assert "`Pan'" == ""
	loc g1 `Pan'Q22_mean `Pan'stress_index `Pan'Q17_AverageLocus ///
		`Pan'sat_index Q12_1_2_sat Q3_1_sat Q9_4_soldloan_none ///
		Q9_4_anycategories
	loc chimera1 `CatWelfare' `CatWelfare' `CatWelfare' `CatWelfare' ///
		`CatWelfare' `CatWelfare' `CatConsumption' `CatConsumption'
	loc tg1 Table 8: Various Measures of Welfare
	loc mg1 mgroups("Subjective well-being" "Assets", ///
		pattern(1 0 0 0 0 0 1 0) lhs("Group"))
	#d ;
	loc outcome_notes1 "Higher values in the indices denote beneficial outcomes.
		Column 1 consists of a standard battery of 20 questions that ask about
		thoughts and feelings in the last week. The feelings and mindsets
		include: being bothered by things that do not normally bother you,
		having a poor appetite, not being able to shake off the blues even with
		support from friends and family, feeling just as good as other people,
		having trouble focusing, feeling depressed, feeling like everything
		required extra effort, being hopeful about the future, thinking your
		life was a failure, feeling fearful, having restless sleep, feeling
		happy, talking less than usual, being lonely, thinking people were
		unfriendly, having crying spells, enjoying life, feeling sad, thinking
		people dislike you, feeling like you couldn't keep going on. In Column
		2, the sample frame is restricted to just those that report
		participating in an economic activity; the index includes three
		questions about job stress. The index of locus of control in Column 3
		includes five questions about locus of control. The adjusted critical
		values were calculated by treating Columns 7-8 of this table and Columns
		1-8 of Table 6 as an outcome family."
	;
	#d cr
}
* Unknown
else {
	di as err "unknown outcome category `category'"
	ex 9
}

* Check that locals are defined when they should be.
foreach loc in serror {
	assert `:list sizeof `loc''
}
loc maxgroups 1
forv i = 1/`maxgroups' {
	* Trim superflous spaces.
	foreach pre in g chimera business tg mg outcome_notes {
		mata: st_local("`pre'`i'", strtrim(stritrim(st_local("`pre'`i'"))))
	}
	* Locals that should be defined if `g`i'' is
	foreach pre in tg outcome_notes {
		assert `:length loc `pre'`i'' if `:length loc g`i''
	}
	* Locals that should not be defined if `g`i'' isn't
	foreach pre in business tg mg outcome_notes {
		assert !`:length loc `pre'`i'' if !`:length loc g`i''
	}

	* Add the prefix "Outcome(s): " to the outcome group notes.
	if `:list sizeof g`i'' {
		loc outcome_notes`i' "Outcome(s): `outcome_notes`i''"
	}
}

* Further checking

* `serror'
conf var `serror', exact

* `g#', `business#', and `chimera#'
forv i = 1/`maxgroups' {
	* `g#'
	loc depvars
	foreach typed of loc g`i' {
		parse_typed_outcome `typed'
		loc depvar `s(depvar)'
		loc depvars : list depvars | depvar
	}
	assert `:list sizeof depvars'
	conf var `depvars', exact
	* Check that there are no duplicate dependent variables in the outcome
	* group. Even if there are no duplicate typed outcomes in the outcome group,
	* there could be duplicate dependent variables if two or more typed outcomes
	* are different only in their regression types.
	assert `:list sizeof depvars' == `:list sizeof g`i''

	* `business#'
	assert `:list business`i' in depvars'

	* `chimera#'
	assert inlist(`:list sizeof chimera`i'', 0, `:list sizeof g`i'')
	if `:list sizeof chimera`i'' {
		* Chimeras and typed outcomes do not go together. If the outcome
		* category is a chimera, it is assumed that all typed outcomes are
		* simple dependent variable names.
		assert `:list depvars == g`i''

		* p-value adjustments do not occur within chimeras but rather their
		* components.
		assert !`:list sizeof families'

		foreach cat of loc chimera`i' {
			evalname Cat `cat'
			* Chimeras cannot be composed of other chimeras.
			assert !strpos(strlower("`r(name)'"), "chimera")
		}
	}
}

* `families'
if `:list sizeof families' {
	* "dq" for "double quote"
	loc temp : subinstr loc families `"""' "", cou(loc dq)
	if !`dq' ///
		loc families `""`families'""'
	* Check that there is no overlap between families.
	foreach family of loc families {
		loc allvars `allvars' `family'
	}
	assert !`:word count `:list dups allvars''
	assert `:list allvars in depvars'
}

* Save the locals back into the parent do-file.
* See <http://www.stata.com/statalist/archive/2003-12/msg00385.html> on
* -c_local-.
if "`clustvar'" != "" ///
	c_local `clustvar' `serror'
if "`adj_families'" != "" ///
	c_local `adj_families' "`families'"
foreach pre in g chimera business tg mg outcome_notes {
	forv i = 1/`maxgroups' {
		loc all_`pre' `"`all_`pre'' `"``pre'`i''"'"'
	}
	loc all_`pre' : list clean all_`pre'
}
if "`groups'" != "" ///
	c_local `groups'			"`all_g'"
if "`chimeras'" != "" ///
	c_local `chimeras'			"`all_chimera'"
if "`business'" != "" ///
	c_local `business'			"`all_business'"
if "`titles'" != "" ///
	c_local `titles'			"`all_tg'"
if "`mgroups'" != "" ///
	c_local `mgroups'			"`all_mg'"
if "`outcome_notes'" != "" ///
	c_local	`outcome_notes'		"`all_outcome_notes'"

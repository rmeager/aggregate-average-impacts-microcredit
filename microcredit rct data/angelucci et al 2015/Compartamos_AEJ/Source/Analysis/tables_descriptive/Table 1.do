/*
Authors:
	Andrew Hillis, Innovations for Poverty Action
	Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
Purpose: Create Table 1.
Notes: This is an intermediate step in the creation of the final version of
Table 1 of the AEJ paper. Additional manual formatting is required:
1. After copying columns 4-6 to Appendix Table 1 (see "Appendix Table 1.do"),
	remove them from Table 1.
2. Remove rows that are all blank now that columns 4-6 have been removed.
*/

vers 13


/* -------------------------------------------------------------------------- */
					/* initialize			*/

* Set the working directory.
cap c comprado

include header

enumtypes


/* -------------------------------------------------------------------------- */
					/* define parameters	*/

* Display format of real numbers in the table
loc format %9.3f

* `outcome' is the outcome variable of interest. `outcome' is not the dependent
* variable in all the tests below: distinguish the overall outcome of interest
* and the dependent variable of a test.
loc outcome Treatment

* Cluster variable
loc clustvar cluster

* Lists of panels' names and corresponding samples
* Panels refer to panels of the table, not the panel sample.
loc panels Endline Panel
loc panel_samples `SampleEndline' `SamplePanel'

* `panel_titles' is a parallel list to `panels': each element is the panel title
* of the corresponding panel.
#d ;
loc panel_titles "
	"Full Endline Sample Frame"
	"Balance Tests for Panel Sample"
";
#d cr

* `panel_vars' is a parallel list to `panels': each element is the list of
* balance variables for the corresponding panel.
do "Analysis/tables_descriptive/Define descriptive globals"
loc panel_vars ""$Endline_vars" "$Panel_vars""


/* -------------------------------------------------------------------------- */
					/* modify dataset		*/

u "$andata_aej", clear

do "Analysis/tables_descriptive/Descriptive variable labels"

tempfile descdata
sa `descdata'


/* -------------------------------------------------------------------------- */
					/* check parameters		*/

u `descdata', clear

* `format'
mata: assert(st_isnumfmt(st_local("format")))

* `outcome'
assert `:list sizeof outcome' == 1
conf var `outcome', exact

* `clustvar'
assert `:list sizeof clustvar' == 1
conf var `clustvar', exact

* `panels' `panel_samples' `panel_titles' `panel_vars'
assert `:list sizeof panels' == `:list sizeof panel_samples'
assert `:list sizeof panels' == `:list sizeof panel_titles'
assert `:list sizeof panels' == `:list sizeof panel_vars'

* `panels'
assert `:list sizeof panels'
foreach panel of loc panels {
	assert `:list sizeof panel' == 1
	conf name `panel'
	assert !strpos("`panel'", "_")
}

* `panel_samples'
foreach sample of loc panel_samples {
	sampexp `sample'
}

* `panel_vars'
foreach vars of loc panel_vars {
	conf var `vars', exact
	unab unab : `vars'
	assert `:list vars == unab'

	loc exclude Treatment BTreatment
	assert "`:list vars & exclude'" == ""
}


/* -------------------------------------------------------------------------- */
					/* top of the table		*/

* Begin creating the table as its own dataset.

* Row 1: Table title
* Row 2: Panel titles
* Row 3: Statistic labels
* Row 4: Column numbers

clear
loc header_rows 4
set obs `header_rows'

* Column 1, which contains the names of balance variables
gen Variable = ""
* Create the other variables, which hold the statistics.
* Only these variables should have names that contain underscores.
foreach panel of loc panels {
	* Column 1 is the name of the statistic.
	* Column 2 is the statistic label.
	loc stats ///
		Treatment	Mean ///
		Difference	"Difference: Treatment - Control" ///
		Test		"Balance Test"
	assert mod(`:list sizeof stats', 2) == 0
	while `:list sizeof stats' {
		gettoken stat stats : stats
		gettoken lab  stats : stats

		* Check `stat'
		assert `:list sizeof stat' == 1
		conf name `stat'
		assert !strpos("`stat'", "_")

		gen `panel'_`stat' = "`lab'" in 3
	}
}

* Table title
replace Variable = "Table 1: Summary statistics and balance tests" in 1

* Panel titles
foreach var of var *_Treatment {
	loc panel = subinstr("`var'", "_Treatment", "", 1)
	loc pos : list posof "`panel'" in panels
	loc title : word `pos' of `panel_titles'
	replace `var' = "`title'" in 2
}

* Column numbers
loc i 1
ds Variable, not
foreach var in `r(varlist)' {
	replace `var' = "(`i++')" in `header_rows'
}

tempfile results
sa `results'


/* -------------------------------------------------------------------------- */
					/* main results			*/

u `results', clear

assert !strmatch(Variable, "SE_*")

forv i = 1/`:list sizeof panels' {
	loc panel   : word `i' of `panels'
	loc sample  : word `i' of `panel_samples'
	loc balvars : word `i' of `panel_vars'

	/* Results are stored in locals whose names include the panel name or a
	balance variable name. The panel name and the balance variable name must
	come after a prefix for the result, or results may be accidentally
	overwritten. No prefix should be the same as another prefix with extra
	characters appended. For example, if one prefix is N, the prefix N_2 should
	not be used. The following prefixes are used:

	[Prefixes followed by panel name]
	clusr cluss F Nr Ns pint_ YMean

	[Prefixes followed by balance variable name]
	b_ d m1m m1s se_ sf vl

	Results should be formatted immediately. */

	/* ---------------------------------------------------------------------- */
					/* define panel locals	*/

	u `descdata', clear

	* The baseline sample is all within one supercluster.
	* "sc_controls" for "supercluster controls"
	loc sc_controls
	if "`panel'" == "Endline" {
		xi i.supercluster_xi
		unab sc_controls : _Isuper*
	}

	sampexp `sample'
	* "pansamp" for "panel sample"
	loc pansamp "`r(exp)'"
	assert survey != "Telephone" if `pansamp'
	assert mi(attrited) if survey == "Telephone"

	/* ---------------------------------------------------------------------- */
					/* means				*/

	* Estimate means and differences in means for the first two columns
	* (*_Treatment and *_Difference).

	foreach var of loc balvars {
		* Variable label
		loc vl`var' : var lab `var'

		* Means
		ci `var' if `pansamp'
		loc m1m`var' = strofreal(r(mean), "`format'")
		loc m1s`var' = strofreal(r(se),   "`format'")

		* Difference between treatment and control
		* Do not regress on `sc_controls' if `var' is a linear combination of
		* `sc_controls' (e.g., urban).
		reg `var' `sc_controls' if `pansamp'
		if e(r2) == 1 ///
			loc sc
		else ///
			loc sc `sc_controls'
		reg `var' `outcome' `sc' if `pansamp', cl(`clustvar')
		* Check that there was no collinearity.
		loc indepvars : colnames e(b)
		loc temp : subinstr loc indepvars "o." "", cou(loc any)
		assert !`any'
		test `outcome'
		stars `=_b[`outcome']', p(`r(p)')
		loc d`var' `r(starred)'
		loc sf`var' = strofreal(_se[`outcome'], "`format'")
	}

	/* ---------------------------------------------------------------------- */
					/* balance tests		*/

	* If there is collinearity, control which variables are omitted.
	_rmcollright `outcome' `balvars' `sc_controls' if `pansamp'
	loc dropped `r(dropped)'
	loc n : list sizeof dropped
	assert `n' <= 1
	if !`n' ///
		loc sc `sc_controls'
	else {
		assert strmatch("`dropped'", "_Isuper*")
		loc omit _Isuperclus_45
		assert `:list omit in sc_controls'
		loc sc : list sc_controls - omit
	}

	reg `outcome' `balvars' `sc' if `pansamp', cl(`clustvar')
	test `balvars'
	loc pint_`panel' = strofreal(r(p), "`format'")

	foreach var of loc balvars {
		test `var'
		stars `=_b[`var']', p(`r(p)')
		loc b_`var' `r(starred)'
		loc se_`var' = strofreal(_se[`var'], "`format'")
	}

	su `outcome' if e(sample)
	loc YMean`panel' = strofreal(r(mean), "`format'")
	loc indep : colnames e(b)
	loc cons _cons
	loc indep : list indep - cons
	test `indep'
	loc F`panel' = strofreal(r(p), "`format'")
	if "`panel'" == "Panel" ///
		assert "`F`panel''" == "`pint_`panel''"
	* "Nr" for "N (from) _r_egression"
	loc Nr`panel' = e(N)
	* "clusr" for "clusters (from) _r_egression"
	loc clusr`panel' = e(N_clust)

	/* ---------------------------------------------------------------------- */
					/* simple panel statistics	*/

	* Calculate simple summary statistics about the panel.

	cou if `pansamp'
	* "Ns" for "N (from) _s_imple (statistics)"
	loc Ns`panel' = r(N)

	qui ta cluster if `pansamp'
	* "cluss" for "clusters (from) _s_imple (statistics)"
	loc cluss`panel' = r(r)

	/* ---------------------------------------------------------------------- */
					/* add to table dataset	*/

	* Fill out columns: add results to the middle of the table.
	* (We will add results to the bottom of the table later.)

	/* Results that depend on the panel but whose local name does not contain
	the panel name must be added here: they will be overwritten in later
	iterations of the loop. For example, `b_[balance variable]' must be added
	here, but `vl_[balance variable]' does not, because it is constant across
	panels. */

	u `results', clear

	* Get the list of balance variables already in the table.
	levelsof Variable if _n > `header_rows', loc(tablevars)

	foreach var of loc balvars {
		if !`:list var in tablevars' {
			assert !inlist(Variable, "`var'", "SE_`var'")

			set obs `=_N + 2'
			replace Variable = "`var'"		in `=_N - 1'
			replace Variable = "SE_`var'"	in L
		}

		cou if Variable == "`var'"
		assert r(N) == 1
		cou if Variable == "SE_`var'"
		assert r(N) == 1

		replace `panel'_Treatment = "`m1m`var''"   if Variable == "`var'"
		replace `panel'_Treatment = "(`m1s`var'')" if Variable == "SE_`var'"

		replace `panel'_Difference = "`d`var''"    if Variable == "`var'"
		replace `panel'_Difference = "(`sf`var'')" if Variable == "SE_`var'"

		replace `panel'_Test = "`b_`var''" if Variable == "`var'"
		replace `panel'_Test = "(`se_`var'')" ///
			if Variable == "SE_`var'" & "`b_`var''" != ""
	}

	sa, replace
}


/* -------------------------------------------------------------------------- */
					/* finish up middle		*/

* Finish up the middle of the table.

* Balance variable labels
replace Variable = "" if strmatch(Variable, "SE_*")
levelsof Variable if _n > `header_rows'
foreach var in `r(levels)' {
	replace Variable = `"`vl`var''"' if Variable == "`var'"
}

* Add omitted categories.
gen n = _n
#d ;
loc omitted "
	"Primary school or none"	"(omitted: above high school)"
	Married						"(omitted: single)"
	"Age 31 - 40"				"(omitted: 18-30)"
";
#d cr
assert mod(`:list sizeof omitted', 2) == 0
while `:list sizeof omitted' {
	gettoken var  omitted : omitted
	gettoken omit omitted : omitted

	levelsof n if Variable == "`var'", loc(n)
	if `:list sizeof n' {
		assert `:list sizeof n' == 1
		assert Variable == "" in `=`n' + 1'
		replace Variable = "`omit'" in `=`n' + 1'
	}
}
drop n

* Add the female balance variable, which is constant.
gen n = _n
set obs `=_N + 1'
replace n = `header_rows' + .5 in L
assert Variable != "Female"
replace Variable = "Female" in L
sort n
replace Endline_Treatment  = "1" if Variable == "Female"
replace Endline_Difference = "0" if Variable == "Female"
replace Panel_Treatment    = "1" if Variable == "Female"
replace Panel_Difference   = "0" if Variable == "Female"
drop n


/* -------------------------------------------------------------------------- */
					/* results at the bottom	*/

* Add blank rows.
set obs `=_N + 2'

#d ;
foreach row in
	N
	"Number of clusters"
	"Share of sample in treatment group"
	"pvalue of F test of joint significance of explanatory variables"
{;
	#d cr
	assert Variable != "`row'"
	set obs `=_N + 1'
	replace Variable = "`row'" in L
}

foreach panel of loc panels {
	foreach var of var `panel'_* {
		replace `var' = "`Ns`panel''" if Variable == "N"
		replace `var' = "`cluss`panel''" if Variable == "Number of clusters"
	}

	replace `panel'_Test = "`YMean`panel''" ///
		if Variable == "Share of sample in treatment group"
	replace `panel'_Test = "`pint_`panel''" if Variable == ///
		"pvalue of F test of joint significance of explanatory variables"
	replace `panel'_Test = "`Nr`panel''" if Variable == "N"
	replace `panel'_Test = "`clusr`panel''" if Variable == "Number of clusters"
}


/* -------------------------------------------------------------------------- */
					/* notes				*/

#d ;
loc notes Respondents are Mexican women aged 18-60. Column 2 reports the
	coefficient on treatment assignment (1=Treatment, 0=Control) when the
	variable in the row is regressed on treatment assignment. Column 3 reports
	the results of balance tests. The cells show the coefficient for each
	variable when they are all included in one regression with treatment
	assignment as the dependent variable. Standard errors are in parentheses
	below the coefficients. All regressions include supercluster fixed effects
	and standard errors clustered by the unit of randomization.
	* p<0.10, ** p<0.05, *** p<.01.
;
#d cr
mata: st_local("notes", strtrim(stritrim(st_local("notes"))))
set obs `=_N + 1'
replace Variable = `"`notes'"' in L


/* -------------------------------------------------------------------------- */
					/* additional formatting	*/

loc i 1
foreach var of var _all {
	ren `var' c`i++'
}

* Create variable row for the row number, using type double to avoid precision
* issues after some row numbers are modified to be non-integer.
gen double row = _n

#d ;
lab de table
	1	title
	2	panels
	3	labels	// statistic labels
	4	numbers
	5	coeffs	// results in the middle of the table
	6	scalars	// results at the bottom of the table
	7	notes
;
#d cr

su row if c1 == "N"
assert r(N) == 1
loc startscalar = r(min)
assert `startscalar' >= 7
su row if c1 == "pvalue of F test of joint significance of explanatory variables"
assert r(N) == 1
loc endscalar = r(min)
assert `endscalar' >= `startscalar'

gen type = .
lab val type table
replace type = "title":table		if row == 1
replace type = "panels":table		if row == 2
replace type = "labels":table		if row == 3
replace type = "numbers":table		if row == 4
replace type = "coeffs":table ///
	if inrange(row, 5, `startscalar' - 1)
replace type = "scalars":table	if inrange(row, `startscalar', `endscalar')
replace type = "notes":table	if row > `endscalar'
assert !mi(type)

reshape long c, i(row) j(col)
gen f = "size10"

* We will complete section-specific formatting, then general formatting.

* Format title.
replace f = f + " bold mergeright3" if type == "title":table & col == 1

* Format panel titles.
replace f = f + " bold" if type == "panels":table & c != ""
* Merge.
gsort row -col
gen merge = .
replace merge = cond(c[_n - 1] != "", 0, max(merge[_n - 1], 0)) + mi(c) ///
	if type == "panels":table
replace f = f + " mergeright" + strofreal(merge) ///
	if type == "panels":table & c != ""
drop merge
* Extra columns
su row
loc rows = r(max)
levelsof col if type == "panels":table & c != "", loc(cols)
gettoken col cols : cols
loc offset 0
foreach col of loc cols {
	loc offcol = `col' + `offset'
	loc groupcols : list groupcols | offcol
	expand 2 if col == `offcol', gen(copy)
	replace c = "" if copy
	replace f = "" if copy
	replace col = col + 1 if col >= `offcol' & !copy
	drop copy
	loc ++offset
}

* Format notes.
replace f = f + " mergeright3" if type == "notes":table & col == 1 & !mi(c)

* Alignment
replace f = f + " center" ///
	if !inlist(type, "title":table, "notes":table) & col > 1
replace f = f + " justify" if type == "notes":table

* Wrap
gen wraprow = !(type == "notes":table & mi(strtrim(c)) & col == 1)
bys row (wraprow): replace wraprow = wraprow[1]
replace f = f + " wrap" if wraprow
su col
loc maxcol = r(max)
replace f = f + " autofitrow" if wraprow & col == `maxcol'
drop wraprow

* Borders
su row if type == "scalars":table
replace f = f + " brbottomdouble" if type == "title":table | row == r(max)
replace f = f + " brbottom" ///
	if inlist(type, "labels":table, "numbers":table) & col > 1

* Concatenate format and column variables.
gen fc = "{" + strtrim(itrim(f)) + "}" + c

reshape wide c f fc, i(row) j(col)

/* Add row heights and column widths. Create variable rowheight for each row's
height, and add column widths to the topmost row (i.e., the row with the minimum
value of variable row). Once rowheight is -order-ed to the beginning of the
dataset and the dataset is sorted by variable row, the dataset will look like
this:

rowheight	c1	c2	c3	...
---------------------------
.			30	30	30	...		[column widths]
15			A1	B1	C1	...
15			A2	B2	C2	...
...			...	...	...	...

This means that in the table .csv file, the text and formatting of cell A1 won't
start in A1 but B2 (one row and column over). Similarly, the text and formatting
of B1 won't start in B1 -- that is where the width of column A is specified --
but instead in C2. After processing them, the formatTables macro removes the row
heights column and column widths row, returning all cells to their proper
locations. */

* Row heights
gen rowheight = .
order rowheight
egen nonmiss = rownonmiss(c*), str
replace rowheight = 15
//if !nonmiss | type != "coeffs":table | ///
//	type != "scalars":table
drop nonmiss
su row if type == "notes":table
replace rowheight = 22 if row == r(min)
replace rowheight = 8 if type == "notes":table & mi(strtrim(c1))
replace rowheight = 15 * ceil(strlen(c1) / 31) if mi(rowheight)
replace rowheight = max(rowheight, 30) if type == "coeffs":table
assert !mi(rowheight)
* Column widths
set obs `=_N + 1'
su row
replace row = r(min) - 1 in L
foreach var of var fc* {
	replace `var' = "12" in L
}
replace fc1 = "24" in L
foreach col of loc groupcols {
	replace fc`col' = "0.5" in L
}
sort row


/* -------------------------------------------------------------------------- */
					/* finish up			*/

loc outfile "$resdir/Tables/Unformatted/Table 1.csv"
export delim rowheight fc* using "`outfile'", delim(",") novar replace

* Authors:
* Andrew Hillis, Innovations for Poverty Action
* Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
* Purpose: Modify variable labels for Table 1.

* Remove modifiers from variable labels.
foreach var of var _all {
	loc lab : var lab `var'
	foreach str in baseline "- Mismatch" "- " {
		loc lab = subinstr(`"`lab'"', "`str'", "", .)
	}

	lab var `var' `"`lab'"'
}

lab var BF5						Age
lab var base_formalexper_dum	"High formal credit experience"

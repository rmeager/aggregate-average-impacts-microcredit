* Authors:
*	Andrew Hillis, Innovations for Poverty Action
*	Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
* Purpose: Define globals for Table 1 and Appendix Table 1.

* Component variable lists

* Education variables
loc educvars Q1_5_primary Q1_5_middle Q1_5_high
* Endline heterogeneous variables
loc endhetvars business_before
* Endline-only variables
loc endonlyhetvars urban
* Baseline heterogeneous variables
glo descbasevars base_inc_adult_1000 base_riskpref base_formalexper_dum ///
	impatient_now present_bias_dum base_account base_cundina

#d ;

* Table 1;
glo Endline_vars
	F5_1
	`educvars'
	`endhetvars' `endonlyhetvars'
;
glo Panel_vars
	F5_1
	BQ1_2_married_bin BQ1_2_sep_bin
	`educvars'
	`endhetvars' $descbasevars
;

* Appendix Table 1;
glo Baseline_vars
	BTreatment
	BF5
	`endhetvars'
	BQ1_2_married_bin BQ1_2_sep_bin
	$descbasevars
;

#d cr

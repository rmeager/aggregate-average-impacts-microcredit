/*
Authors:
	Andrew Hillis, Innovations for Poverty Action
	Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
Purpose: Define a number of locals that store the properties of the
specification, which determine what analysis is completed and what output is
produced. The locals are defined based on the specification name, specified to
option -spec()-. The following locals are defined:

`treatmentvar'	List of treatment variable, heterogeneous independent variables,
				and interactions between them
`hetvars'		Heterogeneous variables. These are different from the
				heterogeneous independent variables, which either are the same
				as the heterogeneous variables or are their opposites (*_o
				variable). The term "heterogeneous variables" strictly refers to
				`hetvars': "independent" is added where necessary.
`hetvar_notes'	Notes to add to the tables about how the specification handles
				heterogeneity
`panel'			Independent variables to include if the dependent variable has a
				baseline equivalent
`hetivar'		Code for regression sample
`resultsf'		Directory to store the specification's tables

Each local is saved back into the parent do-file as a local.
The names of the locals in the parent do-file are specified as options. */

/*
Syntax
------

{do|run} "Set specification properties", spec() [options]

Main
----
spec()			Specification name

Local macro names
-----------------
treathet()		Name to use when saving `treatmentvar' as a local macro in
				the parent do-file
heterovars()	Local macro name of `hetvars'
hetero_notes()	Local macro name of `hetvar_notes'
panelvars()		Local macro name of `panel'
sampcode()		Local macro name of `hetivar'
tabledir()		Local macro name of `resultsf'
*/

mata: st_local("0", ", " + st_local("0"))
loc lclname name local
#d ;
syntax,
	/* inputs */
	spec(name)
	/* outputs */
	[treathet(`lclname') heterovars(`lclname') hetero_notes(`lclname') ///
	panelvars(`lclname') sampcode(`lclname') tabledir(`lclname')]
;
#d cr

enumtypes

di "`spec'"

* Set default values for some of the locals.
loc panel BaselineValue BaselineValueMDum InPanel
loc hetivar = `SampleEndline'

* AIT for full (endline) sample
if "`spec'" == "Full_TvC" {
	loc treatmentvar Treatment
	loc fdir Tables 3-7
}
else {
	unknown_spec `spec'
	/*NOTREACHED*/
}

* Trim superfluous spaces in `hetvar_notes'.
mata: st_local("hetvar_notes", strtrim(stritrim(st_local("hetvar_notes"))))

* Check locals that should not be blank.
foreach loc in treatmentvar panel hetivar {
	assert `:length loc `loc''
}

* Check `treatmentvar', `hetvars', and `hetvar_notes'.
conf var `treatmentvar' `hetvars', exact
assert !`:word count `:list dups treatmentvar''
assert !`:word count `:list dups hetvars''
loc Treatment Treatment
if !strmatch("`spec'", "Heterogeneous*") {
	assert `:list treatmentvar == Treatment'
	assert !`:list sizeof hetvars'
	assert !`:length loc hetvar_notes'
}
else {
	* `hetvars'
	assert `:list sizeof hetvars'
	foreach var of loc hetvars {
		assert inlist(`var', 0, 1) if !mi(`var')
		assert strlen("nhymean_`var'") <= c(namelen)

		* Check that no variable of `hetvars' is a heterogeneous variable with
		* opposite meaning.
		assert !strmatch("`var'", "*_o")
	}

	* `treatmentvar' and `hetvars'
	loc hasTreatment : list Treatment in treatmentvar
	loc hasTX 0
	loc hasTX_No 0
	foreach var of loc treatmentvar {
		if strmatch("`var'", "TX_No_*") ///
			loc hasTX_No 1
		else if strmatch("`var'", "TX_*") ///
			loc hasTX 1
	}
	assert `hasTreatment' + `hasTX' + `hasTX_No' == 2
	if `hasTX' & `hasTX_No' ///
		assert `:list sizeof hetvars' == 1
}

* Check `hetivar'.
tempvar test
sampexp `hetivar'
gen `test' = `r(exp)'
assert inlist(`test', 0, 1)

* Check `panel'.
if `:list sizeof panel' ///
	conf name `panel'

* Define `resultsf'.
assert `:length loc fdir'
if "`spec'" == "Full_TvC" ///
	loc resultsf "$resdir/Tables/`fdir'"
else ///
	loc resultsf "$resdir/Tables/HTE result tables/`fdir'"
* Create the directory specified by `resultsf' if necessary.
cap mkdir "`resultsf'"
mata: assert(direxists(st_local("resultsf")))

* Save the locals back into the parent do-file.
* See <http://www.stata.com/statalist/archive/2003-12/msg00385.html> on
* -c_local-.
if "`treathet'" != "" ///
	c_local `treathet'			`treatmentvar'
if "`heterovars'" != "" ///
	c_local `heterovars'		`hetvars'
if "`hetero_notes'" != "" ///
	c_local `hetero_notes'		"`hetvar_notes'"
if "`panelvars'" != "" ///
	c_local `panelvars'			`panel'
if "`sampcode'" != "" ///
	c_local `sampcode'			`hetivar'
if "`tabledir'" != "" ///
	c_local `tabledir'			"`resultsf'"

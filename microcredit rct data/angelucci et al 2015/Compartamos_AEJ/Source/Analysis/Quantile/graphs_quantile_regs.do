/*
Authors:
	Andrew Hillis, Innovations for Poverty Action
	Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
Date of creation: 2012
Purpose: Run quantile regressions using bootstrapped SEs, saving the results for
use in other do-files.
*/

***Run in Stata 13 to replicate AEJ!***

vers 10.1


/* -------------------------------------------------------------------------- */
					/* initialize			*/

* Set the working directory.
cap c comprado

include header

enumtypes

cap log close analysis_qtes
cap log using "${logf}analysis_qtes", name(analysis_qtes) s replace


/* -------------------------------------------------------------------------- */
					/* specify parameters	*/

* The paper for which to produce the results
loc paper `PaperAEJ'
* The filename of the dataset to store the quantile results
* Subsets of the dataset will be stored in the "Quantile subsets" directory of
* the file's parent directory.
loc resultsdta "$resdir/Datasets/quantiles"
* The filename of the dataset to store the count variable results
loc countdta "$resdir/Datasets/quantiles_counts"
* Regression sample code
loc sampcode `SampleEndline'
* numlist of quantiles to estimate
loc quants 5(5)95
* Number of repetitions for the bootstrap
loc reps 1000
* 1 to add data signatures to intermediate bootstrap result datasets; 0 not to.
loc datasig 0

* The outcome categories to run
* AEJ
loc torun `torun' `CatSelfEmploy'
loc torun `torun' `CatIncome'
loc torun `torun' `CatLabor'
loc torun `torun' `CatConsumptionChimera'
loc torun `torun' `CatSocialChimera'
loc torun `torun' `CatWelfareChimera'


/* -------------------------------------------------------------------------- */
					/* check parameters		*/

* `paper'
evalname Paper `paper'
assert inlist(`paper', `PaperAEJ')
* `sampcode'
sampexp `sampcode'
* `quants'
numlist "`quants'"
loc quants `r(numlist)'
* `datasig'
assert inlist(`datasig', 0, 1)
loc datasig = cond(`datasig', "datasig", "")


/* -------------------------------------------------------------------------- */
					/* create results dataset	*/

* Create a dataset named `tempresults' to store the quantile results.
* At the end of the section, it will be saved as `resultsdta'.
clear
tempfile tempresults
sa `tempresults', empty

* Prepare to save results dataset subsets.
mata: pathsplit(st_local("resultsdta"), dir = "", file = "")
mata: st_local("subsetdir", pathjoin(dir, "Quantile subsets"))
cap mkdir "`subsetdir'"
loc vers = floor(c(stata_version))
loc subsetdir "`subsetdir'/Stata `vers', reps=`reps'"
cap mkdir "`subsetdir'"
cap mkdir "`subsetdir'/old"

* Loop through the outcome categories of `torun'.
loc chivars Q10_4_tot_helpers Q10_4_tot_helpers_s Q4_3 Q9_3_totalcategories ///
	Q16_tot_somepower_cond Q16_tot_aconf_cond
foreach category of loc torun {
	evalname Cat `category'
	loc catname `r(name)'

	do "Analysis/Quantile/Quantile outcome group" ///
		category(`category') group(g0) count(countout)
	foreach typed of loc g0 {
		loc count = cond(`:list typed in countout', "count", "")
		parse_typed_outcome `typed'
		loc var `s(depvar)'
		loc chi = cond(`:list var in chivars', "chi", "")
		#d ;
		do "Analysis/Quantile/Quantile outcome"
			resultsdta(`tempresults')
			dtasubset("`subsetdir'/quantiles `catname' `typed'")
			category(`category') typed(`typed') `count'
			paper(`paper') sampcode(`sampcode') quants(`quants') reps(`reps')
			`chi' `datasig',
		;
		#d cr
	}
}

* Add metadata to the results dataset, and save it.
if `:list sizeof torun' {
	u `tempresults', clear
	dtanotes, creator(graphs_quantile_regs.do)
	* -qreg- seems to produce different results in Stata 10 and 13, even under
	* version control.
	note: This dataset may replicate only if Stata `c(stata_version)' is used.
	sa "`resultsdta'", replace
}


/* -------------------------------------------------------------------------- */
					/* count variables		*/

* Run a slightly different estimation for outcomes that are counts (e.g., number
* of employees).

* Create a dataset named `tempresults' to store the count variable results.
* At the end of the section, it will be saved as `countdta'.
clear
tempfile tempresults
sa `tempresults', empty

* Column 1: Count variable name
* Column 2: Cutoff points to examine
* Colume 3: 1 if the rightmost point extends to the right; 0 if not.
#d ;
if `paper' == `PaperAEJ'
	loc specs
		/* self-employment */
		Q10_1					0/2		1
		/* labor */
		Q10_4_tot_family		0/2		1
		/* consumption */
		Q9_3_totalcategories	0/3		1
		/* social */
		Q16_tot_somepower_cond	0/4		0
		Q16_tot_aconf_cond		0/4		0
;
#d cr
while `:list sizeof specs' {
	* "c" prefix for "count": "cvar" for "count variable."
	gettoken cvar		specs : specs
	gettoken cvals		specs : specs
	gettoken extendr	specs : specs

	assert inlist(`extendr', 0, 1)
	loc extendr = cond(`extendr', "extendr", "")
	do "Analysis/Quantile/Quantile count outcome" ///
		countdta(`tempresults') ///
		cvar(`cvar') cvals(`cvals') `extendr' ///
		paper(`paper') sampcode(`sampcode')
}

* Add metadata to the results dataset, and save it.

u `tempresults', clear
dtanotes, creator(graphs_quantile_regs.do)
sa "`countdta'", replace


/* -------------------------------------------------------------------------- */
					/* finish up			*/

cap log close analysis_qtes

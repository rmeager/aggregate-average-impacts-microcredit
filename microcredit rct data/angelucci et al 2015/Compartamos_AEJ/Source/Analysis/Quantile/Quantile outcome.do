/*
Authors:
	Andrew Hillis, Innovations for Poverty Action
	Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
Purpose: Run the quantile regressions and update the quantile results dataset
for a single outcome.

Options
-------
All options other than -count-, -chi-, and -datasig- are required.

Dataset
-------
resultsdta()	Filename of the quantile results dataset
dtasubset()		Filename to save this outcome's subset of the results dataset

Dependent variable
------------------
category()		Outcome category
typed()			Name of typed outcome

graphs_quantile_regs.do parameters
----------------------------------
paper()			`paper' parameter
sampcode()		`sampcode' parameter
quants()		`quants' parameter
reps()			`reps' parameter

Other
-----
chi				Run a chi-squared test on the outcome.
datasig			Add data signatures to intermediate bootstrap result datasets.
*/

mata: comma0()
#d ;
syntax,
	/* dataset */
	resultsdta(str) dtasubset(str)
	/* dependent variable */
	category(integer) typed(name) [count]
	/* graphs_quantile_regs.do parameters */
	paper(integer) sampcode(integer) quants(numlist) reps(integer)
	/* other */
	[chi datasig]
;
#d cr

enumtypes

sampexp `sampcode'
loc sampexp `r(exp)'

if `paper' == `PaperAEJ' {
	vers 11.2: set seed 356500692
}
else {
	err 9
}


/* -------------------------------------------------------------------------- */
					/* bootstrapped quantile regressions	*/

* Bootstrap quantile regressions, saving the results in a postfile.

do "Analysis/Quantile/Prepare regression variables" ///
	typed(`typed') paper(`paper') sampcode(`sampcode') dopanel(dopanel)
loc var `typed'
conf var `var', exact

* Minimize the size of the dataset.
keep if `sampexp'
keep `var' Treatment `dopanel' cluster survey InPanel
/* Drop missing observations. From [R]:

"You should thus drop observations with missing values. Leaving in missing
values causes no problem in one sense because all Stata commands deal with
missing values gracefully. It does, however, cause a statistical problem.
Bootstrap sampling is defined as drawing, with replacement, samples of size N
from a set of N observations. bootstrap determines N by counting the number of
observations in memory, not counting the number of nonmissing values on the
relevant variables. The result is that too many observations are resampled; the
resulting bootstrap samples, because they are drawn from a population with
missing values, are of unequal sizes." */
drop if mi(`var')
foreach dtavar of var _all {
	assert !mi(`dtavar')
}

loc vers = floor(c(stata_version))
loc dtadir "$resdir/Datasets/Quantile bootstrap/Stata `vers', reps=`reps'"
cap mkdir "`dtadir'"
cap mkdir "`dtadir'/old"
* "bsres" for "bootstrap results"
evalname Cat `category'
loc bsres "`dtadir'/bootstrap `r(name)' `var'"

if "`count'" != "" ///
	loc reps 10

if `paper' == `PaperAEJ' {
	preserve

	tempname post
	tempfile postres
	postfile `post' quantile q using `postres', replace

	loc version vers `c(stata_version)'
	foreach i of loc quants {
		loc cmd qreg `var' Treatment `dopanel', q(`i')
		`version': cap noi `cmd'
		if _rc ///
			post `post' (`i') (.a)
		else {
			loc converged : list converged | i
			post `post' (`i') (_b[Treatment])
			tempfile `i'
			`version': bootstrap q=_b[Treatment], ///
				reps(`reps') cluster(cluster) saving(``i''): ///
				`cmd'
		}
	}

	postclose `post'

	loc n = _N
	u `postres', clear
	gen rep = 0
	foreach i of loc converged {
		d using ``i''
		assert r(N) == `reps'
		append using ``i''
		gen old = !mi(rep)
		sort old, stable
		replace rep = _n if !old
		drop old
		assert rep == _n if mi(quantile)
		replace quantile = `i' if mi(quantile)
	}

	reshape wide q, i(rep) j(quantile)

	gen spec = ""
	gen N = `n'
	gen df_r = .

	sa "`bsres'", replace
	restore
}


/* -------------------------------------------------------------------------- */
					/* additional calculations	*/

loc lab : var lab `var'

* Chi-squared and K-S tests
if "`chi'" != "" {
	ta `var' Treatment if `sampexp', chi
	loc edp_tab = r(p)
	loc edp_ksm .
}
else {
	ksmirnov `var' if `sampexp', by(Treatment)
	loc edp_ksm = r(p_cor)
	loc edp_tab .
}

u "`bsres'", clear

dtanotes, creator(Quantile outcome.do)
sa, replace

* Results for the full dataset
* The first observation of `bsres' is -bootstrap-'s first call to -qtes-, which
* uses the data as-is without resampling.
isid rep
sort rep
assert rep == 0 in 1
assert _N == `reps' + 1
foreach i of loc quants {
	loc q`i'_b = q`i' in 1
}
loc df = df_r in 1
drop in 1

* SEs from the bootstrap
foreach i of loc quants {
	qui su q`i'
	loc q`i'_se = r(sd)
}


/* -------------------------------------------------------------------------- */
					/* add results to dataset	*/

u "`resultsdta'", clear

set obs `=_N + 1'

putlast outvar =	"`var'"
putlast olab =		`"`lab'"'
putlast ord =		_N

parse_typed_outcome `var'
putlast typed_depvar =	"`s(depvar)'"
putlast typed_type2 =	"`s(type2)'"

putlast category =	`category'
categorytable `category'
putlast area =		"`r(name)'"

foreach dtavar in reps df edp_tab edp_ksm {
	putlast `dtavar' = ``dtavar''
}

foreach i of loc quants {
	putlast q`i'_tc  = `q`i'_b'
	putlast q`i'_tse = `q`i'_se'
}

sa, replace

keep if outvar == "`var'"
dtanotes, creator(Quantile outcome.do)
sa "`dtasubset'", replace

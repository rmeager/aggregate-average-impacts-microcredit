/*
Authors:
	Andrew Hillis, Innovations for Poverty Action
	Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
Purpose: Run regressions for a single count variable.

Options
-------
All options other than extendr are required.

Dataset
-------
countdta()	Filename of the count variable results dataset

Specification
-------------
cvar()		Name of typed outcome of count variable
cvals()		Count variable cutoff points to examine
extendr		Rightmost cutoff point extends to the right, including all values
			beyond the point.

graphs_quantile_regs.do parameters
----------------------------------
paper()		`paper' parameter
sampcode()	`sampcode' parameter
*/

mata: comma0()
#d ;
syntax,
	/* dataset */
	countdta(str)
	/* specification */
	cvar(name) cvals(numlist int >=0 sort) [extendr]
	/* graphs_quantile_regs.do parameters */
	paper(integer) sampcode(integer)
;
#d cr

enumtypes

loc uniq : list uniq cvals
assert `:list sizeof cvals' == `:list sizeof uniq'

sampexp `sampcode'
loc sampexp `r(exp)'

do "Analysis/Quantile/Prepare regression variables" ///
	typed(`cvar') paper(`paper') sampcode(`sampcode') dopanel(dopanel)
parse_typed_outcome `cvar'
loc typed_depvar	`s(depvar)'
loc type2			`s(type2)'
if "`type2'" != "" {
	drop `typed_depvar'
	ren `cvar' `typed_depvar'
	loc cvar `typed_depvar'
}

keep if `sampexp' & !mi(`cvar')

* Check `cvar'.
assert `cvar' == floor(`cvar')
gettoken min : cvals
assert `cvar' >= `min'
cou if `cvar' == `min'
assert r(N)
loc max : word `:list sizeof cvals' of `cvals'
if "`extendr'" == "" {
	assert `cvar' <= `max'
	cou if `cvar' == `max'
	assert r(N)
}

* Loop through the cutoff points, estimating the treatment effect on being at
* each of the points.
xi i.supercluster_xi
loc lab : var lab `cvar'
foreach cval of loc cvals {
	if "`extendr'" != "" & `cval' == `max' {
		loc op >=
		loc cval_o `cval'+
	}
	else {
		loc op "=="
		loc cval_o `cval'
	}

	tempvar cutoff
	gen `cutoff' = `cvar' `op' `cval'
	reg `cutoff' Treatment `dopanel' _Isuper*, vce(cl cluster)

	loc en = e(N)
	loc df = e(df_r)

	loc ct = _b[Treatment]
	loc se = _se[Treatment]
	test Treatment
	loc p = r(p)
	loc inf = cond(`p' < .1, "Significant", "Not significant")
	loc radius = invttail(e(df_r), .05) * `se'
	loc cu = `ct' + `radius'
	loc cl = `ct' - `radius'

	sort `cvar'
	gen n = _n
	su n if `cutoff'
	di _N
	loc qt = round(100 * (r(min) - 1) / _N)
	drop n

	* Update the results dataset.

	preserve

	u "`countdta'", clear

	set obs `=_N + 1'

	putlast outvar =		"`cvar'"
	putlast olab =			`"`lab'"'
	putlast typed_depvar =	"`typed_depvar'"
	putlast typed_type2 =	"`type2'"
	putlast var_val =		`cval'
	putlast var_val_o =		"`cval_o'"
	putlast quantile =		`qt'
	putlast tc_q =			`ct'
	putlast tse_q =			`se'
	putlast p_q =			`p'
	* CU_q and CL_q are swapped in order to replicate old results.
	putlast CU_q =			`cl'
	putlast CL_q =			`cu'
	putlast inference_q =	"`inf'"
	putlast df =			`df'
	putlast N =				`en'

	sa, replace

	restore
}

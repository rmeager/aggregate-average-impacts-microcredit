/*
Authors:
	Andrew Hillis, Innovations for Poverty Action
	Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
Purpose: Determine the typed outcomes for quantile regression based on the
outcome category. This information is needed by both graphs_quantile_regs.do and
graphs_quantile.do. It is necessary now that we are running the regressions in
pieces, creating results dataset subsets, making the correct order of the
regressions difficult to know after the fact.

Options
-------
Option -category()- is required.

category()	Outcome category
group()		Name of the local macro in the calling do-file in which to store the
			outcome group associated with the outcome category, including count
			variables
count()		Name of the local macro in the calling do-file in which to store the
			list of count variables associated with the outcome category. Not
			supported for `CatOld*' categories.
*/

mata: comma0()
syntax, category(integer) [group(name local) count(name local)]

enumtypes

evalname Cat `category'

assert "`Pan'" == ""

* AEJ
if `category' == `CatSelfEmploy' {
	// Q10_any bizstart12mo bizclose: binary
	loc g0 Q10_9_totrev Q10_8_totexp Q10_9_toprof Q10_1
	loc count0 Q10_1
}
else if `category' == `CatIncome' {
	loc g0 Q13_3_amount Q13_jobincome remitandtrans Q13_5_amount_mo
}
else if `category' == `CatLabor' {
	// Q12_2: binary
	loc g0 Q5_frac_working Q10_4_tot_family
	loc count0 Q10_4_tot_family
}
else if `category' == `CatConsumptionChimera' {
	loc g0 Q9_3_totalcategories Q9_totvalue Q7_nonfood_nondur foodconsump ///
		Q18_2_2_amount_wk Q18_2_1_amount_wk Q7_tempt Q18_2_3_amount_wk
	loc count0 Q9_3_totalcategories
}
else if `category' == `CatSocialChimera' {
	// Q2_3_sing_any Q20_2: binary
	loc g0 Q5_fraction Q16_tot_somepower_cond Q16_tot_aconf_cond ///
		 `Pan'Q15_2_mean_formal `Pan'Q15_2_mean_people
	loc count0 Q16_tot_somepower_cond Q16_tot_aconf_cond
}
else if `category' == `CatWelfareChimera' {
	// Q12_1_2_sat Q3_1_sat Q9_4_soldloan_none Q9_4_anycategories: binary
	loc g0 `Pan'Q22_mean `Pan'stress_index `Pan'Q17_AverageLocus ///
		`Pan'sat_index
}
else {
	err 9
}

assert `:list count0 in g0'

if "`group'" != "" ///
	c_local `group' `g0'
if "`count'" != "" ///
	c_local `count' `count0'

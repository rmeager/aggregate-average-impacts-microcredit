/*
Authors:
	Andrew Hillis, Innovations for Poverty Action
	Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
Purpose: Prepare the variables of a quantile regression.

Options
-------
All options are required.

typed()		Name of typed outcome
paper()		graphs_quantile_regs.do `paper' parameter
sampcode()	graphs_quantile_regs.do `sampcode' parameter
dopanel()	Name of the local macro in the calling do-file in which to store the
			list of panel variables to include in the regression
*/

mata: comma0()
syntax, typed(name) paper(integer) sampcode(integer) dopanel(name local)

enumtypes

if `paper' == `PaperAEJ' ///
	u "$andata_aej", clear
else ///
	err 9

* Implement typed outcome regression types.
parse_typed_outcome `typed'
loc outcome `s(depvar)'
loc type1   `s(type1)'
loc type2   `s(type2)'
assert "`type1'" == ""
assert inlist("`type2'", "bo", "")
if "`type2'" == "bo" {
	clonevar bo_`outcome' = `outcome'
	replace bo_`outcome' = . if !Q10_1
	sampexp `sampcode'
	assert !mi(Q10_1) if `r(exp)'

	cap conf var BE_`outcome', exact
	if !_rc {
		gen BE_bo_`outcome' =		BE_`outcome'
		gen BE_bo_`outcome'_mdum =	BE_`outcome'_mdum
	}
}

* Prepare the dependent variable's baseline equivalent and its missingness
* dummy.
cap conf var BE_`typed', exact
if _rc ///
	c_local `dopanel'
else {
	clonevar BaselineValue = BE_`typed'
	assert !mi(BE_`typed')

	conf var BE_`typed'_mdum, exact
	clonevar BaselineValueMDum = BE_`typed'_mdum
	assert inlist(BaselineValueMDum, 0, 1)
	assert !BaselineValue if BaselineValueMDum

	c_local `dopanel' BaselineValue BaselineValueMDum InPanel
}

* Author: Matt White, Innovations for Poverty Action, mwhite@poverty-action.org
* Purpose: Complete initialization common across do-files.

* -version- intentionally omitted


/* -------------------------------------------------------------------------- */
					/* system parameters	*/

if c(stata_version) < 12 & c(memory) < /* 1000m */ 1000 * 1024^2 {
	preserve
	clear
	set mem 1000m
	restore
}

if c(matsize) < 800 ///
	set matsize 800

set varabbrev off
set more off

clear programs
clear mata


/* -------------------------------------------------------------------------- */
					/* macros				*/

* Define globals that are constant and are used in multiple do-files.

* Define $compradir as the location of Main.
loc curdir "`c(pwd)'"
nobreak {
	loc diff 0
	#d ;
	foreach cmd in
		"c compra"
	{;
		#d cr
		cap `cmd'
		if !_rc {
			mata: st_local("diff", strofreal(c("pwd") != st_local("curdir")))
			if `diff' ///
				continue, break
		}
	}
	assert !_rc & `diff'
	glo compradir "`c(pwd)'"
	cd "`curdir'"
}

* Directories
glo datadir			"$compradir/data"
glo resdir			"$compradir/results"
* These directories must end with a forward slash.
glo logf			./

* Datasets
* Final, clean analysis datasets
glo andata_aej		"$datadir/analysis_data_AEJ_pub"

* Used for organizing do-file sections that are collapsible in the do-file
* editor or Notepad++.
loc org 1


/* -------------------------------------------------------------------------- */
					/* load programs		*/

foreach type in ado {
	loc base "`c(pwd)'/`type'"
	mata: st_local("`type'dirs", strlower(invtokens(`"""' :+ ///
		(st_local("base"), dir(st_local("base"), "dirs", "*", 1)') :+ ///
		`"""')))
}

loc exclude "`"`c(pwd)'/ado\External"'"
mata: st_local("exclude", strlower(st_local("exclude")))
assert `:list exclude in adodirs'
loc adodirs : list adodirs - exclude

* Add the ado directory to the ado-path.
foreach dir of loc adodirs {
	adopath ++ "`dir'"
}

* Run the .mata files in the ado directory.
foreach dir of loc adodirs {
	loc files : dir "`dir'" file "*.mata"
	foreach file of loc files {
		cap noi do "`dir'/`file'"
		if c(stata_version) >= 11.2 ///
			assert !_rc
		else if _rc {
			tempname fh
			file open `fh' using "`dir'/`file'", r
			loc eof 0
			loc found 0
			while !`eof' & !`found' {
				file read `fh' line
				loc eof = r(eof)
				if !`eof' ///
					loc found = regexm(`"`macval(line)'"', "^vers ([0-9.]+)$")
			}
			file close `fh'
			assert `found'
			loc vers = regexs(1)
			conf n `vers'
			assert `vers' >= 11.2
		}
	}
}

preptest

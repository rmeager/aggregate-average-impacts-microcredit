* This do file displays the results I got from basic regressions with the Angelucci et al 2015 data
* Rachael Meager
* Feb 2015
* If you have comments or questions please contact me at rmeager@mit.edu

*NOTES*
* I have taken the data labels at face value


regress Q10_9_totrev Treatment, robust
regress Q10_8_totexp Treatment, robust
regress Q10_9_toprof Treatment, robust

regress Q10_9_totrev BTreatment, robust
regress Q10_8_totexp BTreatment, robust
regress Q10_9_toprof BTreatment, robust

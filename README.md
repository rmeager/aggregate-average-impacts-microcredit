README FOR THE DIRECTORY ASSOCIATED WITH THE PROJECT "UNDERSTANDING THE AVERAGE IMPACT OF MICROCREDIT" 
Author: Rachael Meager (rachael.meager@gmail.com)
Date: March 27 2017

1.  This directory contains the core code base for the project as it stood in Dec 2016, which can be run either from scratch or taking the cleaned data as given. It has terrible nonexistent directory structure and is not an example of the highest standards of project management but it does work. I aim to update this whole project structure in the future when I write the accompanying R package but this is what I have now.

2. The scripts call for installation of certain packages and each start by erasing the workspace, which is why there is no R masterfile that can run the whole project at this time. Sorry. This too will be fixed in future. 

3. This project critically relies on the package Rstan, which only works if your machine has a C++ compiler that can talk to R.  If you don't have one, you can find instructions for downloading and setting up Rstan and the requisite compiler here: https://github.com/stan-dev/rstan/wiki/RStan-Getting-Started. Configuration is described as "optional but highly recommended", I have found it to be non-optional for my machines. 

4. To reproduce the project results taking the data as given, first run all R files starting with the word "replicating". Then run the files starting with "microcredit-paper-basic-analysis-optimised-". Then you can run the files starting with "microcredit-paper-basic-analysis-graphics-tables-". 

5. The data is presented as given because the variables were partially constructed using the original authors' stata code and partially constructed by me in R. To see exactly what I did in R, look at the file import_organise_data_v7.R. As you can see from that file, the input data file I provide in this directory was compiled from the 7 data sets available online and then cleaned and standardized. 
The procedure for this compilation is messy and requires some cleaning in Stata (using the original code from the authors, to ensure compliance with their results) and some cleaning in R. The original data files, the original cleaning and compilation code from the authors, and where necessary my own cleaning and compilation files are all located in the subdirectory "microcredit rct data". 

6. Because I want the project to be fully replicable with free and open source software, I have supplied this directory with the cleaned data files ready for use in R and Stan.  That means some of the comments and stops in the code, which were designed to prevent the loading of the wrong data files (e.g. after discovering errors in the original cleaning code), are not relevant to you when you use my code now. In general you may disregard comments relating to importing raw data unless you plan to mess with the subfolder called "microcredit rct data". Then the comments and stops are there to help you as they helped me.

7. As you can see from the original data, the current import file import_organise_data_v7.R does not import all the variables one might possibly want. In future, for other projects, I plan to use other intermediate variables not currently used here.
  
8. The output files produced by this project are large. Stanfit objects are large objects and this project uses many, many stanfit objects which are saved as output (this was largely to give me flexibility when I wrote the graphics and second-stage analysis files). You will need a decent amount of free storage space if you want to replicate this project (on the order of 30 GB). R is also RAM-intensive, and I used an MIT server with a terabyte of RAM to run this. If the project isn't working for you, check these issues before you email me. R sometimes issues generic errors in these cases, so the error message may not point you to them.

9. If you see any errors in this code please alert me immediately via email. 
If the code does not run on your machine and delivers generic errors, please check package installation issues, test Stan as described in step 3, and check RAM and storage before contacting me.
functions {
  real normal_ss_log(int N, int P, real y_sq_sum, vector xy_sum,
                     matrix xx_sum, vector beta_mus, vector beta_taus, real sigma) { 
    real beta_xy;
    real lp;
    vector[P] beta;
    beta <- append_row(beta_mus, beta_taus);
    beta_xy <- dot_product(xy_sum, beta);
    lp <- -.5*(y_sq_sum - 2*beta_xy + sum(beta * beta' .* xx_sum))/sigma^2;
    lp <- lp - .5*N*log(sigma^2);
    return lp;
  }
}


data {
  int<lower=0> K;  //  number of sites
  int<lower=0> N;  //  total number of observations
  int<lower=0> Q;  //  number of interactions groups
  int<lower=0> P;  //  dimensionality of data passed to the mean of the likelihood: P = 2Q but it's easier to enter it directly
  real y[N];  //  outcome variable of interest
  int site[N];  //  factor variable to split them out into K sites
  matrix[N,Q] X_mu;  //  bunch of stacked and interacted covariates on mean
  matrix[N,Q] X_tau;  //  same bunch of stacked and interacted covariates on slopes
}

transformed data {
  int N_k[K];  // number of observations from site K
  real y_sq_sum[K];  // sum_i y_{ki}^2
  vector[P] xy_sum[K];  // sum_i y_ki [X_mu_{ki}', X_tau_{ki}']
  matrix[P,P] xx_sum[K];  // sum_i [X_mu_{ki}', X_tau_{ki}'] [X_mu_{ki}', X_tau_{ki}']'
  int s; // indexes the loop over sites 
  vector[P] x;  //  possibly I'm going to be in trouble since this guys starts his life as a row vectorrrrr
  // initialize everything to zero
  N_k <- rep_array(0, K);
  y_sq_sum <- rep_array(0.0, K);
  xy_sum <- rep_array(rep_vector(0.0, P), K);
  xx_sum <- rep_array(rep_matrix(0.0, P, P), K);
  for (n in 1:N) {
    s <- site[n];
    x <- (append_col(X_mu[n], X_tau[n]))';  //  here is a column vector i hope ?? hum hum hum
    N_k[s] <- N_k[s] + 1;
    y_sq_sum[s] <- y_sq_sum[s] + y[n]^2;
    xy_sum[s] <- xy_sum[s] + y[n]*x;
    xx_sum[s] <- xx_sum[s] + x*x';
  }
}

parameters {
  vector[Q] tau_block;  //  I split up tau and mu, not modeling them as correlated, as a first-pass smulified model
  vector[Q] mu_block;
  matrix[K,Q] tau_k_block;
  matrix[K,Q] mu_k_block;
  real<lower=0> sigma_tau_block[Q];  //  I split the taus and mus up into blocks, not modeling them as correlated across subgroups
  real<lower=0> sigma_mu_block[Q];
  real<lower=0> sigma_y_k[K];
}

transformed parameters {

}

model {
  //  bounded uniform priors on the standard deviation parameters (sorry, Andy)
  sigma_tau_block ~ uniform(0,10000);
  sigma_mu_block ~ uniform(0,10000);
  sigma_y_k ~ uniform(0,100000);


  //  I am hard coding the priors on the hyperparameters here
  for(i in 1:Q) {
    tau_block[i] ~ normal(0,1000);
    mu_block[i] ~ normal(0,1000);
      for(k in 1:K) {
        tau_k_block[k,i] ~ normal(tau_block[i],sigma_tau_block[i]);
        mu_k_block[k,i] ~ normal(mu_block[i],sigma_mu_block[i]);
        // increment_normal_ss_lp(N_k[k], P, y_sq_sum[k], xy_sum[k], xx_sum[k], mu_k_block[k]', tau_k_block[k]', sigma_y_k[k]);
        increment_log_prob(normal_ss_log(N_k[k],P, y_sq_sum[k], xy_sum[k], xx_sum[k], mu_k_block[k]', tau_k_block[k]', sigma_y_k[k]));

      }
    }

}

generated quantities{

vector[Q] predicted_tau_k_block; 
vector[Q] predicted_mu_k_block;

for(q in 1:Q){
predicted_tau_k_block[q] <- normal_rng(tau_block[q], sigma_tau_block[q]);
predicted_mu_k_block[q] <- normal_rng(mu_block[q], sigma_mu_block[q]);

}



}

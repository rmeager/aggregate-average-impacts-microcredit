
data {
int<lower=0> K;// number of sites 
int<lower=0> N;// total number of observations 
real y[N];// outcome variable of interest
int ITT[N];// intention to treat indicator
int site[N];//factor variable to split them out into K sites
}
parameters {
real tau;//
real mu;//
vector[K] tau_k;// 
vector[K] mu_k;//
real<lower=0> sigma_tau;//
real<lower=0> sigma_mu;//
real<lower=0> sigma_y_k[K];//


}
transformed parameters {

}
model {
//let me try with bounded uniform
sigma_tau ~ uniform(0,100000);
sigma_mu ~ uniform(0,100000);
sigma_y_k ~ uniform(0,100000);

//I am hard coding the priors on the hyperparameters here
tau ~ normal(0,1000); 
mu ~ normal(0,1000); // one could later insert a more realistic mean but it doesnt matter



tau_k ~ normal(tau, sigma_tau);

mu_k ~ normal(mu, sigma_mu);



{vector[N] meanvec_y;
vector[N] sigmavec_y;
for(n in 1:N){
meanvec_y[n] <- mu_k[site[n]] + tau_k[site[n]]*ITT[n];
sigmavec_y[n] <- sigma_y_k[site[n]];
}

y ~ normal(meanvec_y,sigmavec_y); //data level 
}
}

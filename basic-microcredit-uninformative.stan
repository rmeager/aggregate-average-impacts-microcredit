data {
  int<lower=0> K;  // number of sites
  int<lower=0> N;  // total number of observations
  int<lower=0> P;  // dimensionality of parameter vector which is jointly distributed - here, it is 2 dimensional
  real y[N];       // outcome variable of interest
  int ITT[N];      // intention to treat indicator
  int site[N];     // factor variable to split them out into K sites
  matrix[P,P] mutau_prior_sigma;
  vector[P] mutau_prior_mean;
}
parameters {
  vector[P] mutau;
  matrix[K,P] mutau_k;

  real<lower=0> sigma_y_k[K];
  corr_matrix[P] Omega;        //  correlation
  vector<lower=0>[P] theta;    //  scale
}
transformed parameters {
  matrix[P,P] sigma_mutau;
  sigma_mutau <- quad_form_diag(Omega,theta);
}
model {
  //data variance priors
  // XXX: change this?
  sigma_y_k ~ uniform(0,100000);
  // sigma_y_k ~ inv_gamma(0.1,10);

  // parameter variance priors
  // XXX: change this?
  theta ~ cauchy(0,2.5);
  // theta ~ normal(0,100);
  Omega ~ lkj_corr(2);

  // hyperparameter priors
  mutau ~ multi_normal(mutau_prior_mean, mutau_prior_sigma) ;

  for (k in 1:K) {
    mutau_k[k] ~ multi_normal(mutau, sigma_mutau);
  }

  {
    vector[N] meanvec_y;
    vector[N] sigmavec_y;
    for (n in 1:N) {
      meanvec_y[n] <- mutau_k[site[n],1] + mutau_k[site[n],2]*ITT[n];
      sigmavec_y[n] <- sigma_y_k[site[n]];
    }
    y ~ normal(meanvec_y,sigmavec_y); // data level
  }
}
